#include "generator.h"

#include <charconv>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <regex>
#include <sstream>

#include <boost/json.hpp>
#include <searcher.h>

namespace fs = std::filesystem;

namespace impl
{
template <typename... Ts>
struct any_of_t
{
	std::tuple<Ts...> values;
};

template <typename T, typename... Ts, std::size_t... Is>
constexpr bool compare(
	T const& lhs, any_of_t<Ts...> const& rhs, std::index_sequence<Is...>)
{
	return (... || (lhs == std::get<Is>(rhs.values)));
}
}  // namespace impl

template <typename T, typename... Ts>
constexpr bool operator==(T const& lhs, impl::any_of_t<Ts...> const& rhs)
{
	return impl::compare(lhs, rhs, std::index_sequence_for<Ts...>{});
}

template <typename T, typename... Ts>
constexpr bool operator!=(T const& lhs, impl::any_of_t<Ts...> const& rhs)
{
	return !impl::compare(lhs, rhs, std::index_sequence_for<Ts...>{});
}

template <typename... Ts>
constexpr auto any_of(Ts&&... args)
{
	return impl::any_of_t<Ts...>{ { std::forward<Ts>(args)... } };
}

constexpr bool is_space(char ch)
{
	return ch == any_of(' ', '\n', '\r', '\t', '\f', '\v');
}

void proxies_from_file(
	std::string_view exec, std::string_view infile, std::string_view outfile)
{
	std::vector<card_quantity> cards;

	std::ifstream deck{ std::string{ infile } };
	while (is_space(deck.peek()))
		deck.get();
	if (deck.peek() == any_of('[', '{'))
	{
		std::array<char, 4096> buf;
		boost::json::parser    p;
		while (deck.read(buf.data(), buf.size()))
			p.write(buf.data(), deck.gcount());
		auto jv = p.release();

		if (auto const* const obj = jv.if_object())
			jv = obj->at("cards");

		for (auto const& el : jv.as_array())
		{
			cards.emplace_back(boost::json::value_to<card_t>(el.at("card")),
				el.at("quantity").to_number<std::uint8_t>());
		}
	}
	else
	{
		searcher    s{ exec };
		std::string line;
		while (std::getline(deck, line))
		{
			std::uint8_t q = 1;
			auto         ptr =
				std::from_chars(line.c_str(), line.c_str() + line.size(), q).ptr;
			while (ptr < line.c_str() + line.size() && is_space(*ptr))
				++ptr;

			auto const beg = ptr;
			while (ptr < line.c_str() + line.size() && *ptr != any_of('[', '<', '|'))
				++ptr;

			auto const end = [](char const* ptr) {
				while (is_space(*ptr))
					--ptr;
				return ptr;
			}(ptr);
			auto query = std::format("!\"{}\"", std::string_view{ beg, end });

			std::string_view set, number;
			char const*      group_end = nullptr;
			while (ptr < line.c_str() + line.size())
			{
				switch (*ptr)
				{
				case '[':
					group_end =
						std::find(ptr + 2, line.c_str() + line.size(), ']');
					set = { ptr + 1, group_end };
					ptr = group_end;
					break;
				case '<':
					group_end =
						std::find(ptr + 2, line.c_str() + line.size(), '>');
					number = { ptr + 1, group_end };
					if (number[0] < '0' || number[0] > '9')
						number = {};
					ptr = group_end;
					break;
				case '|':
					group_end =
						std::find(ptr + 2, line.c_str() + line.size(), '|');
					if (set.empty())
						set = { ptr + 1, group_end };
					else
						number = { ptr + 1, group_end };
					ptr = group_end;
					break;
				default:
					++ptr;
				}
			}
			if (!set.empty())
				query += std::format(" set:{}", set);
			if (!number.empty())
				query += std::format(" cn:{}", number);

			cards.emplace_back(s.search(query)[0], q);
		}
	}
	return generate_proxies(exec, cards, outfile);
}

void proxies_from_search(
	std::string_view exec, std::string_view search, std::string_view outfile)
{
	std::vector<card_quantity> cards;
	searcher                   s{ exec };
	s.for_each_result(
		search, [&cards](card_t&& c) { cards.emplace_back(std::move(c), 1); });
	return generate_proxies(exec, cards, outfile);
}

void generate_proxies(
	std::string_view exec, std::string_view input, std::string_view outfile)
{
	if (std::filesystem::exists(input))
		return proxies_from_file(exec, input, outfile);
	else
		return proxies_from_search(exec, input, outfile);
}

void generate_proxies(
	std::string_view exec, std::span<card_quantity> cards, std::string_view outfile)
{
	std::ofstream ostr(std::string{ outfile });
	ostr << "<html>\n<head><title>Proxies</"
	        "title><style>img{width:215;height:300}</style></head>\n<body>\n";
	for (const auto& i : cards)
		for (int j = 0; j < i.quantity; ++j)
			ostr << std::format("<img src={}>\n", i.card.image_uris.large);
	ostr << "</body></html>";
}
