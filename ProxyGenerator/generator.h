#pragma once
#include <span>
#include <string_view>

#include <card.h>

struct card_quantity
{
	card_t       card;
	std::uint8_t quantity = 1;
};

// input can be a file or a search
void generate_proxies(
	std::string_view exec, std::string_view input, std::string_view outfile);
void generate_proxies(std::string_view exec,
	std::span<card_quantity>           cards,
	std::string_view                   outfile);
