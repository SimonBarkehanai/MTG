#include <filesystem>
#include <fstream>
#include <iostream>

#include <boost/program_options.hpp>

#include "generator.h"

using namespace std::literals;
namespace po = boost::program_options;
namespace fs = std::filesystem;

void usage(std::string_view exec)
{
	std::cout << std::format(
		"Usage:\n"
		"  {} <Input> <Output File>\n"
		"  Options:\n"
		"    Input can be a deck file as a plain text document or a json deck "
		"or a quoted search\n"
		"    Output File will be written to as html\n",
		exec);
}

int main(int argc, char* argv[])
{
	if (argc != 3)
	{
		usage(argv[0]);
		return 2;
	}
	else
	{
		generate_proxies(argv[0], argv[1], argv[2]);
	}
}