#pragma once

#define BOOST_UUID_NO_TYPE_TRAITS

#include <array>
#include <chrono>
#include <optional>
#include <string>
#include <vector>

#include <boost/json/fwd.hpp>
#include <boost/json/value_to.hpp>

struct date_t
{
	uint16_t year;
	uint8_t  month;  // Jan is 1, Feb is 2, ...
	uint8_t  day;
};

struct uuid
{
	static constexpr uuid from_string(std::string_view s)
	{
		uuid         res{};
		std::uint8_t i = 0;
		for (auto const ch : s)
		{
			res.data[i / 2] <<= 4;
			std::uint8_t val;
			if ('0' <= ch && ch <= '9')
				val = ch - '0';
			else if ('a' <= ch && ch <= 'f')
				val = ch - 'a' + 10;
			else if ('A' <= ch && ch <= 'F')
				val = ch - 'A' + 10;
			else
				continue;
			res.data[i / 2] |= val;
			if (++i >= 16)
				break;
		}
		return res;
	}

	std::array<std::uint8_t, 16> data;
};

struct image_uris_t
{
	std::string small, normal, large, png, art_crop, border_crop;
};

enum class legalities_e
{
	legal,
	not_legal,
	restricted,
	banned,
};

struct legalities_t
{
	legalities_e standard, future, historic, gladiator, pioneer, explorer,
		modern, legacy, pauper, vintage, penny, commander, brawl, historicbrawl,
		alchemy, paupercommander, duel, oldschool, premodern;
};

struct prices_t
{
	std::optional<double> usd, usd_foil, usd_etched, eur, eur_foil, tix;
};

enum class component_e : uint8_t
{
	unknown,
	token,
	meld_part,
	meld_result,
	combo_piece,
};

struct related_card_object_t
{
	std::string object;
	uuid        id{};
	component_e component{};
	std::string name, type_line, uri;
};

enum class colors_e : uint8_t
{
	C = 0x00,
	W = 0x01,
	U = 0x02,
	B = 0x04,
	R = 0x08,
	G = 0x10,
};

struct card_face_t
{
	std::string             object;
	std::string             artist;
	std::optional<colors_e> color_indicator;
	colors_e                colors{};
	std::string             flavor_name, flavor_text;
	uuid                    illustration_id;
	image_uris_t            image_uris;
	std::string loyalty, mana_cost, name, oracle_text, power, printed_name,
		printed_text, printed_type_line, toughness, type_line, watermark;
	uuid artist_id{};
};

struct purchase_uris_t
{
	std::string tcgplayer, cardmarket, cardhoarder;
};

struct related_uris_t
{
	std::string gatherer, tcgplayer_infinite_articles, tcgplayer_infinite_decks,
		edhrec, mtgtop8;
};

struct preview_t
{
	date_t      previewed_at;
	std::string source_uri, source;
};

enum class games_e : uint8_t
{
	none  = 0x0,
	paper = 0x1,
	arena = 0x2,
	mtgo  = 0x4,
};

enum class rarity_e : uint8_t
{
	unknown,
	common,
	uncommon,
	rare,
	special,
	mythic,
	bonus,
};

enum class border_color_e : uint8_t
{
	unknown,
	black,
	borderless,
	gold,
	silver,
	white,
};

enum class layout_e : uint8_t
{
	normal,
	split,
	flip,
	transform,
	modal_dfc,
	meld,
	leveler,
	class_e,
	saga,
	adventure,
	planar,
	scheme,
	vanguard,
	token,
	double_faced_token,
	emblem,
	augment,
	host,
	art_series,
	double_sided,
};

enum class finishes_e : std::uint8_t
{
	foil    = 0x1,
	nonfoil = 0x2,
	etched  = 0x4,
	glossy  = 0x8,
};

enum class frame_effects_e : uint32_t
{
	none               = 0x0000'0000,
	legendary          = 0x0000'0001,
	miracle            = 0x0000'0002,
	nyxtouched         = 0x0000'0004,
	draft              = 0x0000'0008,
	devoid             = 0x0000'0010,
	tombstone          = 0x0000'0020,
	colorshifted       = 0x0000'0040,
	inverted           = 0x0000'0080,
	sunmoondfc         = 0x0000'0100,
	compasslanddfc     = 0x0000'0200,
	originpwdfc        = 0x0000'0400,
	mooneldrazidfc     = 0x0000'0800,
	moonreversemoondfc = 0x0000'1000,
	showcase           = 0x0000'2000,
	extendedart        = 0x0000'4000,
	companion          = 0x0000'8000,
	etched             = 0x0001'0000,
	fullart            = 0x0002'0000,
	snow               = 0x0004'0000,
};

enum class set_type_e : uint8_t
{
	unknown,
	core,
	expansion,
	masters,
	masterpiece,
	from_the_vault,
	spellbook,
	premium_deck,
	duel_deck,
	draft_innovation,
	treasure_chest,
	commander,
	planechase,
	archenemy,
	vanguard,
	funny,
	starter,
	box,
	promo,
	token,
	memorabilia,
};

struct frame_t
{
	std::uint16_t value;

	static constexpr frame_t future()
	{
		return std::numeric_limits<frame_t>::max();
	}
};

enum class security_e
{
	oval,
	triangle,
	acorn,
	arena
};

struct card_t
{
	std::string object;
	// core fields
	int              arena_id{};
	uuid             id{};
	std::string      lang;
	int              mtgo_id{}, mtgo_foil_id{};
	std::vector<int> multiverse_ids;
	int              tcgplayer_id{}, tcgplayer_etched_id{}, cardmarket_id{};
	uuid             oracle_id{};
	std::string      prints_search_uri, rulings_uri, scryfall_uri, uri;

	// gameplay fields
	std::vector<related_card_object_t> all_parts;
	std::vector<card_face_t>           card_faces;
	double                             cmc{};
	colors_e                           color_identity{};
	std::optional<colors_e>            color_indicator{};
	colors_e                           colors{};
	int                                edhrec_rank{};
	bool                               foil{}, nonfoil{};
	int                                hand_modifier{};
	std::vector<std::string>           keywords;
	layout_e                           layout{};
	legalities_t                       legalities;
	int                                life_modifier{};
	std::string                        loyalty, mana_cost, name;
	int                                penny_rank;
	std::string                        oracle_text;
	bool                               oversized;
	std::string                        power;
	std::optional<colors_e>            produced_mana{};
	bool                               reserved;
	std::string                        toughness, type_line;

	// print fields
	std::string              artist;
	bool                     booster{};
	border_color_e           border_color;
	uuid                     card_back_id{};
	std::string              collector_number;
	bool                     content_warning{}, digital{};
	finishes_e               finishes;
	std::string              flavor_name, flavor_text;
	frame_effects_e          frame_effects{};
	frame_t                  frame{};
	bool                     full_art{};
	games_e                  games{};
	bool                     highres_image{};
	uuid                     illustration_id{};
	std::string              image_status;
	image_uris_t             image_uris;
	prices_t                 prices;
	std::string              printed_name, printed_text, printed_type_line;
	bool                     promo{};
	std::vector<std::string> promo_types;
	purchase_uris_t          purchase_uris;
	rarity_e                 rarity;
	related_uris_t           related_uris;
	date_t                   released_at;
	bool                     reprint{};
	std::string              scryfall_set_uri, set_name, set_search_uri;
	set_type_e               set_type;
	std::string              set_uri;
	std::string              set;
	uuid                     set_id;
	bool                     story_spotlight{}, textless{}, variation{};
	uuid                     variation_of;
	security_e               security_stamp;
	std::string              watermark;
	preview_t                preview;

	// ???
	std::vector<uuid> artist_ids;
};

card_t tag_invoke(boost::json::value_to_tag<card_t>, boost::json::value const& jv);
