#include "searcher.h"

#include <charconv>
#include <filesystem>
#include <format>
#include <iostream>

#include <boost/json.hpp>

#include "getter.h"
#include "search.h"

template <typename T>
	requires(std::same_as<std::invoke_result_t<T, card_t&&>, void>)
auto search_impl(std::string_view target, T&& func, getter& g)
{
	bool          has_more = false;
	std::uint16_t page     = 1;
	do
	{
		auto const json = g.get_json(std::format("{}&page={}", target, page));
		if (json.at("object").get_string() == "error")
			throw scryfall_error{ json.at("code").get_string().c_str() };

		assert(json.at("data").is_array());
		for (const auto& card : json.at("data").get_array())
		{
			func(boost::json::value_to<card_t>(card));
		}

		has_more = json.at("has_more").as_bool();
		++page;
	}
	while (has_more);
}


searcher::searcher(std::string_view exec)
	: getter_{ std::make_unique<getter>("api.scryfall.com",
		std::filesystem::absolute(exec)             // make exec absolute
			.replace_filename("scryfall_cert.pem")  // replace exe with cert
			.lexically_normal()                     // remove /foo/../ and /./
			.string()) }
{
}

searcher::~searcher() = default;

std::string target_from_query(std::string_view query)
{
	std::string target{ "/cards/search?q=" };
	for (const auto ch : query)
	{
		if (('a' <= ch && ch <= 'z') || ('A' <= ch && ch <= 'Z')
			|| ('0' <= ch && ch <= '9') || ch == '/' || ch == '=' || ch == '?'
			|| ch == '&' || ch == ',' || ch == '!' || ch == '-' || ch == '.'
			|| ch == ':')
			target += ch;
		else
		{
			char str[4];
			auto res = std::to_chars(std::begin(str) + 2, std::end(str), ch, 16);
			if (res.ptr == std::end(str))
			{
				str[1] = '%';
				target.append(std::begin(str) + 1, 3);
			}
			else
			{
				str[0] = '%';
				str[1] = '0';
				target.append(std::begin(str), 3);
			}
		}
	}
	return target;
}

std::vector<card_t> const& searcher::search(std::string_view query)
{
	results_.clear();
	search_impl(
		target_from_query(query),
		[this](card_t card) { results_.push_back(std::move(card)); },
		*getter_);
	return results_;
}

void searcher::for_each_result(
	std::string_view query, std::function<void(card_t&&)> const& func)
{
	search_impl(target_from_query(query), func, *getter_);
}
