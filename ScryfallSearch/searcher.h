#pragma once

#include <vector>

#include "card.h"
#include <functional>

class getter;

class scryfall_error : public std::exception
{
public:
	using std::exception::exception;
};

class searcher
{
public:
	searcher(std::string_view exec);
	~searcher();

	std::vector<card_t> const& search(std::string_view query);
	void                       for_each_result(
							  std::string_view query, std::function<void(card_t&&)> const& func);

private:
	std::unique_ptr<getter> getter_;
	std::vector<card_t>     results_;
};
