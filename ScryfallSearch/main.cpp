#include <filesystem>
#include <fstream>
#include <iostream>
#include <ranges>

#include <boost/program_options.hpp>
#include <searcher.h>

namespace po = boost::program_options;

std::string fix_name(std::string_view name)
{
	if (auto const pos = name.find("//"); pos != name.npos)
		return std::string{ name.substr(0, pos - 1) };
	else
		return std::string{ name };
}

std::string to_upper(std::string_view s)
{
	std::string res;
	std::ranges::transform(s, std::back_inserter(res), [](char ch) {
		if (ch >= 'a' && ch <= 'z')
			return static_cast<char>(ch - 'a' + 'A');
		else
			return ch;
	});
	return res;
}

int main(int argc, char* argv[])
{
	std::string                   search;
	bool                          forge = false;
	std::ostream*                 ostr  = &std::cout;
	std::unique_ptr<std::ostream> stream_ptr;

	for (int i = 1; i < argc; ++i)
	{
		std::string_view const arg{ argv[i] };
		if (arg == "-f" || arg.starts_with("--f"))
			forge = true;
		else if (arg == "-o" || arg.starts_with("--o"))
		{
			// next arg is output
			++i;
			if (!(*argv[i] == '-' && *++argv[i] == '\0'))
			{
				stream_ptr.reset(new std::ofstream(argv[i]));
				ostr = stream_ptr.get();
			}
		}
		else
			(search.empty() ? search : (search += ' ')).append(arg);
	}

	if (search.empty())
	{
		std::cout << std::format(
			"Usage:\n"
			"    {} [-f] <search>\n"
			"Options:\n"
			"     -f  Output Forge format (Name|Set)\n",
			argv[0]);
		return 2;
	}

	searcher s{ argv[0] };
	try
	{
		if (forge)
		{
			s.for_each_result(search, [ostr](card_t const& card) {
				*ostr << std::format("{}|{}\n", fix_name(card.name),
					to_upper(card.set));
			});
		}
		else
			s.for_each_result(search, [ostr](card_t const& card) {
				*ostr << std::format("{}\n", card.name);
			});
	}
	catch (scryfall_error const& e)
	{
		std::cout << std::format("Scryfall Error '{}' for search '{}'\n",
			e.what(), search);
	}
	catch (std::exception const& e)
	{
		std::cout << std::format("Other error: '{}'\n", e.what());
	}
}