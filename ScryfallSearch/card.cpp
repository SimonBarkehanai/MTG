#include "card.h"

#include <charconv>
#include <iostream>
#include <limits>

#include <boost/json.hpp>
#include <boost/preprocessor/list/for_each.hpp>
#include <boost/preprocessor/stringize.hpp>
#include <boost/preprocessor/variadic/to_list.hpp>

namespace json = boost::json;

namespace impl
{
template <typename E>
	requires std::is_enum_v<E>
struct enum_metadata;

template <typename E>
	requires std::is_enum_v<E>
struct enable_enum_from_str
{
	static constexpr bool value = false;
};

template <typename E>
	requires enable_enum_from_str<E>::value
E enum_from_string(std::string_view s)
{
	using data = enum_metadata<E>;
	for (std::size_t i = 0; i < data::values.size(); ++i)
	{
		if (s == data::names[i])
			return data::values[i];
	}
	throw std::invalid_argument(
		std::format("Invalid enumerator name '{}' "
					"for type '{}'",
			s, data::type_name));
}

#define ENUM_VALUE_COMMA(r, E, x)   E::x,
#define STRINGIZE_COMMA(r, data, x) std::string_view{ BOOST_PP_STRINGIZE(x) },
#define GENERATE_ENUM_METADATA(E, list)                                      \
	template <>                                                              \
	struct impl::enum_metadata<E>                                            \
	{                                                                        \
		static constexpr std::array       values{ BOOST_PP_LIST_FOR_EACH(    \
            ENUM_VALUE_COMMA, E, list) };                              \
		static constexpr std::array       names{ BOOST_PP_LIST_FOR_EACH(     \
            STRINGIZE_COMMA, _, list) };                               \
		static constexpr std::string_view type_name = BOOST_PP_STRINGIZE(E); \
	};                                                                       \
	template <>                                                              \
	struct impl::enable_enum_from_str<E>                                     \
	{                                                                        \
		static constexpr bool value = true;                                  \
	};

#define ENABLE_ENUM_FROM_JSON(E, ...)                                 \
	GENERATE_ENUM_METADATA(E, BOOST_PP_VARIADIC_TO_LIST(__VA_ARGS__)) \
	E tag_invoke(json::value_to_tag<E>, json::value const& jv)        \
	{                                                                 \
		return impl::enum_from_string<E>(jv.as_string());             \
	}
#define ENABLE_BITWISE_ENUM_FROM_JSON(E, ...)                                          \
	GENERATE_ENUM_METADATA(E, BOOST_PP_VARIADIC_TO_LIST(__VA_ARGS__))                  \
	constexpr E& operator|=(E& lhs, E rhs)                                             \
	{                                                                                  \
		using underlying = std::underlying_type_t<E>;                                  \
		lhs              = static_cast<E>(                                             \
            static_cast<underlying>(lhs) | static_cast<underlying>(rhs)); \
		return lhs;                                                                    \
	}                                                                                  \
	E tag_invoke(json::value_to_tag<E>, json::value const& jv)                         \
	{                                                                                  \
		E res{};                                                                       \
		for (auto const& val : jv.as_array())                                          \
			res |= impl::enum_from_string<E>(val.as_string());                         \
		return res;                                                                    \
	}

template <typename... Ts>
struct compile_time_list
{
};

template <typename T>
struct compile_time_for_each_impl;

template <template <typename...> typename L, typename... Ts>
struct compile_time_for_each_impl<L<Ts...>>
{
	template <typename F>
	constexpr void operator()(F&& func)
	{
		using A = int[sizeof...(Ts)];
		A{ (func(Ts{}), 0)... };
	}
};

template <typename T, typename F>
void compile_time_for_each(F&& func)
{
	return compile_time_for_each_impl<T>{}(std::forward<F>(func));
}

template <typename T>
	requires std::is_class_v<T>
struct struct_metadata;

template <typename T>
	requires std::is_class_v<T>
struct enable_struct_from_json
{
	static constexpr bool value = false;
};

template <typename T>
	requires enable_struct_from_json<T>::value
T struct_from_json(json::value const& jv)
{
	using data = struct_metadata<T>;
	T res{};
	for (auto const& el : jv.as_object())
	{
		bool found = false;
		compile_time_for_each<std::remove_cvref_t<typename data::members>>(
			[&found, &el, &res]<typename T>(T) {
				if (!found && el.key() == T::name())
				{
					using type = std::remove_cvref_t<decltype(res.*T::pointer())>;
					res.*T::pointer() = json::value_to<type>(el.value());
					found             = true;
				}
			});
		if (!found)
		{
			throw std::invalid_argument(
				std::format("Invalid member '{}' "
							"for type '{}'",
					std::string_view{ el.key().data(), el.key().size() },
					data::type_name));
		}
	}
	return res;
}

template <typename... Ts>
constexpr auto types_from_lambdas(int, Ts&&... lambdas)
{
	return compile_time_list<decltype(lambdas())...>{};
}

#define METADATA_FOR_MEMBER(r, T, x)                 \
	, []() {                                         \
		struct _                                     \
		{                                            \
			static constexpr auto pointer()          \
			{                                        \
				return &T::x;                        \
			}                                        \
			static constexpr std::string_view name() \
			{                                        \
				return BOOST_PP_STRINGIZE(x);        \
			}                                        \
		};                                           \
		return _{};                                  \
	}
#define GENERATE_STRUCT_METADATA(T, list)                                    \
	template <>                                                              \
	struct impl::struct_metadata<T>                                          \
	{                                                                        \
		/* workaround the prohibition of declaring types in decltype         \
		 * expressions */                                                    \
		static constexpr auto decltype_workaround = types_from_lambdas(      \
			0 BOOST_PP_LIST_FOR_EACH(METADATA_FOR_MEMBER, T, list));         \
		using members = decltype(decltype_workaround);                       \
		static constexpr std::string_view type_name = BOOST_PP_STRINGIZE(T); \
	};                                                                       \
	template <>                                                              \
	struct impl::enable_struct_from_json<T>                                  \
	{                                                                        \
		static constexpr bool value = true;                                  \
	};
#define ENABLE_STRUCT_FROM_JSON(T, ...)                                 \
	GENERATE_STRUCT_METADATA(T, BOOST_PP_VARIADIC_TO_LIST(__VA_ARGS__)) \
	T tag_invoke(json::value_to_tag<T>, json::value const& jv)          \
	{                                                                   \
		return impl::struct_from_json<T>(jv);                           \
	}

template <typename T>
struct enable_optional_from_json
{
	static constexpr bool value = false;
};

#define ENABLE_OPTIONAL_FROM_JSON(T)          \
	template <>                               \
	struct impl::enable_optional_from_json<T> \
	{                                         \
		static constexpr bool value = true;   \
	};
}  // namespace impl

namespace boost::json
{
// for this overload to be found, it needs to be in a namespace that can be seen
// by value_to with ADL
template <typename T>
	requires impl::enable_optional_from_json<T>::value
std::optional<T>
tag_invoke(json::value_to_tag<std::optional<T>>, json::value const& jv)
{
	if (jv.is_null())
		return std::nullopt;
	else
		return json::value_to<T>(jv);
}
}  // namespace boost::json

date_t tag_invoke(json::value_to_tag<date_t>, json::value const& jv)
{
	std::string_view src = jv.as_string();
	return date_t{ .year = static_cast<uint16_t>(
					   (src[0] - '0') * 1'000 + (src[1] - '0') * 100
					   + (src[2] - '0') * 10 + (src[3] - '0')),
		.month = static_cast<uint8_t>((src[5] - '0') * 10 + (src[6] - '0')),
		.day   = static_cast<uint8_t>((src[8] - '0') * 10 + (src[9] - '0')) };
}

uuid tag_invoke(json::value_to_tag<uuid>, json::value const& jv)
{
	return uuid::from_string(jv.as_string());
}

ENABLE_STRUCT_FROM_JSON(image_uris_t, small, normal, large, png, art_crop,
	border_crop);

ENABLE_ENUM_FROM_JSON(legalities_e, legal, not_legal, restricted, banned);

ENABLE_STRUCT_FROM_JSON(legalities_t, standard, future, historic, gladiator,
	pioneer, explorer, modern, legacy, pauper, vintage, penny, commander, brawl,
	historicbrawl, alchemy, paupercommander, duel, oldschool, premodern);

namespace boost::json
{
// for this overload to be found, it needs to be in a namespace that can be seen
// by value_to with ADL
std::optional<double>
tag_invoke(json::value_to_tag<std::optional<double>>, json::value const& jv)
{
	if (jv.is_null())
		return std::nullopt;
	if (auto str = jv.if_string(); str)
	{
		double val;
		std::from_chars(str->begin(), str->end(), val);
		return val;
	}
	throw std::invalid_argument(
		"Value cannot be converted to std::optional<double>");
}
}  // namespace boost::json

ENABLE_STRUCT_FROM_JSON(prices_t, usd, usd_foil, usd_etched, eur, eur_foil, tix);

ENABLE_ENUM_FROM_JSON(component_e, unknown, token, meld_part, meld_result,
	combo_piece);

ENABLE_STRUCT_FROM_JSON(related_card_object_t, object, id, component, name,
	type_line, uri)

ENABLE_BITWISE_ENUM_FROM_JSON(colors_e, C, W, U, B, R, G)

ENABLE_OPTIONAL_FROM_JSON(colors_e);

ENABLE_STRUCT_FROM_JSON(card_face_t, object, artist, color_indicator, colors,
	flavor_name, flavor_text, illustration_id, image_uris, loyalty, mana_cost,
	name, oracle_text, power, printed_name, printed_text, printed_type_line,
	toughness, type_line, watermark, artist_id);

ENABLE_STRUCT_FROM_JSON(purchase_uris_t, tcgplayer, cardmarket, cardhoarder);

ENABLE_STRUCT_FROM_JSON(related_uris_t, gatherer, tcgplayer_infinite_articles,
	tcgplayer_infinite_decks, edhrec, mtgtop8);

ENABLE_STRUCT_FROM_JSON(preview_t, previewed_at, source_uri, source);

ENABLE_BITWISE_ENUM_FROM_JSON(games_e, none, paper, arena, mtgo)

ENABLE_ENUM_FROM_JSON(rarity_e, unknown, common, uncommon, rare, special,
	mythic, bonus);

ENABLE_ENUM_FROM_JSON(border_color_e, unknown, black, borderless, gold, silver,
	white);

// class layouts need to be explicit
template <>
struct impl::enum_metadata<layout_e>
{
	static constexpr std::array values{ BOOST_PP_LIST_FOR_EACH(ENUM_VALUE_COMMA,
		layout_e,
		BOOST_PP_VARIADIC_TO_LIST(normal, split, flip, transform, modal_dfc, meld,
			leveler, class_e, saga, adventure, planar, scheme, vanguard, token,
			double_faced_token, emblem, augment, host, art_series, double_sided)) };
	static constexpr std::array names{ BOOST_PP_LIST_FOR_EACH(STRINGIZE_COMMA, _,
		BOOST_PP_VARIADIC_TO_LIST(normal, split, flip, transform, modal_dfc, meld,
			leveler, class, saga, adventure, planar, scheme, vanguard, token,
			double_faced_token, emblem, augment, host, art_series, double_sided)) };
	static constexpr std::string_view type_name = BOOST_PP_STRINGIZE(E);
};

template <>
struct impl::enable_enum_from_str<layout_e>
{
	static constexpr bool value = true;
};

layout_e tag_invoke(json::value_to_tag<layout_e>, json::value const& jv)
{
	return impl::enum_from_string<layout_e>(jv.as_string());
}

ENABLE_BITWISE_ENUM_FROM_JSON(finishes_e, foil, nonfoil, etched, glossy);

ENABLE_BITWISE_ENUM_FROM_JSON(frame_effects_e, none, legendary, miracle,
	nyxtouched, draft, devoid, tombstone, colorshifted, inverted, sunmoondfc,
	compasslanddfc, originpwdfc, mooneldrazidfc, moonreversemoondfc, showcase,
	extendedart, companion, etched, fullart, snow);

ENABLE_ENUM_FROM_JSON(set_type_e, unknown, core, expansion, masters,
	masterpiece, from_the_vault, spellbook, premium_deck, duel_deck,
	draft_innovation, treasure_chest, commander, planechase, archenemy,
	vanguard, funny, starter, box, promo, token, memorabilia);

frame_t tag_invoke(json::value_to_tag<frame_t>, json::value const& jv)
{
	std::string_view const str = jv.as_string();
	if (str == "future")
		return frame_t::future();
	else
	{
		frame_t res{};
		std::from_chars(str.data(), str.data() + str.size(), res.value);
		return res;
	}
}

ENABLE_ENUM_FROM_JSON(security_e, oval, triangle, acorn, arena);

ENABLE_STRUCT_FROM_JSON(card_t, object, arena_id, id, lang, mtgo_id, mtgo_foil_id,
	multiverse_ids, tcgplayer_id, tcgplayer_etched_id, cardmarket_id, oracle_id,
	prints_search_uri, rulings_uri, scryfall_uri, uri, all_parts, card_faces,
	cmc, color_identity, color_indicator, colors, edhrec_rank, foil, nonfoil,
	hand_modifier, keywords, layout, legalities, life_modifier, loyalty,
	mana_cost, name, penny_rank, oracle_text, oversized, power, produced_mana,
	reserved, toughness, type_line, artist, booster, border_color, card_back_id,
	collector_number, content_warning, digital, finishes, flavor_name,
	flavor_text, frame_effects, frame, full_art, games, highres_image,
	illustration_id, image_status, image_uris, prices, printed_name,
	printed_text, printed_type_line, promo, promo_types, purchase_uris, rarity,
	related_uris, released_at, reprint, scryfall_set_uri, set_name,
	set_search_uri, set_type, set_uri, set, set_id, story_spotlight, textless,
	variation, variation_of, security_stamp, watermark, preview, artist_ids);
