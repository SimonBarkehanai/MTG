#include "getter.h"

#include <charconv>
#include <iostream>
#include <thread>

#include <boost/asio/io_context.hpp>
#include <boost/asio/ip/v6_only.hpp>
#include <boost/beast/core/flat_buffer.hpp>
#include <boost/beast/core/tcp_stream.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/http/empty_body.hpp>
#include <boost/beast/http/string_body.hpp>
#include <boost/beast/ssl/ssl_stream.hpp>
#include <boost/beast/version.hpp>
#include <boost/json.hpp>
#include <boost/preprocessor/stringize.hpp>

#include "card.h"

using namespace std::literals;
namespace net   = boost::asio;
namespace beast = boost::beast;
namespace http  = beast::http;
namespace json  = boost::json;

class buffered_read;

class getter_impl
{
public:
	explicit getter_impl(std::string_view host, std::string const& cert_file)
		: m_ioc{}
		, m_ssl{ boost::asio::ssl::context::method::sslv23 }
		, m_stream{ m_ioc, m_ssl }
		, m_buffer{}
		, m_last_request{ clock::time_point::min() }
		, m_host{ host }
	{
		m_ssl.set_options(net::ssl::context::default_workarounds
						  | net::ssl::context::no_sslv2
						  | net::ssl::context::no_sslv3);
		m_ssl.load_verify_file(cert_file);
		boost::system::error_code ec;
		m_stream.set_verify_mode(net::ssl::verify_peer);
		beast::get_lowest_layer(m_stream).socket().open(net::ip::tcp::v6(), ec);
		beast::get_lowest_layer(m_stream).socket().set_option(
			net::ip::v6_only(true), ec);
		beast::get_lowest_layer(m_stream).socket().set_option(
			net::socket_base::reuse_address(false), ec);

		if (!SSL_set_tlsext_host_name(m_stream.native_handle(), m_host.data()))
		{
			ec = { static_cast<int>(::ERR_get_error()),
				net::error::get_ssl_category() };
			throw std::system_error{ ec };
		}

		const auto results =
			net::ip::tcp::resolver{ m_ioc }.resolve(m_host, port, ec);
		if (ec)
			throw std::system_error{ ec };

		beast::get_lowest_layer(m_stream).connect(results, ec);
		if (ec)
			throw std::system_error{ ec };

		m_stream.handshake(net::ssl::stream_base::client, ec);
		if (ec)
			throw std::system_error{ ec };
	}

	~getter_impl()
	{
		beast::error_code ec;
		m_stream.shutdown(ec);
		if (ec && ec != net::error::eof && ec != net::ssl::error::stream_truncated)
			std::cerr << "Error closing stream: " << ec.message() << '\n';
	}

	std::string get(std::string_view target)
	{
		perform_request(target);

		response_type             res{};
		boost::system::error_code ec;
		http::read(m_stream, m_buffer, res, ec);
		if (ec)
			throw std::system_error{ ec };

		return res.body();
	}

	json::value get_json(std::string_view target)
	{
		perform_request(target);

		http::response_parser<http::buffer_body> http_parser;
		http_parser.eager(true);

		char                      buf[512];
		boost::system::error_code ec;
		json::stream_parser       json_parser;
		json_parser.reset();

		while (!http_parser.is_done())
		{
			http_parser.get().body().data = buf;
			http_parser.get().body().size = sizeof(buf);
			http::read(m_stream, m_buffer, http_parser, ec);

			if (ec && ec != http::error::need_buffer)
				throw boost::system::system_error{ ec };

			json_parser.write(buf, sizeof(buf) - http_parser.get().body().size);
		}

		json_parser.finish();

		return json_parser.release();
	}

	void download_file(std::string_view target, std::string_view dest, bool verbose)
	{
		auto const dest_s = std::string{ dest };
		perform_request(target);

		boost::system::error_code ec;

		http::response_parser<http::file_body> parser{};

		parser.eager(true);
		parser.get().body().open(dest_s.c_str(), beast::file_mode::write, ec);
		parser.body_limit(boost::none);
		if (ec)
			throw std::system_error{ ec };

		if (verbose)
		{
			auto next = std::chrono::system_clock::now();
			while (!parser.is_done())
			{
				http::read_some(m_stream, m_buffer, parser);
				if (std::chrono::system_clock::now() > next)
				{
					if (auto length = parser.content_length(); length.has_value())
					{
						auto const gotten =
							*length - *parser.content_length_remaining();
						std::cout << std::format(
							"\rReceived {} of {} kilobytes ({}%)",
							gotten / 1024,
							*length / 1024,
							gotten * 100 / *length);
					}
					next += std::chrono::milliseconds(300);
				}
			}
			if (auto length = parser.content_length(); length.has_value())
			{
				std::cout << "\rReceived " << *length / 1024 << " of "
						  << *length / 1024 << " kilobytes (100%)\n";
			}
		}
		else
		{
			http::read(m_stream, m_buffer, parser);
		}
	}

	auto get_buffered(std::string_view target);

private:
	using string_body_type = http::string_body;
	using fields_type      = http::fields;
	using request_type     = http::request<http::empty_body, fields_type>;
	using response_type    = http::response<string_body_type, fields_type>;

	request_type create_request(std::string_view target)
	{
		constexpr int version = 11;

		request_type req{ http::verb::get,
			beast::string_view{ target.data(), target.size() },
			version };
		req.set(http::field::host, { m_host.data(), m_host.size() });
		req.set(http::field::user_agent, BOOST_BEAST_VERSION_STRING);

		return req;
	}

	void perform_request(std::string_view target)
	{
		auto req = create_request(target);

		// follow rate limits of 10 requests / sec
		std::this_thread::sleep_until(m_last_request + 100ms);
		m_last_request = clock::now();

		boost::system::error_code ec;
		http::write(m_stream, req, ec);
		if (ec)
			throw std::system_error{ ec };
	}

	using clock = std::chrono::steady_clock;

	net::io_context                             m_ioc;
	net::ssl::context                           m_ssl;
	beast::ssl_stream<boost::beast::tcp_stream> m_stream;
	beast::flat_buffer                          m_buffer;
	clock::time_point                           m_last_request;
	std::string                                 m_host;

	static constexpr std::string_view port{ "443" };

	friend class buffered_read;
};

class buffered_read
{
public:
	buffered_read(getter_impl* g) : getter_(g)
	{
		parser_.body_limit(boost::none);
	}

	std::string_view read_some()
	{
		http::read_some(getter_->m_stream, getter_->m_buffer, parser_);
		return { static_cast<char*>(getter_->m_buffer.data().data()),
			getter_->m_buffer.data().size() };
	}

	bool done() const
	{
		return parser_.is_done();
	}

private:
	getter_impl*                             getter_;
	http::response_parser<http::string_body> parser_;
};

getter::getter(std::string_view host, std::string const& cert_file)
	: impl_{ std::make_unique<getter_impl>(host, cert_file) }
{
}

getter::~getter() = default;

std::string getter::get(std::string_view target)
{
	return impl_->get(target);
}

boost::json::value getter::get_json(std::string_view target)
{
	return impl_->get_json(target);
}

void getter::download_file(
	std::string_view target, std::string_view dest, bool verbose)
{
	impl_->download_file(target, dest, verbose);
}

auto getter_impl::get_buffered(std::string_view target)
{
	perform_request(target);
	return buffered_read{ this };
}
