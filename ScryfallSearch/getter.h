#pragma once

#include <chrono>
#include <string>
#include <string_view>

#include <boost/json/fwd.hpp>

class getter_impl;

class getter
{
public:
	// cert_file needs to be null terminated
	getter(std::string_view host, std::string const& cert_file);
	~getter();

	// if you call these functions repeatedly, it will block to follow the rate
	// limits of 10 requests / second
	std::string        get(std::string_view target);
	boost::json::value get_json(std::string_view target);
	void               download_file(
					  std::string_view target, std::string_view dest, bool verbose = false);

private:
	std::unique_ptr<getter_impl> impl_;
};
