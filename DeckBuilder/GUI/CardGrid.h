#pragma once
#include <Downloader.h>

#include <QScrollArea>

#include "CardImage.h"

class CardGrid : public QScrollArea
{
	Q_OBJECT
public:
	explicit CardGrid(QWidget* parent = nullptr);

	void ClearResults();

	void reserve(std::size_t newCap)
	{
		mCards.reserve(newCap);
	}
	[[nodiscard]] std::size_t NumUniqueCards() const noexcept
	{
		return mCards.size();
	}
	template <typename T>
	void push_back(T&& val)
	{
		val.image->show();
		mCards.push_back(std::forward<T>(val));
	}
	template <typename... Args>
	void emplace_back(Args&&... args)
	{
		mCards.emplace_back(std::forward<Args>(args)...).image->show();
	}

	[[nodiscard]] decltype(auto) operator[](std::size_t pos) noexcept
	{
		return mCards[pos];
	}
	[[nodiscard]] decltype(auto) operator[](std::size_t pos) const noexcept
	{
		return mCards[pos];
	}
	[[nodiscard]] decltype(auto) at(std::size_t pos)
	{
		return mCards.at(pos);
	}
	[[nodiscard]] decltype(auto) at(std::size_t pos) const
	{
		return mCards.at(pos);
	}

	[[nodiscard]] auto& cards()
	{
		return mCards;
	}
	[[nodiscard]] const auto& cards() const
	{
		return mCards;
	}

	[[nodiscard]] std::vector<CardCardImage>::const_iterator find(
		std::string_view set, std::string_view colnum) const
	{
		return cfind(set, colnum);
	};
	[[nodiscard]] std::vector<CardCardImage>::iterator find(
		std::string_view set, std::string_view colnum);
	[[nodiscard]] std::vector<CardCardImage>::const_iterator cfind(
		std::string_view set, std::string_view colnum) const;
	[[nodiscard]] int findIndex(std::string_view set, std::string_view colnum) const;

	int NumCards() const;

	enum class Grouping
	{
		NoGrouping,
		Type,
		Category,
		Color,
		ColorIdentity,
	};

	[[nodiscard]] auto GetGrouping() const
	{
		return mGrouping;
	}
	[[nodiscard]] void SetGrouping(Grouping grouping)
	{
		mGrouping = grouping;
	}
	void LayoutImages();

	[[nodiscard]] std::vector<QAction*>& ContextMenuActions()
	{
		return mContextMenuActions;
	}
	[[nodiscard]] const std::vector<QAction*>& ContextMenuActions() const
	{
		return mContextMenuActions;
	}

	void ReloadImages(Downloader* downloader);

signals:
	void CardClicked(int index);
	void ContextMenuActionTriggered(int cardIndex, int actionIndex);

protected:
	void resizeEvent(QResizeEvent* e) override;
	void contextMenuEvent(QContextMenuEvent* e) override;

private:
	void LayoutUngrouped();
	void LayoutByType();

	int MaxColumns() const
	{
		return MaxColumns(width());
	}
	int MaxColumns(int width) const;

	int IndexAt(QPoint pos);
	int IndexAt(int x, int y)
	{
		return IndexAt(QPoint{ x, y });
	}

	std::vector<CardCardImage>           mCards;
	std::vector<QLabel*>                 mLabels;
	std::vector<QMetaObject::Connection> mConnections;
	std::vector<QAction*>                mContextMenuActions;
	Grouping                             mGrouping = Grouping::NoGrouping;

	inline static constexpr auto Padding = 10, Spacing = 10, LabelHeight = 15;
};
