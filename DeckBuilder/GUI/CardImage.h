#pragma once
#include <string>  // for string

#include <QBitmap>
#include <QEvent>     // for QEvent
#include <QLabel>     // for QLabel
#include <QObject>    // for QObject
#include <QPolygonF>  // for QPolygonF
#include <QString>    // for QString
#include <QWidget>    // for QWidget

#include "Card.h"
#include "ClickableLabel.h"  // for ClickableLabel

constexpr int constexpr_round(float val)
{
	auto ival = static_cast<int>(val);
	if (val - ival < .5f)
		return ival;
	else
		return ival + 1;
}

class CardImage : public ClickableLabel
{
	Q_OBJECT
public:
	enum class Face
	{
		SingleFace,
		Front,
		Back
	};

	explicit CardImage(std::string front    = "",
		int                        quantity = 0,
		QWidget*                   parent   = nullptr,
		Qt::WindowFlags            flags    = {});
	CardImage(std::string front,
		std::string       back,
		int               quantity = 0,
		QWidget*          parent   = nullptr,
		Qt::WindowFlags   flags    = {},
		Face              face     = Face::Front);

	void SetPicture(std::string front);
	void SetPicture(std::string front, std::string back);
	void SetQuantity(int newQuantity);

	inline static constexpr int Width  = 200,
								Height = constexpr_round(Width * 1040. / 745);

protected:
	virtual void enterEvent(QEvent* e) override;
	virtual void leaveEvent(QEvent* e) override;

signals:
	void Hovered();
	void UnHovered();

private slots:
	void Flip();

private:
	void SetPicture(std::string src, Face face);
	void ShowPicture();

	QLabel*         mQuantity = new QLabel(this);
	ClickableLabel* mFlip     = new ClickableLabel(this);
	QPixmap         mFront, mBack;
	Face            mFace = Face::SingleFace;

	inline static constexpr int QuantityWidth = 25, QuantityHeight = 20,
								BorderRight = 20, QuantityBorderTop = 35,
								FlipSize = 36, Spacing = 10;
	inline static const QBitmap* BorderMap = nullptr;

public:
	inline static constexpr int StackOffset =
		QuantityBorderTop + QuantityHeight + Spacing;
};

struct CardCardImage
{
	Card       card;
	CardImage* image;
};
