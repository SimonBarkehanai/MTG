#include "Sideboard.h"

#include <fstream>

Sideboard::Sideboard(Downloader* downloader, QWidget* parent, Qt::WindowFlags f)
	: MainDeck(downloader, parent, f)
{
	Grid()->SetGrouping(CardGrid::Grouping::NoGrouping);
}

void Sideboard::LoadDeck(std::string_view file)
{
	MainDeck::LoadDeck(nlohmann::json::parse(std::ifstream(file))["side"]);
}
