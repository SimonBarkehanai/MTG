#include "SearchPane.h"

#include <QMessageBox>
#include <QScrollBar>
#include <QStringBuilder>
#include <QTimer>

#include "LoadResources.h"

SearchPane::SearchPane(
	Downloader* downloader, QWidget* parent, std::string imageSize, Qt::WindowFlags f)
	: QWidget(parent, f), mSearcher(downloader), mDownloader(downloader)
{
	mSearchText->setPlaceholderText("Search...");
	mSearchText->setStyleSheet("padding-right:" % QString::number(IconSize - 2));

	mResultArea->move(0, SearchHeight + SearchSpacing);

	mError = new QLabel(mResultArea->widget());
	mError->move(Padding, Padding);
	mError->setAlignment(Qt::AlignmentFlag::AlignTop);
	mError->setWordWrap(true);
	mError->setTextFormat(Qt::TextFormat::RichText);

	mSearchIcon->resize(IconSize, IconSize);
	mSearchIcon->setPixmap(QPixmap(QStringLiteral(":/Icons/Search.png")));
	mSearchIcon->setMouseTracking(true);

	mRandomButton->resize(RandomWidth, RandomHeight);
	mRandomButton->setText("&Random");

	PopulateResults();

	connect(mSearchIcon, &ClickableLabel::clicked, this, &SearchPane::Search);
	connect(mSearchText, &QLineEdit::returnPressed, this, &SearchPane::Search);
	connect(mRandomButton, &QPushButton::clicked, this, &SearchPane::RandomSearch);
}

void SearchPane::ReDownload()
{
	std::for_each(begin(Grid()->cards()),
		end(Grid()->cards()),
		[downloader = mDownloader](
			const CardCardImage& c) { downloader->AddToDownload(c.card); });

	Grid()->ReloadImages(mDownloader);
}

void SearchPane::resizeEvent(QResizeEvent* e)
{
	const auto w = width(), h = height();
	mSearchText->resize(w - (RandomSpacing + RandomWidth), SearchHeight);
	mSearchIcon->move(w - (RandomSpacing + RandomWidth + IconSize + 1), 1);
	mRandomButton->move(w - (RandomWidth), 0);
	mResultArea->resize(w, h - (SearchSpacing + SearchHeight));
	LayoutResults();
}

void SearchPane::Search()
{
	mSearcher.Stop();

	ClearResults();

	auto q = mSearchText->text().toStdString();
	std::for_each(
		cbegin(mExtraArgs), cend(mExtraArgs), [&q](const std::string& val) {
			q += ' ';
			q += val;
		});

	mSearcher.Search(q);

	PopulateResults();
}

void SearchPane::RandomSearch()
{
	mSearcher.Stop();

	ClearResults();

	auto q = mSearchText->text().toStdString();
	std::for_each(
		cbegin(mExtraArgs), cend(mExtraArgs), [&q](const std::string& val) {
			q += ' ';
			q += val;
		});

	mSearcher.Random(q);

	PopulateResults();
}

void SearchPane::ClearResults()
{
	mResultArea->ClearResults();
	mError->hide();
	mNumResultsShown = 0;
	mResultArea->widget()->resize(0, 0);
}

void SearchPane::PopulateResults()
{
	// use the capacity from searcher to ensure we don't have to allocate
	mResultArea->reserve(mSearcher.Results().capacity());

	const auto last = std::min(mSearcher.Results().size(), mNumResultsShown + 5);

	for (std::size_t i = mNumResultsShown; i < last; ++i)
	{
		auto crd = mSearcher.ResultAt(i);

		CardImage* img;
		if (crd.NumFaces == 1)
			img = new CardImage(mSearcher.GetDownloader()->FullPathFor(crd[0]),
				crd.GetQuantity(),
				mResultArea->widget());
		else
			img = new CardImage(mSearcher.GetDownloader()->FullPathFor(crd[0]),
				mSearcher.GetDownloader()->FullPathFor(crd[1]),
				crd.GetQuantity(),
				mResultArea->widget());

#ifdef __cpp_aggregate_paren_init
		mResultArea->emplace_back(crd, img);
#else
		mResultArea->push_back(CardCardImage{ .card = crd, .image = img });
#endif

		connect(img, &CardImage::clicked, this, [this, index = i] {
			emit CardClicked(index);
		});
	}

	QString ErrorText =
		"<p align=center>" % QString::fromStdString(mSearcher.Error()) % "</p>";

	decltype(auto) warnings = mSearcher.Warnings();

	for (const auto& warning : warnings)
	{
		ErrorText += QString::fromStdString(warning) % "<br>";
	}
	mError->setText(ErrorText);

	LayoutResults();

	mNumResultsShown = last;

	if (mSearcher.Results().size() > mNumResultsShown || mSearcher.Busy())
		QTimer::singleShot(
			std::chrono::milliseconds(0), this, &SearchPane::PopulateResults);
}

void SearchPane::LayoutResults()
{
	mResultArea->LayoutImages();

	if (mResultArea->NumUniqueCards() == 0)
	{
		mError->resize(mError->parentWidget()->width() - 2 * Padding,
			mError->parentWidget()->height() - 2 * Padding);
		mError->show();
	}
	else
		mError->hide();
}
