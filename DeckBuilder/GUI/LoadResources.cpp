#include <QResource>

void LoadResources()
{
	static bool initialized = false;
	if (!initialized)
	{
		Q_INIT_RESOURCE(Resources);
		initialized = true;
	}
}
