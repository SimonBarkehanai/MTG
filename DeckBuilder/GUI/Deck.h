#pragma once

#include <map>

#include <QLineEdit>
#include <QSplitter>

#include "MainDeck.h"
#include "Sideboard.h"

class Deck : public QWidget
{
	Q_OBJECT
public:
	explicit Deck(Downloader* downloader,
		const QString&        name   = "",
		QWidget*              parent = nullptr,
		Qt::WindowFlags       f      = {});

	template <typename T>
	void LoadDeck(T&& file) requires(
		!std::is_same_v<std::remove_cvref_t<T>,
			nlohmann::json> && !std::is_same_v<std::remove_cvref_t<T>, std::string_view>)
	{
		LoadDeck(std::string_view(std::forward<T>(file)));
	}
	void LoadDeck(std::string_view file);
	void LoadDeck(nlohmann::json src);

	void ClearDeck()
	{
		mMain->ClearDeck();
		mSide->ClearDeck();
	}

	explicit operator nlohmann::json() const;

	MainDeck* Main()
	{
		return mMain;
	}
	Sideboard* Side()
	{
		return mSide;
	}

	enum class Board
	{
		MainDeck,
		Sideboard
	};

	template <typename CardT>
	void AddCard(CardT&& card, Board board = Board::MainDeck)
	{
		switch (board)
		{
		case Board::MainDeck:
			mMain->AddCard(std::forward<CardT>(card));
			break;
		case Board::Sideboard:
			mSide->AddCard(std::forward<CardT>(card));
			break;
		}
	}

	void ReDownload()
	{
		mMain->ReDownload();
		mSide->ReDownload();
	}

	void SetSideboardVisible(bool visible)
	{
		mSide->setVisible(visible);
	}
	bool GetSideboardVisible() const
	{
		return mSide->isVisible();
	}

protected:
	void resizeEvent(QResizeEvent* e) override;

private:
	QLineEdit* mName     = new QLineEdit(this);
	QSplitter* mSplitter = new QSplitter(this);
	MainDeck*  mMain;
	Sideboard* mSide;

	void SetupMainDeckActions();
	void SetupSideboardActions();

	inline static constexpr auto LabelHeight = 20, Spacing = 5;
};
