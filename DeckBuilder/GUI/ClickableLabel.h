#pragma once
#include <QLabel>

class ClickableLabel : public QLabel
{
	Q_OBJECT
public:
	ClickableLabel(QWidget* parent = nullptr, Qt::WindowFlags f = {});

signals:
	void clicked();

protected:
	void mousePressEvent(QMouseEvent* e) override;
};
