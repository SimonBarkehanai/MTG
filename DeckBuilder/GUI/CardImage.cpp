#include "CardImage.h"

#include <chrono>   // for milliseconds
#include <cmath>    // for sqrt
#include <utility>  // for move

#include <QBitmap>         // for QBitmap
#include <QFont>           // for QFont
#include <QPainter>        // for QPainter
#include <QPixmap>         // for QPixmap
#include <QPointF>         // for QPointF
#include <QStringLiteral>  // for QStringLiteral
#include <QTimer>          // for QTimer

#include "LoadResources.h"

template <int Width, int Height>
auto MakeBorderMap()
{
	static const auto map = [] {
		QPolygonF     points;
		constexpr int BorderRadiusX = Width * .0475,
					  BorderRadiusY = Height * .035, BorderRadius = BorderRadiusX;
		static_assert(BorderRadiusX == BorderRadiusY,
			"height and width ratio "
			"should allow for a "
			"circular "
			"border");
		for (int i = 0; i <= BorderRadius; ++i)
		{
			double x = i;
			double y = BorderRadius
			           - std::sqrt(BorderRadius * BorderRadius
								   - (BorderRadius - i) * (BorderRadius - i));
			points.append(QPointF(x, y));
		}
		for (int i = BorderRadius; i >= 0; --i)
		{
			double x = Width - i;
			double y = BorderRadius
			           - std::sqrt(BorderRadius * BorderRadius
								   - (BorderRadius - i) * (BorderRadius - i));
			points.append(QPointF(x, y));
		}
		for (int i = 0; i <= BorderRadius; ++i)
		{
			double x = Width - i;
			double y = Height - BorderRadius
			           + std::sqrt(BorderRadius * BorderRadius
								   - (BorderRadius - i) * (BorderRadius - i));
			points.append(QPointF(x, y));
		}
		for (int i = BorderRadius; i >= 0; --i)
		{
			double x = i;
			double y = Height - BorderRadius
			           + std::sqrt(BorderRadius * BorderRadius
								   - (BorderRadius - i) * (BorderRadius - i));
			points.append(QPointF(x, y));
		}
		auto map = QBitmap(Width, Height);
		map.fill(Qt::color0);

		QPainter painter(&map);
		painter.setBrush(Qt::color1);
		painter.drawPolygon(points);

		return map;
	}();
	return &map;
}

CardImage::CardImage(
	std::string front, int quantity, QWidget* parent, Qt::WindowFlags flags)
	: CardImage(std::move(front), "", quantity, parent, flags, Face::SingleFace)
{
}

CardImage::CardImage(std::string front,
	std::string                  back,
	int                          quantity,
	QWidget*                     parent,
	Qt::WindowFlags              flags,
	Face                         face)
	: ClickableLabel(parent, flags), mFace(face)
{
	if (BorderMap == nullptr)
		BorderMap = MakeBorderMap<Width, Height>();

	SetQuantity(quantity);

	mQuantity->setGeometry(Width - QuantityWidth - BorderRight,
		QuantityBorderTop,
		QuantityWidth,
		QuantityHeight);
	auto font = mQuantity->font();
	font.setPointSize(10);
	mQuantity->setFont(font);
	mQuantity->setStyleSheet(
		QStringLiteral("color:lightgrey;border-radius:3px;"
					   "padding-right:0.5px;"
					   "background-color:qlineargradient("
					   "spread:pad,x1:0,y1:0,x2:1,y2:0,"
					   "stop:0 rgba(120,120,120,50),"
					   "stop:1 rgba(80,80,80,250));"));
	mQuantity->setAlignment(
		Qt::AlignmentFlag::AlignVCenter | Qt::AlignmentFlag::AlignRight);

	mFlip->setGeometry(Width - FlipSize - BorderRight,
		QuantityBorderTop + QuantityHeight + Spacing,
		FlipSize,
		FlipSize);
	mFlip->setPixmap(QPixmap(QStringLiteral(":/Icons/Flip.png")));
	mFlip->setVisible(face != Face::SingleFace);

	if (back.empty())
		SetPicture(std::move(front));
	else
		SetPicture(std::move(front), std::move(back));

	connect(mFlip, &ClickableLabel::clicked, this, &CardImage::Flip);

	resize(Width, Height);
}

void CardImage::SetPicture(std::string front)
{
	mFace = Face::SingleFace;
	SetPicture(std::move(front), Face::SingleFace);
}

void CardImage::SetPicture(std::string front, std::string back)
{
	setVisible(mFace != Face::SingleFace);
	mFace = Face::Front;
	SetPicture(std::move(front), Face::Front);
	SetPicture(std::move(back), Face::Back);
}

void CardImage::SetQuantity(int newQuantity)
{
	if (newQuantity <= 0)
		mQuantity->hide();
	else
	{
		mQuantity->setText('x' + QString::number(newQuantity));
		mQuantity->show();
	}
}

void CardImage::enterEvent(QEvent* e)
{
	emit Hovered();

	QLabel::enterEvent(e);
}

void CardImage::leaveEvent(QEvent* e)
{
	emit UnHovered();

	QLabel::leaveEvent(e);
}

void CardImage::Flip()
{
	switch (mFace)
	{
	case Face::Front:
		mFace = Face::Back;
		setPixmap(mBack);
		break;
	case Face::Back:
		mFace = Face::Front;
		setPixmap(mFront);
		break;
	case Face::SingleFace:
		break;  // do nothing
	}
}

void CardImage::SetPicture(std::string src, Face face)
{
	QPixmap pix(QString::fromStdString(src));
	if (pix.isNull())
	{
		if (face == Face::Front)
			mFront = QPixmap();
		else
			mBack = QPixmap();
		setPixmap(QPixmap());

		QTimer::singleShot(std::chrono::milliseconds(250),
			this,
			[this, src = std::move(src), face = face]() mutable {
				SetPicture(std::move(src), face);
			});
	}
	else
	{
		auto scaled = pix.scaled(
			Width, Height, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
		scaled.setMask(*BorderMap);

		if (face == Face::Back)
			mBack = scaled;
		else
			mFront = scaled;

		ShowPicture();
	}
}

void CardImage::ShowPicture()
{
	setPixmap(mFace != Face::Back ? mFront : mBack);
	mFlip->setVisible(mFace != Face::SingleFace);
}
