#pragma once
#include <Downloader.h>

#include "MainDeck.h"

class Sideboard : public MainDeck
{
	Q_OBJECT
public:
	explicit Sideboard(
		Downloader* downloader, QWidget* parent = nullptr, Qt::WindowFlags f = {});

	virtual void LoadDeck(std::string_view file);
	using MainDeck::LoadDeck;

	// explicit operator nlohmann::json() const;

	// CardGrid* const       Grid() { return mGrid; }
	// const CardGrid* const Grid() const { return mGrid; }

signals:
	// void CardClicked(int index);

protected:
	// void resizeEvent(QResizeEvent* e) override;

private:
	// CardGrid*             mGrid = new CardGrid(this);
	// Downloader* mDownloader;
};
