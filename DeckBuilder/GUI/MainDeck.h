#pragma once
#include <Downloader.h>

#include "CardGrid.h"

class MainDeck : public QWidget
{
	Q_OBJECT
public:
	explicit MainDeck(
		Downloader* downloader, QWidget* parent = nullptr, Qt::WindowFlags f = {});

	void AddCard(const Card& card);
	void AddCard(Card&& card);
	void RemoveCard(const Card& card);
	void RemoveCard(Card&& card);

	[[nodiscard]] Card CardAt(int index);

	virtual void LoadDeck(std::string_view file);
	virtual void LoadDeck(const char* file)
	{
		LoadDeck(std::string_view(file));
	}
	virtual void LoadDeck(const nlohmann::json& src);

	void ClearDeck();

	explicit operator nlohmann::json() const;

	CardGrid* const Grid()
	{
		return mGrid;
	}
	const CardGrid* const Grid() const
	{
		return mGrid;
	}

	void ReDownload();

signals:
	void CardClicked(int index);

protected:
	void resizeEvent(QResizeEvent* e) override;

private:
	CardGrid*   mGrid = new CardGrid(this);
	Downloader* mDownloader;
};
