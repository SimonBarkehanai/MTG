#if __has_include(<sdkddkver.h>)
#	include <sdkddkver.h>
#endif

#include <algorithm>
#include <array>
#include <fstream>
#include <iostream>
#include <string>
#include <string_view>
#include <vector>

#include <boost/beast.hpp>
#include <boost/beast/ssl.hpp>
#include <nlohmann/json.hpp>

#include <QApplication>
#include <QMainWindow>
#include <QStyle>

#include "CardImage.h"
#include "Deck.h"
#include "MainDeck.h"
#include "MainWindow.h"
#include "SearchPane.h"
#include "Sideboard.h"

using namespace std::literals;
namespace beast = boost::beast;
namespace http  = beast::http;
namespace net   = boost::asio;
namespace ssl   = net::ssl;
using tcp       = net::ip::tcp;

#define IMG_FOLDER R"(C:\Users\simon\AppData\Local\DeckBuilder\CardPics\sld\)"
constexpr auto deckfile = R"(C:\Users\simon\Desktop\deck.json)";

constexpr std::array images{
	IMG_FOLDER "Acidic Slime.134.jpg",
	IMG_FOLDER "Ajani Steadfast.87.jpg",
	IMG_FOLDER "Arahbo, Roar of the World.25.jpg",
	IMG_FOLDER "Arcbound Ravager.56.jpg",
	IMG_FOLDER "Birds of Paradise.92.jpg",
	IMG_FOLDER "Captain Sisay.51.jpg",
	IMG_FOLDER "Dig Through Time.97.jpg",
	IMG_FOLDER "Forest.67.jpg",
	IMG_FOLDER "Goblin Bushwhacker.17.jpg",
	IMG_FOLDER "Golgari Thug.7.jpg",
	IMG_FOLDER "Ink-Eyes, Servant of Oni.33.jpg",
	IMG_FOLDER "Meren of Clan Nel Toth.52.jpg",
};
std::array<CardImage*, images.size()> cardImages;

void addImg(QWidget* parent)
{
	static int index  = 0;
	CardImage* img    = new CardImage(images[index], 1, parent);
	cardImages[index] = img;
	img->move(10 + 210 * (index % 6), 10 + 60 * (index / 6));
	++index;

	QObject::connect(img, &CardImage::Hovered, img, &QWidget::raise);
}

auto CardImageTest()
{
	auto w = std::make_unique<QMainWindow>();
	for (int i = 0; i < 12; ++i)
		addImg(w.get());
	w->resize(300, 380);
	w->show();
	return w;
}

auto SearchPaneTest(Downloader* downloader)
{
	auto p = std::make_unique<SearchPane>(downloader);

	p->resize(800, 600);
	p->show();
	return p;
}

auto MainDeckTest(Downloader* downloader)
{
	auto p = std::make_unique<MainDeck>(downloader);

	p->LoadDeck(deckfile);

	p->resize(800, 600);
	p->show();
	return p;
}

auto DeckTest(Downloader* downloader)
{
	auto p = std::make_unique<Deck>(downloader);

	p->LoadDeck(deckfile);

	p->resize(800, 600);
	p->show();
	return p;
}

auto MainWindowTest(Downloader* downloader)
{
	auto p = std::make_unique<MainWindow>(downloader);

	p->LoadDeck(deckfile);

	p->resize(1200, 600);
	p->show();
	return p;
}

auto SideboardTest(Downloader* downloader)
{
	auto p = std::make_unique<Sideboard>(downloader);
	p->LoadDeck(deckfile);
	p->resize(1200, 600);
	p->show();
	return p;
}

auto sets()
{
	const std::array bad = { "con"sv,
		"prn"sv,
		"aux"sv,
		"nul"sv,
		"com1"sv,
		"com2"sv,
		"com3"sv,
		"com4"sv,
		"com5"sv,
		"com6"sv,
		"com7"sv,
		"com8"sv,
		"com9"sv,
		"lpt1"sv,
		"lpt2"sv,
		"lpt3"sv,
		"lpt4"sv,
		"lpt5"sv,
		"lpt6"sv,
		"lpt7"sv,
		"lpt8"sv,
		"lpt9"sv };

	constexpr std::string_view host = "api.scryfall.com", port = "443",
							   target = "/sets";
	constexpr auto version            = 11;
	// The io_context is required for all I/O
	net::io_context ioc;

	// The SSL context is required, and holds certificates
	ssl::context ctx(ssl::context::sslv23_client);

	// This holds the root certificate used for verification
	ctx.set_default_verify_paths();

	// These objects perform our I/O
	tcp::resolver                        resolver(ioc);
	beast::ssl_stream<beast::tcp_stream> stream(ioc, ctx);

	// Set SNI Hostname (many hosts need this to handshake successfully)
	if (!SSL_set_tlsext_host_name(stream.native_handle(), host.data()))
	{
		beast::error_code ec{ static_cast<int>(::ERR_get_error()),
			net::error::get_ssl_category() };
		throw beast::system_error{ ec };
	}

	// Look up the domain name
	auto const results = resolver.resolve(host, port);

	// Make the connection on the IP address we get from a lookup
	beast::get_lowest_layer(stream).connect(results);

	// Perform the SSL handshake
	stream.handshake(ssl::stream_base::client);

	// Set up an HTTP GET request message
	http::request<http::string_body> req{ http::verb::get, target.data(), version };
	req.set(http::field::host, host.data());
	req.set(http::field::user_agent, BOOST_BEAST_VERSION_STRING);

	// Send the HTTP request to the remote host
	http::write(stream, req);

	// This buffer is used for reading and must be persisted
	beast::flat_buffer buffer;

	// Declare a container to hold the response
	http::response<http::string_body> res;

	// Receive the HTTP response
	http::read(stream, buffer, res);

	auto                     json = nlohmann::json::parse(res.body());
	std::vector<std::string> sets, disallowedSets;
	for (const auto& elem : json["data"])
	{
		sets.push_back(elem["code"].get<std::string>());
	}

	for (const auto& set : sets)
	{
		if (std::any_of(
				bad.begin(), bad.end(), [&set](auto val) { return val == set; }))
			disallowedSets.push_back(set);
	}

	if (disallowedSets != std::vector{ "con"s })
		throw "New sets using restricted names!";
}

int main(int argc, char* argv[])
{
	QApplication app(argc, argv);
	Downloader   downloader(5);

	auto p = MainWindowTest(&downloader);

	app.exec();

	std::ofstream(deckfile) << static_cast<nlohmann::json>(*p);
}
