#pragma once
#include <QAction>
#include <QMainWindow>
#include <QMenu>
#include <QMenuBar>
#include <QSplitter>

#include "Deck.h"
#include "SearchPane.h"

class MainWindow : public QMainWindow
{
public:
	explicit MainWindow(
		Downloader* downloader, QWidget* parent = nullptr, Qt::WindowFlags f = {});

	template <typename... Args>
	auto LoadDeck(Args&&... args)
	{
		return mDeck->LoadDeck(std::forward<Args>(args)...);
	}

	Deck* GetDeck()
	{
		return mDeck;
	}
	const Deck* GetDeck() const
	{
		return mDeck;
	}

	SearchPane* GetSearch()
	{
		return mSearch;
	}
	const SearchPane* GetSearch() const
	{
		return mSearch;
	}

	explicit operator nlohmann::json() const
	{
		return static_cast<nlohmann::json>(*mDeck);
	}

protected:
	void resizeEvent(QResizeEvent* e) override;
	void showEvent(QShowEvent* e) override;

private slots:
	void ImageSizeChange(int newIndex);
	void Save();
	void SaveAs();
	void Open();
	void ShowSideboard(bool show);
	void ImportArena();
	void ExportArena();

	void RestrictLegality(bool enabled);
	void EnforceColorID(bool enabled);

	void FormatChange(int newIndex);

	void SelectPicSaveLocation();

private:
	QSplitter*  mSplitter = new QSplitter(this);
	Deck*       mDeck;
	SearchPane* mSearch;
	QLabel*     mImportExportNotifier = new QLabel(this);

	QMenuBar* mMenuBar    = new QMenuBar(this);
	QMenu *   mFileMenu   = new QMenu("&File", mMenuBar),
		  *mEditMenu      = new QMenu("&Edit", mMenuBar),
		  *mSearchMenu    = new QMenu("&Search", mMenuBar),
		  *mImageMenu     = new QMenu("&Images", mMenuBar),
		  *mFormatMenu    = new QMenu("&Format", mMenuBar),
		  *mImageSizeMenu = new QMenu("&Size", mImageMenu);

	QAction *mNewAct           = new QAction("&New...", this),
			*mSaveAct          = new QAction("&Save", this),
			*mSaveAsAct        = new QAction("Save &As...", this),
			*mOpenAct          = new QAction("&Open...", this),
			*mImportArenaAct   = new QAction("&Import From MTG Arena", this),
			*mExportArenaAct   = new QAction("&Export To MTG Arena", this),
			*mQuitAct          = new QAction("&Quit", this),
			*mShowSideboardAct = new QAction("Show &Sideboard", this),
			*mSuggestBasicsAct = new QAction("Suggest &Basics", this),
			*mGroupsAct        = new QAction("Set &Groups...", this),
			*mSortAct          = new QAction("S&ort by...", this),
			*mShowSchemeAct    = new QAction("Show S&cheme Deck", this),
			*mShowPlanarAct    = new QAction("Show &Planar Deck", this),
			*mShowVanguardAct  = new QAction("Show &Vanguard Deck", this),
			*mRestrictLegalityAct =
				new QAction("Restrict to Format &Legal Cards", this),
			*mEnforceColorIDAct =
				new QAction("Enforce &Commander Color Identity", this),
			*mSaveLocationAct = new QAction("Edit Save &Location", this);

	// small, normal, large, PNG
	std::array<QAction*, 4> mImageSizeActs{
		new QAction("&Small", this),
		new QAction("&Normal", this),
		new QAction("&Large", this),
		new QAction("&PNG", this),
	};

	enum class Format
	{
		Freeform = 0,
		Standard,
		Historic,
		Pioneer,
		Modern,
		Legacy,
		Vintage,
		Commander,
		Brawl,
		Pauper,
		FutureStandard,
		DuelCommander,
		PennyDreadful,
		OldSchool,
	};

	constexpr static std::array<std::string_view, 14> FormatNames{
		"Freeform",
		"Standard",
		"Historic",
		"Pioneer",
		"Modern",
		"Legacy",
		"Vintage",
		"Commander",
		"Brawl",
		"Pauper",
		"Future",
		"Duel",
		"Penny",
		"OldSchool",
	};

	std::array<QAction*, 14> mFormatActs{
		new QAction("F&reeform", this),
		new QAction("&Standard", this),
		new QAction("&Historic", this),
		new QAction("&Pioneer", this),
		new QAction("&Modern", this),
		new QAction("&Legacy", this),
		new QAction("&Vintage", this),
		new QAction("&Commander", this),
		new QAction("&Brawl", this),
		new QAction("P&auper", this),
		new QAction("&Future Standard", this),
		new QAction("D&uel Commander", this),
		new QAction("Penny &Dreadful", this),
		new QAction("&Old School", this),
	};

	Format mFormat  = Format::Freeform;
	Color  mColorID = Color::Unspecified;

	Downloader* mDownloader;
	std::string mSavePath;

	using set_string = std::array<char, 5>;
	const std::vector<std::pair<set_string, set_string>> MTGASetToCannonical;

	void SetupSearchActions();
	void SetupMenus();

	static constexpr auto Padding = 10, Spacing = 10;
};
