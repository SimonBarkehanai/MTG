#include "MainWindow.h"

#include <concepts>
#include <filesystem>
#include <fstream>

#include <QAction>
#include <QApplication>
#include <QClipboard>
#include <QFileDialog>
#include <QThread>
#include <QTimer>

#include "StringViewIsh.h"

namespace fs = std::filesystem;

auto GetMTGASetsToCanonnical(Searcher& searcher)
{
	std::vector<std::pair<std::array<char, 5>, std::array<char, 5>>> res;
	auto json = nlohmann::json::parse(searcher.GetTarget("/sets"));

	std::for_each(
		begin(json["data"]), end(json["data"]), [&res](const nlohmann::json& set) {
			if (auto it = set.find("arena_code"); it != end(set))
			{
				auto arena      = it->get<std::string_view>(),
					 cannonical = set["code"].get<std::string_view>();
				if (arena != cannonical)
				{
					std::pair<std::array<char, 5>, std::array<char, 5>> toAdd;
					std::copy(begin(arena), end(arena), begin(toAdd.first));
					std::copy(
						begin(cannonical), end(cannonical), begin(toAdd.second));
					res.push_back(toAdd);
				}
			}
		});
	return res;
}

MainWindow::MainWindow(Downloader* downloader, QWidget* parent, Qt::WindowFlags f)
	: QMainWindow(parent, f)
	, mDeck(new Deck(downloader))
	, mSearch(new SearchPane(downloader))
	, mDownloader(downloader)
	, MTGASetToCannonical(GetMTGASetsToCanonnical(mSearch->GetSearcher()))
{
	setCentralWidget(new QWidget);
	mSplitter = new QSplitter(centralWidget());
	mSplitter->addWidget(mDeck);
	mSplitter->addWidget(mSearch);

	QObject::connect(mSearch,
		&SearchPane::CardClicked,
		mDeck,
		[deck = mDeck, search = mSearch](
			int index) { deck->AddCard(search->CardAt(index)); });

	SetupSearchActions();

	SetupMenus();

	mImageSizeActs[static_cast<std::underlying_type_t<Downloader::ImageSize>>(
					   mDownloader->GetImageSize())]
		->setChecked(true);
	mFormatActs[static_cast<std::underlying_type_t<Format>>(mFormat)]->setChecked(
		true);

	const auto size = mImportExportNotifier->fontMetrics().size(
						  {}, "Successfully imported deck from clipboard")
	                  + QSize{ 20, 10 };
	mImportExportNotifier->setGeometry(
		QRect{ QPoint{ (centralWidget()->width() - size.width()) / 2,
				   centralWidget()->height() - size.height() - Padding },
			size });
	mImportExportNotifier->setStyleSheet(
		"background-color:rgba(128,128,128,"
		"100);border-radius:1px");
	mImportExportNotifier->setAlignment(
		Qt::AlignmentFlag::AlignHCenter | Qt::AlignmentFlag::AlignVCenter);
}

void MainWindow::resizeEvent(QResizeEvent* e)
{
	QMainWindow::resizeEvent(e);
	mSplitter->setGeometry(Padding,
		Padding,
		centralWidget()->width() - 2 * Padding,
		centralWidget()->height() - 2 * Padding);
}

void MainWindow::showEvent(QShowEvent* e)
{
	QMainWindow::showEvent(e);
	mShowSideboardAct->setChecked(mDeck->GetSideboardVisible());
}

void MainWindow::ImageSizeChange(int newIndex)
{
	for (int i = 0; i < mImageSizeActs.size(); ++i)
		mImageSizeActs[i]->setChecked(i == newIndex);

	static_assert(
		Downloader::ImageSize::Small == static_cast<Downloader::ImageSize>(0)
			&& Downloader::ImageSize::Normal
				   == static_cast<Downloader::ImageSize>(1)
			&& Downloader::ImageSize::Large == static_cast<Downloader::ImageSize>(2)
			&& Downloader::ImageSize::PNG == static_cast<Downloader::ImageSize>(3),
		"ImageSizeChange requires ImageSizes enum to be in the order { Small, "
		"Normal, Large, PNG } starting at 0 to allow casting to ImageSize");

	mDownloader->SetImageSize(static_cast<Downloader::ImageSize>(newIndex));

	mDeck->ReDownload();
	mSearch->ReDownload();
}

void MainWindow::Save()
{
	if (mSavePath.empty())
		SaveAs();
	std::ofstream(mSavePath) << static_cast<nlohmann::json>(*this);
}

void MainWindow::SaveAs()
{
	mSavePath = QFileDialog::getSaveFileName(this,
		"Save Deck As",
		QString::fromStdString(mSavePath),
		"Deck Builder Deck Files (*.dbd)")
	                .toStdString();
	if (mSavePath.empty())
		return;
	else
		Save();
}

void MainWindow::Open()
{
	const auto file = QFileDialog::getOpenFileName(this,
		"Open Deck",
		QString::fromStdString(mSavePath),
		"Deck Builder Deck Files (*.dbd);;Simple Deck File (*.txt)")
	                      .toStdString();

	const auto ext = file.substr(file.rfind('.') + 1);

	if (ext == "dbd")
	{
		LoadDeck(file);
		mSavePath = file;
		return;
	}
	else if (ext == "txt")
	{
		mDeck->ClearDeck();
		mSearch->StopSearching();

		std::ifstream fstr(file);
		Deck::Board   board = Deck::Board::MainDeck;
		std::string   line;
		while (std::getline(fstr, line))
		{
			if (line.empty())
				board = Deck::Board::Sideboard;

			else
			{
				int quantity = 1;
				if (line.front() >= '0' && line.front() <= '9')
				{
					if (auto it = std::find_if(begin(line),
							end(line),
							[](char ch) { return ch != ' ' && ch != 'x'; });
						it != end(line))
					{
						std::from_chars(line.c_str(), &*it, quantity);
						line = line.substr(line.find(' ') + 1);
					}
					else
						continue;
				}

				mSearch->GetSearcher().Search('!' + std::move(line));
				mSearch->GetSearcher().WaitForFinish();

				if (mSearch->GetSearcher().Succeeded())
				{
					auto card = std::move(mSearch->GetSearcher().ResultAt(0));
					card.SetQuantity(quantity);
					mDeck->AddCard(std::move(card), board);
				}
			}
		}
	}
}

void MainWindow::ShowSideboard(bool show)
{
	mDeck->SetSideboardVisible(show);
}

template <typename T, typename StrT>
concept StringViewIshFunc = StringViewIsh<StrT> && requires(T func, StrT s)
{
	func(s);
};

template <StringViewIsh StrT, StringViewIshFunc<StrT> Func>
auto for_each_line(StrT&& string, Func&& f)
{
	std::size_t pos = string.find('\n'), oldPos = 0;
	f(string.substr(oldPos, pos));

	while (pos < string.size())
	{
		oldPos = pos + 1;
		pos    = string.find('\n', pos + 1);
		f(string.substr(oldPos, pos - oldPos));
	}

	return f;
}

void MainWindow::ImportArena()
{
	mDeck->ClearDeck();
	mSearch->StopSearching();

	Deck::Board board        = Deck::Board::MainDeck;
	bool        knownSection = false;

	for_each_line(QApplication::clipboard()->text().toStdString(),
		[this, &board, &knownSection](std::string_view line) {
			if (line.empty())
				return;
			if (line.front() < '0' || line.front() > '9')
			{
				if (line == "Deck")
				{
					board        = Deck::Board::MainDeck;
					knownSection = true;
				}
				else if (line == "Sideboard")
				{
					board        = Deck::Board::Sideboard;
					knownSection = true;
				}
				else
					knownSection = false;
			}
			else if (knownSection)
			{
				int        quantity;
				const auto firstSpace = line.find(' '),
						   openParen  = line.find('('),
						   closeParen = line.find(')');
				std::from_chars(line.data(), line.data() + firstSpace, quantity);
				auto set = std::string(line.substr(
						 openParen + 1, closeParen - (openParen + 1))),
					 cn  = std::string(line.substr(closeParen + 2));

				if (auto it = std::find_if(begin(MTGASetToCannonical),
						end(MTGASetToCannonical),
						[set = std::string_view(set)](
							const std::pair<std::array<char, 5>, std::array<char, 5>>& mapping) {
							return std::string_view(mapping.first.data(), 5)
				                .starts_with(set);
						});
					it != end(MTGASetToCannonical))
				{
					if (it->second.back() == '\0')
						set = it->second.data();
					else
						set.assign(it->second.data(), 5);
				}

				mSearch->GetSearcher().Search(
					"s:" + std::move(set) + " cn:" + std::move(cn));
				mSearch->GetSearcher().WaitForFinish();

				if (mSearch->GetSearcher().Succeeded())
				{
					auto card = std::move(mSearch->GetSearcher().ResultAt(0));
					card.SetQuantity(quantity);
					mDeck->AddCard(std::move(card), board);
				}
			}
		});
	mImportExportNotifier->setText("Successfully imported deck from clipboard");
	QTimer::singleShot(
		std::chrono::seconds(5), mImportExportNotifier, &QLabel::hide);
}

void MainWindow::ExportArena()
{
	nlohmann::json json(*mDeck);
	std::string    exportStr("Deck\n");

	std::for_each(begin(json["main"]),
		end(json["main"]),
		[&exportStr](const nlohmann::json& src) {
			const auto& val =
				src.type() == nlohmann::json::value_t::object ? src : src[0];

			auto quantity = val.type() == nlohmann::json::value_t::object
		                        ? val["quantity"].get<int>()
		                        : val[0]["quantity"].get<int>();
			char buf[4]   = {};
			// fills up to the first three characters in buf with quantity and
		    // sets the character after the last used in the number to a space
			*std::to_chars(std::begin(buf), std::end(buf) - 1, quantity).ptr = ' ';
			exportStr += buf + val["name"].get<std::string>() + '\n';
		});

	exportStr += "\nSideboard\n";

	std::for_each(begin(json["side"]),
		end(json["side"]),
		[&exportStr](const nlohmann::json& val) {
			auto quantity = val["quantity"].get<int>();
			char buf[4]   = {};
			// fills up to the first three characters in buf with quantity and
		    // sets the character after the last used in the number to a space
			*std::to_chars(std::begin(buf), std::end(buf) - 1, quantity).ptr = ' ';
			exportStr += buf + val["name"].get<std::string>() + '\n';
		});

	auto        text      = QString::fromStdString(exportStr);
	QClipboard* clipboard = QApplication::clipboard();
	clipboard->setText(text, QClipboard::Clipboard);

	if (clipboard->supportsSelection())
		clipboard->setText(text, QClipboard::Selection);

	mImportExportNotifier->setText("Decklist copied to clipboard");
	QTimer::singleShot(
		std::chrono::seconds(5), mImportExportNotifier, &QLabel::hide);
}

void MainWindow::RestrictLegality(bool enabled)
{
	auto& args = mSearch->GetExtraArgs();

	if (auto it = std::find_if(cbegin(args),
			cend(args),
			[](std::string_view str) { return str.starts_with("f:"); });
		it != cend(args))
		args.erase(it);

	if (enabled && mFormat != Format::Freeform)
		mSearch->AddExtraArg(
			"f:"
			+ static_cast<std::string>(
				FormatNames[static_cast<std::underlying_type_t<Format>>(mFormat)]));
}

void MainWindow::EnforceColorID(bool enabled)
{
	auto& args = mSearch->GetExtraArgs();

	if (auto it = std::find_if(cbegin(args),
			cend(args),
			[](std::string_view str) { return str.starts_with("id"); });
		it != cend(args))
		args.erase(it);

	if (enabled && mColorID != Color::Unspecified)
		mSearch->AddExtraArg("id<=" + StringForColors(mColorID));
}

void MainWindow::FormatChange(int newIndex)
{
	for (int i = 0; i < mFormatActs.size(); ++i)
		mFormatActs[i]->setChecked(i == newIndex);

	mFormat = static_cast<Format>(newIndex);

	RestrictLegality(mRestrictLegalityAct->isChecked());

	static_assert(Format::Freeform == static_cast<Format>(0)
					  && Format::Standard == static_cast<Format>(1)
					  && Format::Historic == static_cast<Format>(2)
					  && Format::Pioneer == static_cast<Format>(3)
					  && Format::Modern == static_cast<Format>(4)
					  && Format::Legacy == static_cast<Format>(5)
					  && Format::Vintage == static_cast<Format>(6)
					  && Format::Commander == static_cast<Format>(7)
					  && Format::Brawl == static_cast<Format>(8)
					  && Format::Pauper == static_cast<Format>(9)
					  && Format::FutureStandard == static_cast<Format>(10)
					  && Format::DuelCommander == static_cast<Format>(11)
					  && Format::PennyDreadful == static_cast<Format>(12)
					  && Format::OldSchool == static_cast<Format>(13),
		"FormatChange requires Formats enum to be in the order { Standard, "
		"Historic, Pioneer, Modern, Legacy, Vintage, Commander, Brawl, "
		"Pauper, "
		"FutureStandard, DuelCommander, PennyDreadful, OldSchool } with "
		"Standard starting at 0 and NoFormat to have a unique value to "
		"allow "
		"casting to Format");
}

void MainWindow::SelectPicSaveLocation()
{
	fs::path oldDir = mDownloader->GetSaveFolder();

	auto newDir = QFileDialog::getExistingDirectory(this,
		"Save Pictures To",
		QString::fromStdString(mDownloader->GetSaveFolder()))
	                  .toStdString();
	if (!newDir.empty())
	{
		if (!newDir.ends_with('/'))
			newDir += '/';
		fs::copy(oldDir, newDir, fs::copy_options::recursive);
		fs::remove_all(oldDir);
		mDownloader->SetSaveFolder(std::move(newDir));
	}
}

void MainWindow::SetupSearchActions()
{
	std::vector<QAction*>& actions = mSearch->Grid()->ContextMenuActions();
	actions.push_back(new QAction("Add to &Main Board", this));
	actions.push_back(new QAction("Add 4 to Main Board", this));
	actions.push_back(new QAction("Add to &Side Board", this));
	actions.push_back(new QAction("Add 4 to Side Board", this));

	QObject::connect(mSearch->Grid(),
		&CardGrid::ContextMenuActionTriggered,
		mDeck,
		[deck = mDeck, search = mSearch](int cardIndex, int actionIndex) {
			switch (actionIndex)
			{
			case 0:
				deck->AddCard(search->CardAt(cardIndex));
				break;
			case 1:
			{
				auto c = search->CardAt(cardIndex);
				c.SetQuantity(4);
				deck->AddCard(std::move(c));
				break;
			}
			case 2:
				deck->AddCard(search->CardAt(cardIndex), Deck::Board::Sideboard);
				break;
			case 3:
			{
				auto c = search->CardAt(cardIndex);
				c.SetQuantity(4);
				deck->AddCard(std::move(c), Deck::Board::Sideboard);
				break;
			}

			default:
				std::cout << "Unhandled Search Action (index = " << actionIndex
						  << ")\n";
			}
		});
}

void MainWindow::SetupMenus()
{
	mShowSideboardAct->setCheckable(true);
	mSuggestBasicsAct->setCheckable(true);
	mShowSchemeAct->setCheckable(true);
	mShowPlanarAct->setCheckable(true);
	mShowVanguardAct->setCheckable(true);

	mRestrictLegalityAct->setCheckable(true);
	mEnforceColorIDAct->setCheckable(true);

	setMenuBar(mMenuBar);

	mMenuBar->addAction(mFileMenu->menuAction());
	mMenuBar->addAction(mEditMenu->menuAction());
	mMenuBar->addAction(mSearchMenu->menuAction());
	mMenuBar->addAction(mImageMenu->menuAction());

	mFileMenu->addAction(mNewAct);
	mFileMenu->addAction(mOpenAct);
	mFileMenu->addSeparator();
	mFileMenu->addAction(mSaveAct);
	mFileMenu->addAction(mSaveAsAct);
	mFileMenu->addSeparator();
	mFileMenu->addAction(mImportArenaAct);
	mFileMenu->addAction(mExportArenaAct);
	mFileMenu->addSeparator();
	mFileMenu->addAction(mQuitAct);

	mEditMenu->addAction(mShowSideboardAct);
	mEditMenu->addAction(mSuggestBasicsAct);
	mEditMenu->addSeparator();
	mEditMenu->addAction(mFormatMenu->menuAction());
	mEditMenu->addAction(mGroupsAct);
	mEditMenu->addAction(mSortAct);
	mEditMenu->addSeparator();
	mEditMenu->addAction(mShowSchemeAct);
	mEditMenu->addAction(mShowPlanarAct);
	mEditMenu->addAction(mShowVanguardAct);

	mSearchMenu->addAction(mRestrictLegalityAct);
	mSearchMenu->addAction(mEnforceColorIDAct);

	mImageMenu->addAction(mImageSizeMenu->menuAction());
	mImageMenu->addAction(mSaveLocationAct);

	mNewAct->setShortcut(QKeySequence::New);
	mSaveAct->setShortcut(QKeySequence::Save);
	mSaveAsAct->setShortcut(QKeySequence::SaveAs);
	mOpenAct->setShortcut(QKeySequence::Open);
	mQuitAct->setShortcut(QKeySequence::Quit);

	connect(mSaveAct, &QAction::triggered, this, &MainWindow::Save);
	connect(mSaveAsAct, &QAction::triggered, this, &MainWindow::SaveAs);
	connect(mOpenAct, &QAction::triggered, this, &MainWindow::Open);
	connect(mImportArenaAct, &QAction::triggered, this, &MainWindow::ImportArena);
	connect(mExportArenaAct, &QAction::triggered, this, &MainWindow::ExportArena);
	connect(mQuitAct, &QAction::triggered, this, &MainWindow::close);

	connect(mShowSideboardAct, &QAction::toggled, this, &MainWindow::ShowSideboard);
	// connect(mSuggestBasicsAct, &QAction::toggled, this,
	// &MainWindow::SuggestBasics); connect(mGroupsAct, &QAction::triggered,
	// this, &MainWindow::SelectGroups); connect(mSortAct, &QAction::triggered,
	// this, &MainWindow::SelectSortMode); connect(mShowSchemeAct,
	// &QAction::triggered, this, &MainWindow::ShowScheme);
	// connect(mShowPlanarAct, &QAction::triggered, this,
	// &MainWindow::ShowPlanar); connect(mShowVanguardAct, &QAction::triggered,
	// this, &MainWindow::ShowVanguard);

	connect(
		mRestrictLegalityAct, &QAction::toggled, this, &MainWindow::RestrictLegality);
	connect(
		mEnforceColorIDAct, &QAction::toggled, this, &MainWindow::EnforceColorID);

	connect(mSaveLocationAct,
		&QAction::triggered,
		this,
		&MainWindow::SelectPicSaveLocation);

	for (std::size_t i = 0; i < mImageSizeActs.size(); ++i)
	{
		mImageSizeActs[i]->setCheckable(true);
		mImageSizeMenu->addAction(mImageSizeActs[i]);
		connect(
			mImageSizeActs[i], &QAction::triggered, this, [this, i](bool checked) {
				// avoid redownloading if the user clicks an the current size
				if (checked)
					ImageSizeChange(i);
				else
					mImageSizeActs[i]->setChecked(true);
			});
	}

	for (std::size_t i = 0; i < mFormatActs.size(); ++i)
	{
		mFormatActs[i]->setCheckable(true);
		mFormatMenu->addAction(mFormatActs[i]);
		connect(mFormatActs[i], &QAction::triggered, this, [this, i](bool checked) {
			// avoid redownloading if the user clicks an the current format
			if (checked)
				FormatChange(i);
			else
				mFormatActs[i]->setChecked(true);
		});
	}
}