#pragma once

void LoadResources();

namespace
{
struct LoadResourcesImpl
{
	LoadResourcesImpl()
	{
		LoadResources();
	}
} unused;
}  // namespace
