#include "Deck.h"

#include <fstream>

#include <QAction>

Deck::Deck(
	Downloader* downloader, const QString& name, QWidget* parent, Qt::WindowFlags f)
	: QWidget(parent, f)
	, mMain(new MainDeck(downloader))
	, mSide(new Sideboard(downloader))
{
	mName->setText(name);
	mName->setPlaceholderText("New Deck");
	mName->setStyleSheet("background:transparent;border:none");

	auto font = mName->font();
	font.setPointSize(12);
	mName->setFont(font);

	mSplitter->setOrientation(Qt::Orientation::Vertical);
	mSplitter->addWidget(mMain);
	mSplitter->addWidget(mSide);

	QObject::connect(
		mMain, &MainDeck::CardClicked, mMain, [main = mMain](int index) {
			auto c = main->CardAt(index);
			c.SetQuantity(1);
			main->RemoveCard(std::move(c));
		});
	QObject::connect(
		mSide, &Sideboard::CardClicked, mSide, [side = mSide](int index) {
			auto c = side->CardAt(index);
			c.SetQuantity(1);
			side->RemoveCard(std::move(c));
		});

	SetupMainDeckActions();
	SetupSideboardActions();
}

void Deck::LoadDeck(std::string_view file)
{
	LoadDeck(nlohmann::json::parse(std::ifstream(file)));
}

void Deck::LoadDeck(nlohmann::json src)
{
	mMain->LoadDeck(src["main"]);
	mSide->LoadDeck(src["side"]);
	mName->setText(QString::fromStdString(
		*src["name"].get_ptr<nlohmann::json::string_t*>()));
}

void Deck::SetupMainDeckActions()
{
	std::vector<QAction*>& actions = mMain->Grid()->ContextMenuActions();
	actions.push_back(new QAction("Add one", this));
	actions.push_back(new QAction("Make four-of", this));
	actions.push_back(new QAction("Move one to Sideboard", this));
	actions.push_back(new QAction("Move all to Sideboard", this));
	actions.push_back(new QAction("Remove one", this));
	actions.push_back(new QAction("Remove all", this));

	QObject::connect(mMain->Grid(),
		&CardGrid::ContextMenuActionTriggered,
		mMain,
		[main = mMain, side = mSide](int cardIndex, int actionIndex) {
			switch (actionIndex)
			{
			case 0:
			{
				auto c = main->CardAt(cardIndex);
				c.SetQuantity(1);
				main->AddCard(std::move(c));
				break;
			}
			case 1:
			{
				auto c = main->CardAt(cardIndex);
				c.SetQuantity(4 - c.GetQuantity());
				main->AddCard(std::move(c));
				break;
			}
			case 2:
			{
				auto c = main->CardAt(cardIndex);
				c.SetQuantity(1);
				main->RemoveCard(c);
				side->AddCard(std::move(c));
				break;
			}
			case 3:
			{
				auto c = main->CardAt(cardIndex);
				main->RemoveCard(c);
				side->AddCard(std::move(c));
				break;
			}
			case 4:
			{
				auto c = main->CardAt(cardIndex);
				c.SetQuantity(1);
				main->RemoveCard(std::move(c));
				break;
			}
			case 5:
			{
				main->RemoveCard(main->CardAt(cardIndex));
				break;
			}
			default:
				std::cout << "Unhandled Main Deck Action (index = " << actionIndex
						  << ")\n";
			}
		});
}

void Deck::SetupSideboardActions()
{
	std::vector<QAction*>& actions = mSide->Grid()->ContextMenuActions();
	actions.push_back(new QAction("Add one", this));
	actions.push_back(new QAction("Make four-of", this));
	actions.push_back(new QAction("Move one to Main Deck", this));
	actions.push_back(new QAction("Move all to Main Deck", this));
	actions.push_back(new QAction("Remove one", this));
	actions.push_back(new QAction("Remove all", this));

	QObject::connect(mSide->Grid(),
		&CardGrid::ContextMenuActionTriggered,
		mSide,
		[main = mMain, side = mSide](int cardIndex, int actionIndex) {
			switch (actionIndex)
			{
			case 0:
			{
				auto c = side->CardAt(cardIndex);
				c.SetQuantity(1);
				side->AddCard(std::move(c));
				break;
			}
			case 1:
			{
				auto c = side->CardAt(cardIndex);
				c.SetQuantity(4 - c.GetQuantity());
				side->AddCard(std::move(c));
				break;
			}
			case 2:
			{
				auto c = side->CardAt(cardIndex);
				c.SetQuantity(1);
				side->RemoveCard(c);
				main->AddCard(std::move(c));
				break;
			}
			case 3:
			{
				auto c = side->CardAt(cardIndex);
				side->RemoveCard(c);
				main->AddCard(std::move(c));
				break;
			}
			case 4:
			{
				auto c = side->CardAt(cardIndex);
				c.SetQuantity(1);
				side->RemoveCard(std::move(c));
				break;
			}
			case 5:
			{
				side->RemoveCard(side->CardAt(cardIndex));
				break;
			}
			default:
				std::cout << "Unhandled Sideboard Action (index = " << actionIndex
						  << ")\n";
			}
		});
}

Deck::operator nlohmann::json() const
{
	return { { "name", mName->text().toStdString() },
		{ "main", static_cast<nlohmann::json>(*mMain) },
		{ "side", static_cast<nlohmann::json>(*mSide) } };
}

void Deck::resizeEvent(QResizeEvent* e)
{
	QWidget::resizeEvent(e);

	const auto     elemWidth = width();
	constexpr auto SplitterY = LabelHeight + Spacing;

	mName->setGeometry(0, 0, elemWidth, LabelHeight);
	mSplitter->setGeometry(0, SplitterY, elemWidth, height() - SplitterY);
}
