#include "MainDeck.h"

#include <concepts>
#include <fstream>

template <typename CardRefQualified>
CardCardImage MakeCardCardImage(
	CardRefQualified&& card, Downloader* downloader, QWidget* parent = nullptr) requires
	std::same_as<Card, std::remove_cvref_t<CardRefQualified>>
{
	CardImage* im;
	if (card.NumFaces == 1)
		im = new CardImage(
			downloader->FullPathFor(card.front()), card.GetQuantity(), parent);
	else
		im = new CardImage(downloader->FullPathFor(card.front()),
			downloader->FullPathFor(card.back()),
			card.GetQuantity(),
			parent);

	return CardCardImage{ .card = std::forward<CardRefQualified>(card),
		.image                  = im };
}

MainDeck::MainDeck(Downloader* downloader, QWidget* parent, Qt::WindowFlags f)
	: QWidget(parent, f), mDownloader(downloader)
{
	mGrid->SetGrouping(CardGrid::Grouping::Type);
}

template <typename T>
void AddCardImpl(T&& card, MainDeck* deck, CardGrid* grid, Downloader* downloader)
{
	if (auto it = grid->find(card.GetSet(), card.GetCollectorNumber());
		it != grid->cards().end())
	{
		it->card.SetQuantity(it->card.GetQuantity()
							 + (card.GetQuantity() > 0 ? card.GetQuantity() : 1));
		it->image->SetQuantity(it->card.GetQuantity());
	}
	else
	{
		auto c =
			MakeCardCardImage(std::forward<T>(card), downloader, grid->widget());
		if (c.card.GetQuantity() <= 0)
		{
			c.card.SetQuantity(1);
			c.image->SetQuantity(1);
		}

		auto set = c.card.GetSet();
		auto cn  = c.card.GetCollectorNumber();

		downloader->AddToDownload(c.card);

		grid->push_back(std::move(c));
		QObject::connect(c.image,
			&CardImage::clicked,
			deck,
			[deck, grid, set = std::move(set), cn = std::move(cn)] {
				emit deck->CardClicked(grid->findIndex(set, cn));
			});
		grid->LayoutImages();
	}
}

void MainDeck::AddCard(const Card& card)
{
	AddCardImpl(card, this, mGrid, mDownloader);
}

void MainDeck::AddCard(Card&& card)
{
	AddCardImpl(std::move(card), this, mGrid, mDownloader);
}

template <typename T>
void RemoveCardImpl(T&& card, CardGrid* grid)
{
	if (auto it = grid->find(card.GetSet(), card.GetCollectorNumber());
		it != grid->cards().end())
	{
		it->card.SetQuantity(it->card.GetQuantity()
							 - (card.GetQuantity() > 0 ? card.GetQuantity() : 1));
		if (it->card.GetQuantity() <= 0)
		{
			delete it->image;
			grid->cards().erase(it);
			grid->LayoutImages();
		}
		else
			it->image->SetQuantity(it->card.GetQuantity());
	}
}

void MainDeck::RemoveCard(const Card& card)
{
	RemoveCardImpl(card, mGrid);
}

void MainDeck::RemoveCard(Card&& card)
{
	RemoveCardImpl(std::move(card), mGrid);
}

Card MainDeck::CardAt(int index)
{
	return mGrid->operator[](index).card;
}

void MainDeck::LoadDeck(std::string_view file)
{
	LoadDeck(nlohmann::json::parse(std::ifstream(file))["main"]);
}

void MainDeck::LoadDeck(const nlohmann::json& src)
{
	ClearDeck();
	for (const auto& card : src)
	{
		AddCard(Card{ card });
	}
}

void MainDeck::ClearDeck()
{
	std::for_each(begin(mGrid->cards()),
		end(mGrid->cards()),
		[](const CardCardImage& c) { delete c.image; });
	mGrid->cards().clear();
}

MainDeck::operator nlohmann::json() const
{
	nlohmann::json res;
	std::for_each(begin(mGrid->cards()),
		end(mGrid->cards()),
		[&res](const CardCardImage& c) { res.push_back(c.card.to_json()); });

	return res;
}

void MainDeck::ReDownload()
{
	std::for_each(begin(Grid()->cards()),
		end(Grid()->cards()),
		[downloader = mDownloader](
			const CardCardImage& c) { downloader->AddToDownload(c.card); });

	Grid()->ReloadImages(mDownloader);
}

void MainDeck::resizeEvent(QResizeEvent* e)
{
	QWidget::resizeEvent(e);
	mGrid->resize(size());
}
