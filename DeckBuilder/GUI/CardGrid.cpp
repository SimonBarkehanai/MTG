#include "CardGrid.h"

#include <execution>

#include <QContextMenuEvent>
#include <QMenu>
#include <QScrollBar>
#include <QStringBuilder>

#include "any_of.h"

CardGrid::CardGrid(QWidget* parent) : QScrollArea(parent)
{
	setHorizontalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAlwaysOff);
	setWidget(new QWidget);
}

void CardGrid::ClearResults()
{
	for (auto card : mCards)
		delete card.image;
	mCards.clear();
}

void CardGrid::ReloadImages(Downloader* downloader)
{
	std::for_each(begin(mCards), end(mCards), [downloader](const CardCardImage& c) {
		if (c.card.NumFaces == 1)
			c.image->SetPicture(downloader->FullPathFor(c.card.front()));
		else
			c.image->SetPicture(downloader->FullPathFor(c.card.front()),
				downloader->FullPathFor(c.card.back()));
	});
}

void CardGrid::resizeEvent(QResizeEvent* e)
{
	QScrollArea::resizeEvent(e);
	LayoutImages();
}

void CardGrid::contextMenuEvent(QContextMenuEvent* e)
{
	if (auto cardIndex = IndexAt(widget()->mapFrom(this, e->pos()));
		!mContextMenuActions.empty() && cardIndex >= 0)
	{
		std::vector<QMetaObject::Connection> connections;
		QMenu                                menu(this);
		std::for_each(begin(mContextMenuActions),
			end(mContextMenuActions),
			[&menu, &connections, this, index = 0, cardIndex](QAction* act) mutable {
				menu.addAction(act);
				connections.push_back(connect(act,
					&QAction::triggered,
					this,
					[this, cardIndex, actionIndex = index]() {
						if (cardIndex >= 0)
							emit ContextMenuActionTriggered(cardIndex, actionIndex);
					}));
				++index;
			});
		menu.exec(e->globalPos());
		std::for_each(begin(connections),
			end(connections),
			[](const QMetaObject::Connection& connection) {
				disconnect(connection);
			});
	}
}

std::vector<CardCardImage>::iterator CardGrid::find(
	std::string_view set, std::string_view colnum)
{
	auto it = cfind(set, colnum);
	// doesn't change mCards, but returns a non-const iterator
	return mCards.erase(it, it);
}

std::vector<CardCardImage>::const_iterator CardGrid::cfind(
	std::string_view set, std::string_view colnum) const
{
	return std::find_if(std::execution::par_unseq,
		begin(mCards),
		end(mCards),
		[set, colnum](const CardCardImage& card) {
			return card.card.GetCollectorNumber() == colnum
		           && card.card.GetSet() == set;
		});
}

int CardGrid::findIndex(std::string_view set, std::string_view colnum) const
{
	return find(set, colnum) - begin(mCards);
}

int CardGrid::NumCards() const
{
	int numCards = 0;
	std::for_each(begin(mCards), end(mCards), [&numCards](const CardCardImage& c) {
		numCards += c.card.GetQuantity();
	});
	return numCards;
}

void CardGrid::LayoutImages()
{
	switch (mGrouping)
	{
	case Grouping::NoGrouping:
		LayoutUngrouped();
		break;
	case Grouping::Type:
		LayoutByType();
		break;
	}
}

void CardGrid::LayoutUngrouped()
{
	const int w = width(), h = height(),
			  maxVisibleRows =
				  (h + (Spacing - 2 * Padding)) / (CardImage::Height + Spacing);
	int cols = MaxColumns(),
		rows = std::ceil(mCards.size() / static_cast<double>(cols));

	if (rows <= maxVisibleRows)
		widget()->resize(w - 2, h - 2);
	else
	{
		verticalScrollBar()->show();
		const int availableWidth = viewport()->width();
		cols                     = MaxColumns(availableWidth);
		rows = std::ceil(mCards.size() / static_cast<double>(cols));

		widget()->resize(availableWidth,
			rows * (CardImage::Height + Spacing) + (2 * Padding - Spacing));
	}

	for (std::size_t i = 0; i < mCards.size(); ++i)
	{
		const auto x = Padding + (CardImage::Width + Spacing) * (i % cols),
				   y = Padding + (CardImage::Height + Spacing) * (i / cols);
		mCards[i].image->move(x, y);
	}
}

void CardGrid::LayoutByType()
{
	std::for_each(begin(mConnections),
		end(mConnections),
		[](const QMetaObject::Connection& connection) {
			QObject::disconnect(connection);
		});
	mConnections.clear();

	std::sort(std::execution::par_unseq,
		begin(mCards),
		end(mCards),
		[](const CardCardImage& lhs, const CardCardImage& rhs) {
			using underlying = std::underlying_type_t<Type>;
			return static_cast<underlying>(
					   SignificantType(lhs.card.front().GetTypes()))
		           < static_cast<underlying>(
					   SignificantType(rhs.card.front().GetTypes()));
		});

	for (auto label : mLabels)
		delete label;
	mLabels.clear();

	enum TypeIndex
	{
		Creature = 0,
		Instant,
		Sorcery,
		Artifact,
		Enchantment,
		Planeswalker,
		Land,
		NumTypes,
	};
	std::array<int, NumTypes>                   numTypes;
	const std::array<QString, NumTypes>         typeNames{ "Creature",
        "Instant",
        "Sorcery",
        "Artifact",
        "Enchantment",
        "Planeswalker",
        "Land" };
	static constexpr std::array<Type, NumTypes> actualTypes{ Type::Creature,
		Type::Instant,
		Type::Sorcery,
		Type::Artifact,
		Type::Enchantment,
		Type::Planeswalker,
		Type::Land };

	auto            first = begin(mCards);
	decltype(first) next;
	for (int i = 0; i < NumTypes; ++i)
	{
		next = std::find_if(first, end(mCards), [i](const CardCardImage& val) {
			return SignificantType(val.card.GetTypes()) != actualTypes[i];
		});
		numTypes[i] = next - first;
		first       = next;
	}

	verticalScrollBar()->show();

	int nextY = Padding, labelWidth = viewport()->width() - 2 * Padding,
		rootTypeIdx = 0, cols = MaxColumns(viewport()->width()), nextIdx = 0;

	for (int ty = Creature; ty < NumTypes; ++ty)
	{
		if (numTypes[ty] != 0)
		{
			int  numOfType = 0;
			auto label     = new QLabel(widget());
			label->setGeometry(Padding, nextY, labelWidth, LabelHeight);
			label->show();
			nextY += LabelHeight + Spacing / 2;
			mLabels.push_back(label);

			std::array<std::vector<CardImage*>, 10> stacks;
			constexpr auto                          stackIdx = [](int cmc) {
                return std::clamp(cmc, 0, 9);
			};

			for (int i = 0; i < numTypes[ty]; ++i)
			{
				stacks[stackIdx(mCards[nextIdx].card.front().GetCost().CMC())].push_back(
					mCards[nextIdx].image);
				numOfType += mCards[nextIdx].card.GetQuantity();
				++nextIdx;
			}
			label->setText(typeNames[ty] % " - " % QString::number(numOfType));

			int stackNum = 0;

			int maxStack = -1;
			for (const auto& stack : stacks)
			{
				if (!stack.empty())
				{
					for (int posInStack = 0; posInStack < stack.size(); ++posInStack)
					{
						stack[posInStack]->move(
							Padding + (CardImage::Width + Spacing) * (stackNum % cols),
							nextY + CardImage::StackOffset * posInStack);

						if (posInStack > maxStack)
							maxStack = posInStack;

						if (posInStack + 1 < stack.size())
						{
							stack[posInStack]->stackUnder(stack[posInStack + 1]);

							mConnections.push_back(connect(stack[posInStack],
								&CardImage::Hovered,
								stack[posInStack],
								&CardImage::raise));
							mConnections.push_back(connect(stack[posInStack],
								&CardImage::UnHovered,
								stack[posInStack],
								[bottom = stack[posInStack],
									top = stack[posInStack + 1]]() {
									bottom->stackUnder(top);
								}));
						}
					}
					++stackNum;
					if (stackNum % cols == 0)
					{
						nextY += CardImage::Height
						         + CardImage::StackOffset * maxStack + Spacing;
						maxStack = -1;
					}
				}
			}
			if (maxStack != -1)
			{
				nextY += CardImage::Height + CardImage::StackOffset * maxStack
				         + Spacing;
			}
		}
	}

	widget()->resize(viewport()->width(), nextY + (Padding - Spacing));
}

int CardGrid::MaxColumns(int width) const
{
	auto cols = (width + (Spacing - 2 * Padding)) / (CardImage::Width + Spacing);
	return cols > 0 ? cols : 1;
}

int CardGrid::IndexAt(QPoint pos)
{
	auto image = qobject_cast<CardImage*>(widget()->childAt(pos));
	if (image != nullptr)
	{
		auto it = std::find_if(begin(mCards),
			end(mCards),
			[res = widget()->childAt(pos)](
				const CardCardImage& c) { return c.image == res; });

		return it - begin(mCards);
	}
	else
		return -1;
}
