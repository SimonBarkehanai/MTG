#pragma once

#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QScrollArea>
#include <QWidget>

#include "Card.h"
#include "CardGrid.h"
#include "CardImage.h"
#include "Searcher.h"

class SearchPane : public QWidget
{
	Q_OBJECT
public:
	SearchPane(Downloader* downloader,
		QWidget*           parent    = nullptr,
		std::string        imageSize = "normal",
		Qt::WindowFlags    f         = {});

	auto& CardAt(std::size_t index) noexcept
	{
		return mSearcher.ResultAt(index);
	}
	auto& CardAt(std::size_t index) const noexcept
	{
		return mSearcher.ResultAt(index);
	}

	CardGrid* const Grid() noexcept
	{
		return mResultArea;
	}
	const CardGrid* const Grid() const noexcept
	{
		return mResultArea;
	}

	Searcher& GetSearcher() noexcept
	{
		return mSearcher;
	}
	const Searcher& GetSearcher() const noexcept
	{
		return mSearcher;
	}

	void AddExtraArg(std::string arg)
	{
		mExtraArgs.push_back(std::move(arg));
	}
	auto& GetExtraArgs() noexcept
	{
		return mExtraArgs;
	}
	auto& GetExtraArgs() const noexcept
	{
		return mExtraArgs;
	}
	auto& ExtraArgAt(std::size_t idx) noexcept
	{
		return mExtraArgs[idx];
	}
	auto& ExtraArgAt(std::size_t idx) const noexcept
	{
		return mExtraArgs[idx];
	}

	void StopSearching()
	{
		mSearcher.Stop();
	}

	void ReDownload();

signals:
	void CardClicked(int index);

protected:
	void resizeEvent(QResizeEvent* e) override;

private slots:
	void Search();
	void RandomSearch();

	void PopulateResults();

private:
	void ClearResults();
	void LayoutResults();

	QLineEdit*      mSearchText   = new QLineEdit(this);
	ClickableLabel* mSearchIcon   = new ClickableLabel(this);
	QPushButton*    mRandomButton = new QPushButton(this);
	CardGrid*       mResultArea   = new CardGrid(this);
	QLabel*         mError;

	std::size_t mNumResultsShown = 0;

	Searcher                 mSearcher;
	std::vector<std::string> mExtraArgs;
	Downloader*              mDownloader;

	inline static constexpr auto Padding = 10, SearchSpacing = 8,
								 RandomSpacing = 5, SearchHeight = 22,
								 IconSize = SearchHeight - 2, RandomWidth = 60,
								 RandomHeight   = SearchHeight + 2,
								 ResultsSpacing = 10, ErrorHeight = 20;
};
