#include "ClickableLabel.h"

#include <QMouseEvent>

ClickableLabel::ClickableLabel(QWidget* parent, Qt::WindowFlags f)
	: QLabel(parent, f)
{
}

void ClickableLabel::mousePressEvent(QMouseEvent* e)
{
	QLabel::mousePressEvent(e);
	if (e->button() == Qt::MouseButton::LeftButton)
	{
		emit clicked();
		e->accept();
	}
}
