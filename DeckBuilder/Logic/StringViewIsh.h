#pragma once
#include <concepts>

template <typename T>
concept StringViewIsh = requires(T s,
	const typename T::value_type*  str,
	typename T::value_type         ch,
	typename T::size_type          sz)
{
	{
		s.find(str)
		} -> std::same_as<typename T::size_type>;
	{
		s.find(ch)
		} -> std::same_as<typename T::size_type>;
	{
		s.find(str, sz)
		} -> std::same_as<typename T::size_type>;
	{
		s.find(ch, sz)
		} -> std::same_as<typename T::size_type>;
	{
		s.substr(sz, sz)
		} -> std::same_as<T>;
	{
		s.substr(sz)
		} -> std::same_as<T>;
};