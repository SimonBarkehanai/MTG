#pragma once

#if defined(__has_builtin) && __has_builtin(__builtin_unreachable)
#	define UNREACHABLE() __builtin_unreachable()
#elif defined(_MSC_VER)
#	define UNREACHABLE() __assume(false)
#else
#	define UNREACHABLE() assert(!"unreachable")
#endif
