#pragma once
#include <charconv>
#include <deque>
#include <filesystem>
#include <iostream>
#include <mutex>
#include <optional>
#include <string>
#include <thread>

#if __has_include(<sdkddkver.h>)
#	include <sdkddkver.h>
#endif

#include <boost/asio.hpp>
#include <boost/beast.hpp>
#include <boost/beast/ssl.hpp>

#include "Card.h"

// ordered host, target, destination
struct DownloadInfo
{
	// which host to download from (e.g. c2.scryfall.com)
	std::string host;
	// path after scryfall.com (e.g.
	// /file/scryfall-cards/small/front/1/e/1e6bd3fa-0d07-4519-b67f-67867ad13c89.jpg)
	std::string target;
	// path to download the image to
	std::string destination;
};

struct ImagesToDownload
{
	std::mutex               Mutex;  // guards ToDownload
	std::deque<DownloadInfo> ToDownload;
};

class DownloaderWorker
{
public:
	explicit DownloaderWorker(ImagesToDownload* toDownload);
	// needed to put DownloaderWorker in a vector (allows move only if noexcept)
	explicit DownloaderWorker(DownloaderWorker&& src) noexcept;

	void Start();

	void RequestStop() noexcept
	{
		mStopRequested.store(true);
	}

private:
	bool LoadNext() noexcept;
	void Connect();
	void DoDownload();
	void PerformRequest();

	std::atomic_bool  mStopRequested = false;
	ImagesToDownload* mToDownload;
	DownloadInfo      mDownloadInfo;

	boost::asio::io_context                                           mIoc;
	boost::asio::ssl::context                                         mSSL;
	std::optional<boost::beast::ssl_stream<boost::beast::tcp_stream>> mStream;
	boost::beast::flat_buffer                                         mBuffer;
	boost::beast::http::request<boost::beast::http::empty_body>       mReq;
	boost::beast::http::response<boost::beast::http::file_body>       mRes;
	boost::asio::ip::basic_resolver_results<boost::asio::ip::tcp> mResolverResults;

	inline static constexpr std::string_view Host = "scryfall.com",
											 Port = "443";
};

class Downloader
{
public:
	enum class ImageSize : uint8_t
	{
		Small,
		Normal,
		Large,
		PNG
	};

	explicit Downloader(std::size_t numThreads,
		ImageSize                   imageSize  = ImageSize::Large,
		std::string                 saveFolder = DefaultSaveFolder);
	~Downloader();

	enum class Face
	{
		Front,
		Back
	};

	void AddToDownload(const DownloadInfo& info);
	void AddToDownload(DownloadInfo&& info);
	void AddToDownload(const Card& card);
	void AddToDownload(const CardFace& card, Face face = Face::Front);

	void WaitForFinish();

	[[nodiscard]] auto NumThreads() const noexcept
	{
		return mNumThreads;
	}

	std::string FullPathFor(const Card& crd);
	std::string FullPathFor(const CardFace& crd);
	std::string FullPathFor(
		std::string_view name, std::string_view set, std::string_view colNum);

	[[nodiscard]] ImageSize GetImageSize() const noexcept
	{
		return mImageSize;
	}
	// no lifetime issues - from string literal
	[[nodiscard]] std::string_view GetImageSizeString() const
	{
		return ImageSizeStringFromValue(mImageSize);
	}
	void SetImageSize(ImageSize newSize) noexcept
	{
		mImageSize = newSize;
	}

	static constexpr std::string_view ImageSizeStringFromValue(ImageSize size) noexcept
	{
		return ImageSizeStrings[static_cast<std::underlying_type_t<ImageSize>>(size)];
	}
	static constexpr ImageSize ImageSizeFromString(std::string_view size)
	{
		return static_cast<ImageSize>(
			std::find(begin(ImageSizeStrings), end(ImageSizeStrings), size)
			- begin(ImageSizeStrings));
	}

	std::string GetSaveFolder() const
	{
		return mSaveFolder;
	}
	void SetSaveFolder(std::string saveFolder)
	{
		mSaveFolder = saveFolder;
	}

private:
	void NewDownloaderWorker();
	void Stop();

	const std::size_t             mNumThreads;
	ImagesToDownload              mToDownload;
	std::vector<std::thread>      mThreads;
	std::vector<DownloaderWorker> mDownloaders;

	std::string mSaveFolder;
	ImageSize   mImageSize = ImageSize::Normal;

	static constexpr std::string_view ImageExtension(ImageSize imageSize) noexcept
	{
		switch (imageSize)
		{
		case ImageSize::Normal:
			return ".normal.jpg";
			break;
		case ImageSize::Large:
			return ".large.jpg";
			break;
		case ImageSize::Small:
			return ".small.jpg";
			break;
		case ImageSize::PNG:
			return ".png";
			break;
		}
		return "";
	}
	static constexpr std::array ImageSizeStrings{ std::string_view("small"),
		std::string_view("normal"),
		std::string_view("large"),
		std::string_view("png") };

	static const std::string DefaultSaveFolder;
};
