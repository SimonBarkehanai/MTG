#include "Downloader.h"

#include <catch.hpp>

#include "Searcher.h"

TEST_CASE("Downloader Works", "[downloader]")
{
	constexpr auto NumThreads = 5;
	Downloader     p(NumThreads);
	p.AddToDownload(DownloadInfo{ "c1.scryfall.com",
		"/file/scryfall-cards/large/front/1/e/"
		"1e6bd3fa-0d07-4519-b67f-67867ad13c89.jpg",
		R"(C:\Users\Simon\Desktop\Acidic Slime.jpg)" });
	p.AddToDownload(DownloadInfo{ "c1.scryfall.com",
		"/file/scryfall-cards/large/front/5/8/"
		"58cbcf51-3824-4ab3-89e3-cdcc1a0c7267.jpg",
		R"(C:\Users\Simon\Desktop\Necrotic Ooze.jpg)" });
	p.AddToDownload(DownloadInfo{ "c1.scryfall.com",
		"/file/scryfall-cards/large/front/a/e/"
		"ae57dc27-539b-45f2-a18a-ca1f699f645d.jpg",
		R"(C:\Users\Simon\Desktop\Scavenging Ooze.jpg)" });
	p.AddToDownload(DownloadInfo{ "c1.scryfall.com",
		"/file/scryfall-cards/large/front/1/4/"
		"142076ee-870e-4962-8471-be4ae2e6e07c.jpg",
		R"(C:\Users\Simon\Desktop\The Mimeoplasm.jpg)" });
	p.AddToDownload(DownloadInfo{ "c1.scryfall.com",
		"/file/scryfall-cards/large/front/e/6/"
		"e640664f-5cc7-4970-b966-6e6e5ae09c5a.jpg",
		R"(C:\Users\Simon\Desktop\Voidslime.jpg)" });
	p.WaitForFinish();
}

TEST_CASE("Searcher Works", "[searcher]")
{
	Downloader d(
		std::thread::hardware_concurrency() / 2, Downloader::ImageSize::PNG);
	Searcher s(&d);
	CHECK(s.Search("e:sld"));
	s.WaitForFinish();
}