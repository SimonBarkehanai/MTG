#include "Downloader.h"

#include <boost/predef/compiler.h>
#include <boost/predef/os.h>

#include "DecomposedUrl.h"
#include "Unreachable.h"

#ifdef BOOST_OS_WINDOWS_AVAILABLE
#	include <ShlObj.h>
#elif BOOST_OS_LINUX_AVAILABLE
#	include <pwd.h>
#endif

namespace net = boost::asio;
namespace ssl = net::ssl;
using net::ip::tcp;
namespace beast = boost::beast;
namespace http  = beast::http;
namespace fs    = std::filesystem;

DownloaderWorker::DownloaderWorker(ImagesToDownload* toDownload)
	: mToDownload(toDownload)
	, mSSL([] {
		ssl::context ctx(ssl::context::method::sslv23);
		ctx.set_default_verify_paths();
		return ctx;
	}())
	, mResolverResults(tcp::resolver(mIoc).resolve(Host, Port))
{
	mReq.method(http::verb::get);
	mReq.set(http::field::user_agent, BOOST_BEAST_VERSION_STRING);
	Connect();
}

DownloaderWorker::DownloaderWorker(DownloaderWorker&& src) noexcept
	: mStopRequested(src.mStopRequested.load())
	, mToDownload(src.mToDownload)
	, mDownloadInfo(std::move(src.mDownloadInfo))
	, mSSL(std::move(src.mSSL))
	, mStream(std::move(src.mStream))
	, mResolverResults(std::move(src.mResolverResults))
{
	UNREACHABLE();
}

void DownloaderWorker::Start()
{
	while (!mStopRequested)
	{
		if (!LoadNext())  // no work to do
			std::this_thread::sleep_for(std::chrono::seconds(1));
		else if (!fs::exists(mDownloadInfo.destination)
				 || fs::file_size(mDownloadInfo.destination) < 10'000)
			// mDownloadInfo needs to be downloaded
			DoDownload();
	}
}

bool DownloaderWorker::LoadNext() noexcept
{
	std::lock_guard l{ mToDownload->Mutex };
	if (mToDownload->ToDownload.empty())
		return false;
	else
	{
		mDownloadInfo = std::move(mToDownload->ToDownload.front());
		mToDownload->ToDownload.pop_front();
		return true;
	}
}

void DownloaderWorker::Connect()
{
	mStream.emplace(mIoc, mSSL);
	if (!SSL_set_tlsext_host_name(mStream->native_handle(), Host.data()))
		throw beast::system_error(beast::error_code(
			static_cast<int>(::ERR_get_error()), net::error::get_ssl_category()));

	beast::get_lowest_layer(*mStream).connect(mResolverResults);
	mStream->handshake(net::ssl::stream_base::client);
}

void DownloaderWorker::DoDownload()
{
	mReq.target(mDownloadInfo.target);
	mReq.set(http::field::host, mDownloadInfo.host);

	PerformRequest();
	// Response: 3xx redirect
	while (mRes.result_int() >= 300 && mRes.result_int() <= 399)
	{
		const auto decomp = TargetFromUrl(mRes["Location"]);
		mReq.target(decomp.target);
		mReq.set(http::field::host, decomp.host);
		PerformRequest();
	}
}

void DownloaderWorker::PerformRequest()
{
	http::write(*mStream, mReq);

	beast::error_code ec;

	mRes.body().open(
		mDownloadInfo.destination.c_str(), beast::file_mode::write, ec);
	if (ec)
		throw std::system_error{ ec };

	http::read(*mStream, mBuffer, mRes, ec);
	while (ec == beast::http::error::end_of_stream && !mStopRequested.load())
	{
		mRes.body().close();
		ec = {};
		mRes.body().open(
			mDownloadInfo.destination.c_str(), beast::file_mode::write, ec);
		if (ec)
			throw std::system_error{ ec };

		Connect();
		http::write(*mStream, mReq);
		http::read(*mStream, mBuffer, mRes, ec);
	}

	mRes.body().close();

	if (ec)
		throw beast::system_error(ec);
}

const std::string Downloader::DefaultSaveFolder = []() noexcept {
#ifdef BOOST_OS_WINDOWS_AVAILABLE
	wchar_t* wstr = nullptr;
	SHGetKnownFolderPath(FOLDERID_LocalAppData, KF_FLAG_CREATE, nullptr, &wstr);

	char       str[MAX_PATH];
	const auto len =
		WideCharToMultiByte(
			CP_UTF8, 0, wstr, -1, std::begin(str), std::size(str), nullptr, nullptr)
		// subtract 1 from the returned size for the null character
		- 1;

	CoTaskMemFree(wstr);

	return std::string(str, len) + "/DeckBuilder/CardPics/";

#elif BOOST_OS_LINUX_AVAILABLE
	const char* home = std::getenv("HOME");
	if (home == nullptr)
		home = getpwuid(getuid())->pw_dir;

	auto len = std::strlen(home);
	if (home[len - 1] == '/')
		--len;

	return std::string(home, len) + "/.cache/DeckBuilder/CardPics/";

#endif
}();

Downloader::Downloader(
	std::size_t numThreads, ImageSize imageSize, std::string saveFolder)
	: mNumThreads(numThreads)
	, mImageSize(imageSize)
	, mSaveFolder(std::move(saveFolder))

{
	mDownloaders.reserve(mNumThreads);
	mThreads.reserve(mNumThreads);

	for (std::size_t i = 0; i < mNumThreads; ++i)
		NewDownloaderWorker();
}

Downloader::~Downloader()
{
	Stop();
}

template <typename T>
void StartDownload(T&& info, ImagesToDownload& toDownload) requires
	std::is_same_v<std::remove_cvref_t<T>, DownloadInfo>
{
	std::lock_guard l{ toDownload.Mutex };
	toDownload.ToDownload.push_back(std::forward<T>(info));
}

void Downloader::AddToDownload(const DownloadInfo& info)
{
	StartDownload(info, mToDownload);
}

void Downloader::AddToDownload(DownloadInfo&& info)
{
	StartDownload(std::move(info), mToDownload);
}

void Downloader::AddToDownload(const Card& card)
{
	if (card.NumFaces == 1)
		AddToDownload(card.front());
	else
	{
		AddToDownload(card.front());
		AddToDownload(card.back(), Face::Back);
	}
}

void Downloader::AddToDownload(const CardFace& card, Face face)
{
	AddToDownload(DownloadInfo{ .host = "api.scryfall.com",
		.target =
			"/cards/" + card.GetSet() + '/' + card.GetCollectorNumber()
			+ (face == Face::Back ? "?format=image&face=back" : "?format=image"),
		.destination = FullPathFor(card) });
}

void Downloader::WaitForFinish()
{
	const auto sleepDur = std::chrono::milliseconds(500) / mNumThreads;
	while (true)
	{
		{
			std::lock_guard l{ mToDownload.Mutex };
			if (mToDownload.ToDownload.empty())
				break;
		}
		std::this_thread::sleep_for(sleepDur);
	}

	Stop();
}

void Downloader::NewDownloaderWorker()
{
	mDownloaders.emplace_back(&mToDownload);
	mThreads.emplace_back(&DownloaderWorker::Start, &mDownloaders.back());
}

void Downloader::Stop()
{
	std::for_each(begin(mDownloaders),
		end(mDownloaders),
		[](DownloaderWorker& bl) { bl.RequestStop(); });

	std::for_each(begin(mThreads), end(mThreads), [](std::thread& t) {
		if (t.joinable())
			t.join();
	});
}

std::string Downloader::FullPathFor(const Card& crd)
{
	assert(crd.NumFaces == 1);
	return FullPathFor(crd.front());
}
std::string Downloader::FullPathFor(const CardFace& crd)
{
	return FullPathFor(crd.GetName(), crd.GetSet(), crd.GetCollectorNumber());
}

void test(std::string&&);
void test(const std::string&);

std::string operator+(std::string&& lhs, std::string_view rhs)
{
	lhs.append(rhs);
	return lhs;
}
std::string operator+(const std::string& lhs, std::string_view rhs)
{
	return lhs + std::string(rhs);
}

std::string Downloader::FullPathFor(
	std::string_view name, std::string_view set, std::string_view colNum)
{
	constexpr auto iequal_right_lower = [](std::string_view  lhs,
											std::string_view rhs) {
		if (lhs.length() != rhs.length())
			return false;

		std::size_t i = 0;
		for (; i < lhs.length(); ++i)
			if (lhs[i] != rhs[i])
				goto slow;

		return true;
slow:
		auto lower = [](char ch) {
			return (ch >= 'A' && ch <= 'Z') ? ch + ('a' - 'A') : ch;
		};

		for (; i < lhs.length(); ++i)
			if (lower(lhs[i]) != rhs[i])
				return false;

		return true;
	};

	if (iequal_right_lower(set, "con"))  // reserved on Windows
		set = "cfx";

	auto dir = mSaveFolder + set;

	std::filesystem::create_directories(dir);

	return std::move(dir) + '/' + name + '.' + colNum + ImageExtension(mImageSize);
}
