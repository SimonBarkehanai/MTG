#pragma once
#include <concepts>

#include "StringViewIsh.h"

template <typename T>
	requires StringViewIsh<T>
struct DecomposedUrl
{
	T host, target;
};
template <typename T>
constexpr auto TargetFromUrl(T imgUrl) requires StringViewIsh<T>
{
	const auto protocolLength = imgUrl.find("//") + 2;
	const auto hostLength = imgUrl.find('/', protocolLength) - protocolLength;

	return DecomposedUrl<T>{ .host = imgUrl.substr(protocolLength, hostLength),
		.target                    = imgUrl.substr(protocolLength + hostLength,
            imgUrl.find('?') - protocolLength - hostLength) };
}
