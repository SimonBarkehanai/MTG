#include "Searcher.h"

#include <bit>
#include <concepts>

#include <boost/predef/os.h>
#include <fmt/format.h>

#include "DecomposedUrl.h"
#include "any_of.h"

namespace net = boost::asio;
using net::ip::tcp;
namespace beast = boost::beast;
namespace http  = boost::beast::http;
using namespace std::literals;

constexpr Type TypeFor(std::string_view singleType)
{
	if (singleType == "Artifact"sv)
		return Type::Artifact;
	else if (singleType == "Conspiracy"sv)
		return Type::Conspiracy;
	else if (singleType == "Creature"sv)
		return Type::Creature;
	else if (singleType == "Enchantment"sv)
		return Type::Enchantment;
	else if (singleType == "Instant"sv)
		return Type::Instant;
	else if (singleType == "Land"sv)
		return Type::Land;
	else if (singleType == "Phenomenon"sv)
		return Type::Phenomenon;
	else if (singleType == "Plane"sv)
		return Type::Plane;
	else if (singleType == "Planeswalker"sv)
		return Type::Planeswalker;
	else if (singleType == "Scheme"sv)
		return Type::Scheme;
	else if (singleType == "Sorcery"sv)
		return Type::Sorcery;
	else if (singleType == "Vanguard"sv)
		return Type::Vanguard;
	else
		return static_cast<Type>(0);
}

constexpr Type TypesFor(std::string_view typeLine)
{
	std::size_t len = typeLine.find(' '), beg = 0,
				end = typeLine.find("\xe2\x80\x94") - 1;

	Type type = TypeFor(typeLine.substr(0, len));

	while (beg + len < end && beg + len != std::string_view::npos)
	{
		beg += len + 1;
		len                        = typeLine.find(' ', beg) - beg;
		std::string_view this_type = typeLine.substr(beg, len);
		type |= TypeFor(this_type);
	}

	return type;
}

Searcher::Searcher(Downloader* downloader)
	: mBusy(false)
	, mStopRequested(false)
	, mFinishedSuccessfully(false)
	, mSSL([] {
		net::ssl::context ctx(net::ssl::context::sslv23_client);
		ctx.set_default_verify_paths();
		return ctx;
	}())
	, mEndpoint(tcp::resolver(mIoc).resolve(Host, Port))
	, mDownloader(downloader)
{
	mReq.method(http::verb::get);
	mReq.set(http::field::host, Host);
	mReq.set(http::field::user_agent, BOOST_BEAST_VERSION_STRING);

	Connect();
}

Searcher::~Searcher()
{
	if (mThread.joinable())
		mThread.join();
}

bool Searcher::Search(std::string query)
{
	return Search(std::move(query), SearchType::Normal);
}

bool Searcher::Random(std::string query)
{
	return Search(std::move(query), SearchType::Random);
}

void Searcher::WaitForFinish()
{
	while (mBusy.load()) {}
}

void Searcher::WaitForImageDownloadFinish()
{
	WaitForFinish();
	mDownloader->WaitForFinish();
}

void Searcher::Stop() noexcept
{
	RequestStop();
	while (mBusy.load()) {}
}

std::string Searcher::GetTarget(std::string_view target)
{
	mRes.body().clear();
	Get(target);
	return mRes.body();
}

bool Searcher::Search(std::string query, SearchType type)
{
	if (mBusy.load())
		return false;

	if (mThread.joinable())
		mThread.join();

	mStopRequested.store(false);
	mCards.clear();
	mWarnings.clear();

	mThread = std::thread(&Searcher::DoSearch, this, std::move(query), type, 1);

	mBusy.store(true);

	return true;
}

// calls Callable on destruction
template <typename Callable>
struct [[nodiscard]] scope_exit
{
	explicit scope_exit(Callable&& c) : callable_(std::move(c))
	{
	}
	explicit scope_exit() : callable_()
	{
	}
	~scope_exit()
	{
		callable_();
	}

	Callable callable_;
};

void Searcher::DoSearch(std::string query, SearchType type, std::size_t page)
{
	mBusy.store(true);

	// executed when e is destroyed
	scope_exit se{ [this] {
		mBusy.store(false);
	} };

	constexpr auto len = 8, numStart = len - 2;

	std::array<char, len> pageStr{ "&page=" };
	const auto            to_char_res = std::to_chars(
        pageStr.data() + numStart, pageStr.data() + pageStr.size(), page);

	mRes.body().clear();
	Get(((type == SearchType::Normal ? "/cards/search?q=" : "/cards/random?q=") + query)
			.append(&*std::begin(pageStr), to_char_res.ptr));
	auto res = nlohmann::json::parse(mRes.body());

	if (res["object"].get<std::string_view>() == "error"sv)
	{
		mError = res["details"].get<std::string>();

		if (auto warnings = res.find("warnings"); warnings != res.end())
		{
			mWarnings.reserve(warnings->size());
			for (const auto& warning : *warnings)
				mWarnings.emplace_back(warning.get<std::string>());
		}

		mCards.clear();

		mFinishedSuccessfully.store(false);

		return;
	}
	else
	{
		mError.clear();
		mWarnings.clear();

		if (type == SearchType::Normal)
		{
			mCards.reserve(
				std::max(res["total_cards"].get<std::size_t>(), MaxCards));

			for (const auto& result : res["data"])
			{
				if (mStopRequested.load())
					return;
				HandleResult(result);
			}

			if (!mStopRequested.load())
			{
				if (res["has_more"].get<bool>() && page + 1 <= MaxPages)
					DoSearch(std::move(query), type, page + 1);
				else
					mFinishedSuccessfully.store(true);
			}
			else
				mFinishedSuccessfully.store(false);
		}
		else
		{
			HandleResult(res);
			mFinishedSuccessfully.store(true);
		}
	}
}

void Searcher::Get(std::string_view target)
{
	// when expanding characters into %XX,
	// I expect the overall string length to increase by 10%
	constexpr auto ExpectedExpansionIncrease = 1.1;
	std::string    targ;
	targ.reserve(target.length() * ExpectedExpansionIncrease);

	for (auto i : target)  // percent encode where needed
	{
		if (('a' <= i && i <= 'z') || ('A' <= i && i <= 'Z')
			|| ('0' <= i && i <= '9') || i == '/' || i == '=' || i == '?' || i == '&'
			|| i == ',' || i == '!' || i == '-' || i == '.' || i == ':')
			// don't need percent encoding
			targ += i;
		else  // need percent encoding
		{
			std::array<char, 3> dest = { '%' };
			constexpr auto      base = 16;
			std::to_chars(dest.data() + 1,
				dest.data() + sizeof(dest) / sizeof(dest[0]),
				i,
				base);
			targ.append(std::begin(dest), std::end(dest));
		}
	}

	mReq.target(targ);

	http::write(*mStream, mReq);

	beast::error_code ec;
	http::read(*mStream, mBuffer, mRes, ec);
	while (ec == beast::http::error::end_of_stream && !mStopRequested.load())
	{
		Connect();
		http::write(*mStream, mReq);
		http::read(*mStream, mBuffer, mRes, ec);
	}
	if (ec)
		throw beast::system_error(ec);
}

void Searcher::Connect()
{
	mStream.emplace(mIoc, mSSL);
	if (!SSL_set_tlsext_host_name(mStream->native_handle(), Host.data()))
		throw beast::system_error(beast::error_code(
			static_cast<int>(::ERR_get_error()), net::error::get_ssl_category()));

	beast::get_lowest_layer(*mStream).connect(mEndpoint);
	mStream->handshake(net::ssl::stream_base::client);
}

template <typename T>
constexpr int leading_ones(T test) requires(
	std::is_integral_v<T> && sizeof(T) == sizeof(std::uint8_t))
{
#ifdef __cpp_lib_bitops
	std::countl_one(test);
#else
	for (int i = 7; i >= 0; --i)
		if (!(test & 1 << i))
			return 7 - i;
	return 8;
#endif
}

template <typename T>
struct char8_const_iterator
{
	static_assert(sizeof(T) == sizeof(char8_t));

	constexpr char8_const_iterator(T* data) : elem(data)
	{
	}

	const T* elem;

	constexpr char8_t operator*() const
	{
		return *elem;
	}
	constexpr char8_t operator[](std::ptrdiff_t index) const
	{
		return *(elem + index);
	}

	constexpr char8_const_iterator operator++(int)
	{
		return char8_const_iterator(elem++);
	}
	constexpr char8_const_iterator& operator++()
	{
		++elem;
		return *this;
	}
	constexpr char8_const_iterator operator--(int)
	{
		return char8_const_iterator(elem--);
	}
	constexpr char8_const_iterator& operator--()
	{
		--elem;
		return *this;
	}

	constexpr char8_const_iterator operator+(std::ptrdiff_t diff) const
	{
		return char8_const_iterator(elem + diff);
	}
	constexpr char8_const_iterator& operator+=(std::ptrdiff_t diff)
	{
		elem += diff;
		return *this;
	}
	constexpr char8_const_iterator operator-(std::ptrdiff_t diff) const
	{
		return char8_const_iterator(elem - diff);
	}
	constexpr char8_const_iterator& operator-=(std::ptrdiff_t diff)
	{
		elem -= diff;
		return *this;
	}
};

struct code_point_t
{
	uint32_t code_point;
	uint8_t  utf8_bytes;
};

template <typename T>
constexpr code_point_t code_point_for(const char8_const_iterator<T> first_byte)
{
	// single byte character
	if (*first_byte & (1 << 8))
		return { .code_point = *first_byte, .utf8_bytes = 1 };
	else
	{
		switch (leading_ones(*first_byte))
		{
		case 0:
			return { .code_point = *first_byte, .utf8_bytes = 1 };
			break;

		case 2:
			return { .code_point = (*first_byte & 0b00011111U) << 6U
				                   | (*(first_byte + 1) & 0b00111111U),
				.utf8_bytes = 2 };
			break;

		case 3:
			return { .code_point = (*first_byte & 0b00001111U) << 12
				                   | (*(first_byte + 1) & 0b00111111U) << 6
				                   | (*(first_byte + 2) & 0b00111111U),
				.utf8_bytes = 3 };
			break;

		case 4:
			return { .code_point = (*first_byte & 0b00000111U) << 18
				                   | (*(first_byte + 1) & 0b00111111U) << 12
				                   | (*(first_byte + 2) & 0b00111111U) << 6
				                   | (*(first_byte + 3) & 0b00111111U),
				.utf8_bytes = 4 };
			break;
		}
	}
	return code_point_t{};
}
template <typename T>
constexpr code_point_t code_point_for(const T* first_byte)
{
	return code_point_for(char8_const_iterator(first_byte));
}

std::string fix_name(std::string_view full_name)
{
	std::string name;
	name.reserve(full_name.length());

	for (auto ptr = full_name.data(); ptr < full_name.data() + full_name.size();)
	{
		// code point
		auto pt = code_point_for(ptr);
		ptr += pt.utf8_bytes;

		if (pt.utf8_bytes == 1)
		{
			if (char ch = pt.code_point;
				ch != any_of('/', '\\', ':', '*', '"', '?', '<', '>', '|', '\0'))
				// skip the above characters
				name += ch;
		}
		else if (pt.code_point == 0x0160)
			name += 'S';
		else if (0xE0 <= pt.code_point && pt.code_point <= 0xE5)
			name += 'a';
		else if (0xE8 <= pt.code_point && pt.code_point <= 0xEB)
			name += 'e';
		else if (0xEC <= pt.code_point && pt.code_point <= 0xEF)
			name += 'i';
		else if (0xF2 <= pt.code_point && pt.code_point <= 0xF6)
			name += 'o';
		else if (0xF9 <= pt.code_point && pt.code_point <= 0xFC)
			name += 'u';
		else
			std::cout << "Unhandled character '"
					  << std::string_view(ptr - pt.utf8_bytes, pt.utf8_bytes)
					  << "' (code point U+" << std::hex << pt.code_point
					  << std::dec << ")\n";
	}

	return name;
}

std::pair<std::string_view, std::string_view> split_name(std::string_view name)
{
	auto pos = name.find(" // ");
	return { name.substr(0, pos), name.substr(pos + 4) };
}

void Searcher::HandleResult(const nlohmann::json& result)
{
	auto layout = result["layout"].get<std::string_view>();

	if (layout
		== any_of("normal"sv,
			"saga"sv,
			"augment"sv,
			"host"sv,
			"planar"sv,
			"scheme"sv,
			"vanguard"sv,
			"token"sv,
			"emblem"sv,
			// for split cards, fix_name will remove the "//", so different
	        // faces are seperated with a double space
			"split"sv))
		HandleSingleFace(result, fix_name(result["name"].get<std::string_view>()));

	else if (layout == any_of("adventure"sv, "flip"sv))
		HandleSingleFace(result,
			fix_name(split_name(result["name"].get<std::string_view>()).first));

	else if (layout
			 == any_of("transform"sv,
				 "modal_dfc"sv,
				 "double_faced_token"sv,
				 "art_series"sv,
				 "double_sided"sv))
		HandleDoubleFace(result);

	else if (layout == "meld"sv)
		HandleMeld(result);

	else
		std::cout << "Unhandled layout '" << layout << "'\n";
}


Color ColorID(const nlohmann::json& src)
{
	Color colorId = Color::Colorless;
	for (const auto& col : src)
		colorId |= ColorForLetter(col.get<std::string_view>()[0]);
	return colorId;
}

template <StringViewIsh T1, StringViewIsh T2, StringViewIsh T3, StringViewIsh T4>
void Download(Downloader* downloader, T1 url, T2 name, T3 set, T4 colNum)
{
	const auto decomp = TargetFromUrl(url);

	downloader->AddToDownload({
		.host        = std::string(decomp.host),
		.target      = std::string(decomp.target),
		.destination = downloader->FullPathFor(name, set, colNum),
	});
}

void Searcher::HandleSingleFace(const nlohmann::json& card, std::string_view name)
{
	const auto set    = card["set"].get<std::string_view>(),
			   colNum = card["collector_number"].get<std::string_view>();

	// add to results
	mCards.emplace_back(Card{ CardFace{
		std::string(name),
		-1,
		std::string(colNum),
		TypesFor(card["type_line"].get<std::string_view>()),
		ColorID(card["color_identity"]),
		ManaCost::MakeCost(card["mana_cost"].get<std::string_view>()),
		std::string(set),
	} });

	// add to download
	card.operator[]("");
	Download(mDownloader,
		card["image_uris"][mDownloader->GetImageSizeString().data()]
			.get<std::string_view>(),
		name,
		set,
		colNum);
}

void Searcher::HandleDoubleFace(const nlohmann::json& card)
{
	const Color colorId = ColorID(card["color_identity"]);

	const auto set    = card["set"].get<std::string_view>(),
			   colNum = card["collector_number"].get<std::string_view>();

	const auto &front = card["card_faces"][0], &back = card["card_faces"][1];

	// add to results
	mCards.emplace_back(
		Card{ CardFace{
				  fix_name(front["name"].get<std::string>()),
				  -1,
				  std::string(colNum),
				  TypesFor(front["type_line"].get<std::string_view>()),
				  colorId,
				  ManaCost::MakeCost(front["mana_cost"].get<std::string_view>()),
				  std::string(set),
			  },
			CardFace{
				fix_name(back["name"].get<std::string>()),
				-1,
				std::string(colNum),
				TypesFor(back["type_line"].get<std::string_view>()),
				colorId,
				ManaCost::MakeCost(back["mana_cost"].get<std::string_view>()),
				std::string(set),
			} });

	// add to download
	Download(mDownloader,
		front["image_uris"][mDownloader->GetImageSizeString().data()]
			.get<std::string_view>(),
		fix_name(front["name"].get<std::string_view>()),
		set,
		colNum);
	Download(mDownloader,
		back["image_uris"][mDownloader->GetImageSizeString().data()]
			.get<std::string_view>(),
		fix_name(back["name"].get<std::string_view>()),
		set,
		colNum);
}

void Searcher::HandleMeld(const nlohmann::json& card)
{
	const Color colorId = ColorID(card["color_identity"]);

	const auto set    = card["set"].get<std::string_view>(),
			   colNum = card["collector_number"].get<std::string_view>();
	const auto name   = fix_name(card["name"].get<std::string_view>());

	const auto& parts = card["all_parts"];
	enum class Component : uint8_t
	{
		MeldPart,
		MeldResult
	} c = Component::MeldPart;
	std::string_view resultID;
	for (const auto& part : parts)
	{
		if (part["component"].get<std::string_view>() == "meld_result")
		{
			if (part["name"].get<std::string_view>() == name)
				c = Component::MeldResult;
			else
				resultID = part["id"].get<std::string_view>();
		}
	}

	if (c == Component::MeldResult)
		HandleSingleFace(card, name);
	else
	{
		mRes.body().clear();
		Get(fmt::format("/cards/{}", resultID));
		const auto result = nlohmann::json::parse(mRes.body());

		// add to results
		mCards.emplace_back(Card{
			CardFace{
				name,
				-1,
				std::string(colNum),
				TypesFor(card["type_line"].get<std::string_view>()),
				colorId,
				ManaCost::MakeCost(card["mana_cost"].get<std::string_view>()),
				std::string(set),
			},
			CardFace{
				fix_name(result["name"].get<std::string>()),
				-1,
				std::string(colNum),
				TypesFor(result["type_line"].get<std::string_view>()),
				colorId,
				ManaCost::MakeCost(result["mana_cost"].get<std::string_view>()),
				std::string(set),
			} });

		// add to download
		Download(mDownloader,
			card["image_uris"][mDownloader->GetImageSizeString().data()]
				.get<std::string_view>(),
			name,
			set,
			colNum);
		Download(mDownloader,
			result["image_uris"][mDownloader->GetImageSizeString().data()]
				.get<std::string_view>(),
			fix_name(result["name"].get<std::string_view>()),
			set,
			colNum);
	}
}
