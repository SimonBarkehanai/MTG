#include <type_traits>  // for underlying_type_t, is_enum_v

#define BITMASK_OP_REG(Ty, op)                                             \
	constexpr Ty operator op [[nodiscard]](Ty lhs, Ty rhs)                 \
	{                                                                      \
		using underlying = std::underlying_type_t<Ty>;                     \
		return static_cast<Ty>(                                            \
			static_cast<underlying>(lhs) op static_cast<underlying>(rhs)); \
	}
#define BITMASK_OP_EQ(Ty, op)                     \
	constexpr Ty& operator op##=(Ty& lhs, Ty rhs) \
	{                                             \
		return lhs = lhs op rhs;                  \
	}
#define BITMASK_OP(Ty, op) BITMASK_OP_REG(Ty, op) BITMASK_OP_EQ(Ty, op)

#define BITMASK_OPERATIONS(Ty)                                 \
	static_assert(std::is_enum_v<Ty>,                          \
		"Can't define bitmask operations for "                 \
		"non-enumeration types");                              \
	BITMASK_OP(Ty, &)                                          \
	BITMASK_OP(Ty, |)                                          \
	BITMASK_OP(Ty, ^)                                          \
	constexpr Ty operator~[[nodiscard]](Ty val)                \
	{                                                          \
		using underlying = std::underlying_type_t<Ty>;         \
		return static_cast<Ty>(~static_cast<underlying>(val)); \
	}

#define EXPORT_BITMASK_OPERATIONS(Ty) \
	export                            \
	{                                 \
		BITMASK_OPERATIONS(Ty)        \
	}

#define BITMASK_LOGICAL_NOT(Ty)                   \
	constexpr bool operator![[nodiscard]](Ty val) \
	{                                             \
		return val == static_cast<Ty>(0);         \
	}
#define EXPORT_BITMASK_LOGICAL_NOT(Ty) export BITMASK_LOGICAL_NOT(Ty)
