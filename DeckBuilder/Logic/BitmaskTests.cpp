#include <catch.hpp>

#include "BitmaskOperations.h"

enum class BitmaskTest
{
	First   = 0x01,
	Second  = 0x02,
	Third   = 0x04,
	Fourth  = 0x08,
	Fifth   = 0x10,
	Sixth   = 0x20,
	Seventh = 0x40,
	Eighth  = 0x80,
};
BITMASK_OPERATIONS(BitmaskTest)
BITMASK_LOGICAL_NOT(BitmaskTest)

TEST_CASE("Use bitwise operators", "[bitmask]")
{
	CHECK((BitmaskTest::First | BitmaskTest::Second)
		  == static_cast<BitmaskTest>(0x3));
	CHECK((BitmaskTest::First & BitmaskTest::Second)
		  == static_cast<BitmaskTest>(0x0));
	CHECK((BitmaskTest::First & ~BitmaskTest::Second)
		  == static_cast<BitmaskTest>(0x1));
	CHECK((BitmaskTest::Second & ~BitmaskTest::Second)
		  == static_cast<BitmaskTest>(0x0));
	BitmaskTest val = BitmaskTest::Sixth;
	CHECK((val |= BitmaskTest::Fifth) == static_cast<BitmaskTest>(0x30));
	CHECK(val & BitmaskTest::Fifth);
	val ^= (BitmaskTest::Fifth | BitmaskTest::Third);
	CHECK(val == (BitmaskTest::Sixth | BitmaskTest::Third));
}

TEST_CASE("Use constexpr bitwise operators", "[bitmask]")
{
	STATIC_REQUIRE((BitmaskTest::First | BitmaskTest::Second)
				   == static_cast<BitmaskTest>(0x3));
	STATIC_REQUIRE((BitmaskTest::First & BitmaskTest::Second)
				   == static_cast<BitmaskTest>(0x0));
	STATIC_REQUIRE((BitmaskTest::First & ~BitmaskTest::Second)
				   == static_cast<BitmaskTest>(0x1));
	STATIC_REQUIRE((BitmaskTest::Second & ~BitmaskTest::Second)
				   == static_cast<BitmaskTest>(0x0));
	STATIC_REQUIRE([] {
		BitmaskTest val = BitmaskTest::Sixth;
		return (val |= BitmaskTest::Fifth);
	}() == static_cast<BitmaskTest>(0x30));
	STATIC_REQUIRE(!!([] {
		BitmaskTest val = BitmaskTest::Sixth;
		return (val |= BitmaskTest::Fifth);
	}() & BitmaskTest::Fifth));
	STATIC_REQUIRE([] {
		auto val = [] {
			BitmaskTest val = BitmaskTest::Sixth;
			return (val |= BitmaskTest::Fifth);
		}();
		val ^= (BitmaskTest::Fifth | BitmaskTest::Third);
		return val;
	}() == (BitmaskTest::Sixth | BitmaskTest::Third));
}
