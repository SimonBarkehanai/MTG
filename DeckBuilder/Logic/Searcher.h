#pragma once
#include <filesystem>
#include <optional>
#include <string>
#include <vector>

#include <nlohmann/json.hpp>

#include "Card.h"
#include "Downloader.h"

class Searcher
{
public:
	explicit Searcher(Downloader* downloader);
	~Searcher();

	// @return whether the search will happen
	// (i.e. false if there already is an ongoing search)
	bool Search(std::string query);

	// @return whether the search will happen
	// (i.e. false if there already is an ongoing search)
	bool Random(std::string query = "");

	void WaitForFinish();
	void WaitForImageDownloadFinish();

	std::vector<Card>& Results() noexcept
	{
		return mCards;
	}
	std::vector<std::string>& Warnings() noexcept
	{
		return mWarnings;
	}
	auto& ResultAt(std::size_t index) noexcept
	{
		return mCards[index];
	}
	auto& ResultAt(std::size_t index) const noexcept
	{
		return mCards[index];
	}
	std::string WarningAt(std::size_t index) const noexcept
	{
		return mWarnings[index];
	}
	std::string Error() const noexcept
	{
		return mError;
	}
	bool Succeeded() const noexcept
	{
		return mError.empty();
	}
	bool Busy() const noexcept
	{
		return mBusy.load();
	}
	void RequestStop() noexcept
	{
		mStopRequested.store(true);
	}
	void Stop() noexcept;

	Downloader* GetDownloader() noexcept
	{
		return mDownloader;
	}
	const Downloader* GetDownloader() const noexcept
	{
		return mDownloader;
	}

	[[nodiscard]] std::string GetTarget(std::string_view target);

private:
	enum class SearchType
	{
		Normal,
		Random
	};

	bool Search(std::string query, SearchType type);
	void DoSearch(std::string query, SearchType type, std::size_t page = 1);

	void Get(std::string_view target);
	void Connect();

	void HandleResult(const nlohmann::json& result);
	void HandleSingleFace(const nlohmann::json& card, std::string_view name);
	void HandleDoubleFace(const nlohmann::json& card);
	void HandleMeld(const nlohmann::json& card);

	std::thread      mThread;
	std::atomic_bool mBusy, mStopRequested, mFinishedSuccessfully;

	boost::asio::io_context                                           mIoc;
	boost::asio::ssl::context                                         mSSL;
	std::optional<boost::beast::ssl_stream<boost::beast::tcp_stream>> mStream;
	boost::asio::ip::tcp::resolver::results_type                      mEndpoint;

	boost::beast::http::request<boost::beast::http::string_body>  mReq;
	boost::beast::http::response<boost::beast::http::string_body> mRes;
	boost::beast::flat_buffer                                     mBuffer;

	std::string              mError = "You haven't searched for anything yet.";
	std::vector<std::string> mWarnings;
	std::vector<Card>        mCards;

	Downloader* mDownloader;

	static constexpr int         HttpVersion = 11;
	static constexpr std::size_t MaxPages = 6, CardsPerPage = 175,
								 MaxCards = MaxPages * CardsPerPage;

	static constexpr std::string_view Host = "api.scryfall.com", Port = "443";
};
