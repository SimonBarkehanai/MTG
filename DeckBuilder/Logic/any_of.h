#pragma once
#include <concepts>
#include <tuple>
#include <type_traits>
#include <utility>

template <typename T>
concept CompFn =
	(std::is_pointer_v<T> && std::is_function_v<std::remove_pointer_t<T>>)
	|| std::is_member_function_pointer_v<T> || requires
{
	&T::operator();
};

template <typename T>
concept NotCompFn = !CompFn<T>;

template <CompFn Comparator, typename... Ts>
struct any_of
{
	template <typename... Us>
	constexpr any_of(Us&&... us) requires(
		std::same_as<Ts, std::remove_cvref_t<Us>>&&...)
		: vals(std::forward<Us>(us)...)
	{
	}

	template <typename Compare, typename... Us>
	constexpr any_of(Compare&& compare, Us&&... us) requires(
		std::same_as<Comparator,
			std::remove_cvref_t<Compare>> && (std::same_as<Ts, std::remove_cvref_t<Us>> && ...))
		: comp(std::forward<Compare>(compare)), vals(std::forward<Us>(us)...)
	{
	}

	template <typename T, typename Comp, typename... Us>
	friend constexpr bool operator==(T, any_of<Comp, Us...>);

private:
	std::tuple<Ts...> vals;
	Comparator        comp = Comparator{};

	template <typename T>
	constexpr bool eq(T compare)
	{
		return eq(compare, std::index_sequence_for<Ts...>());
	}

	template <typename T, std::size_t... Is>
	constexpr bool eq(T compare, std::index_sequence<Is...>)
	{
		return (... || (comp(compare, std::get<Is>(vals))));
	}
};

template <typename T, typename Compare, typename... Ts>
constexpr bool operator==(T lhs, any_of<Compare, Ts...> rhs)
{
	return rhs.eq(lhs);
}

template <CompFn Compare, NotCompFn... Ts>
any_of(Compare, Ts...)
	-> any_of<std::remove_cvref_t<Compare>, std::remove_cvref_t<Ts>...>;

template <typename... Ts>
any_of(Ts...) -> any_of<std::equal_to<>, std::remove_cvref_t<Ts>...>;
