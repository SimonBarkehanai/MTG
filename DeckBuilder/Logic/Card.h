#pragma once
#include <array>        // for array
#include <charconv>     // for from_chars, to_chars, to_chars_result
#include <cstdint>      // for uint8_t, uint16_t
#include <iterator>     // for begin, end, next, prev
#include <stdexcept>    // for runtime_error
#include <string>       // for string, char_traits
#include <string_view>  // for string_view, basic_string_view
#include <utility>      // for move
#include <variant>      // for variant

#include <nlohmann/json.hpp>  // for json, basic_json

#include "BitmaskOperations.h"  // for operator|, operator|=, is_bitfield

enum class Type : uint16_t
{
	NoType       = 0x000,
	Creature     = 0x0001,
	Instant      = 0x0002,
	Sorcery      = 0x0004,
	Artifact     = 0x0008,
	Enchantment  = 0x0010,
	Planeswalker = 0x0020,
	Land         = 0x0040,
	Conspiracy   = 0x0080,
	Plane        = 0x0100,
	Phenomenon   = Plane,
	Scheme       = 0x0200,
	Vanguard     = 0x0400,
};
BITMASK_OPERATIONS(Type)

// returns the most significant Type from type
// e.g. Creature from Enchantment Creature, Artifact from Artifact Enchantment
inline Type SignificantType(Type type)
{
	for (auto test : { Type::Creature,
			 Type::Instant,
			 Type::Sorcery,
			 Type::Artifact,
			 Type::Enchantment,
			 Type::Planeswalker,
			 Type::Land })
		if ((type & test) == test)
			return test;
	return type;
}

enum class Color : uint8_t
{
	White       = 0x001,
	Blue        = 0x002,
	Black       = 0x004,
	Red         = 0x008,
	Green       = 0x010,
	Colorless   = 0x000,
	W           = White,
	U           = Blue,
	B           = Black,
	R           = Red,
	G           = Green,
	C           = Colorless,
	Unspecified = static_cast<std::underlying_type_t<Color>>(-1),
};
BITMASK_OPERATIONS(Color)

constexpr char LetterForColor(Color c)
{
	switch (c)
	{
	case Color::White:
		return 'W';
	case Color::Blue:
		return 'U';
	case Color::Black:
		return 'B';
	case Color::Red:
		return 'R';
	case Color::Green:
		return 'G';
	case Color::Colorless:
		return 'C';
	}
	return '?';
}
constexpr Color ColorForLetter(char ch)
{
	switch (ch)
	{
	case 'W':
	case 'w':
		return Color::White;
	case 'B':
	case 'b':
		return Color::Blue;
	case 'U':
	case 'u':
		return Color::Black;
	case 'R':
	case 'r':
		return Color::Red;
	case 'G':
	case 'g':
		return Color::Green;
	default:
		return Color::Colorless;
	}
}
inline std::string StringForColors(Color c)
{
	std::string res;
	for (auto color :
		{ Color::White, Color::Blue, Color::Black, Color::Red, Color::Green })
		if ((c & color) != Color::Colorless)
			res += LetterForColor(color);

	if (res.empty())
		return { LetterForColor(Color::Colorless) };
	else
		return res;
}

struct HybridMana
{
	Color Color1, Color2;

	constexpr bool operator==(const HybridMana& rhs) const = default;
};

#ifdef NDEBUG
#	include "Unreachable.h"
#	define ManaCostAssert(cond, source) ((void)0)
#	define ManaCostError(source)        (UNREACHABLE())
#else
class BadManaCost : public std::runtime_error
{
public:
	explicit BadManaCost(std::string_view ManaCost)
		: runtime_error("Failed to construct a mana cost with the string '"
						+ std::string(ManaCost) + '\'')
	{
	}
};

[[noreturn]] inline void ThrowBadManaCost(std::string_view src)
{
	throw BadManaCost(src);
}

#	define ManaCostAssert(cond, source) \
		((void)((!!(cond)) || (ThrowBadManaCost(source), 0)))
#	define ManaCostError(source) ((void)(ThrowBadManaCost(source), 0))
#endif

struct ManaCost
{
	inline static constexpr auto MaxHybrid = 8;
	uint8_t Generic, White, Blue, Black, Red, Green, Colorless, X;
	std::array<HybridMana, MaxHybrid> Hybrid;
	uint8_t                           NumHybrid;

	[[nodiscard]] constexpr uint8_t CMC() const
	{
		auto cmc =
			White + Blue + Black + Red + Green + Colorless + Generic + NumHybrid;
		for (int i = 0; i < NumHybrid; ++i)
			// if it is a 2/W style hybrid, add an extra (true -> 1)
			cmc += static_cast<int>(Hybrid[i].Color1 == Color::Colorless);
		return cmc;
	}

	[[nodiscard]] constexpr auto Colors() const
	{
		auto colors = (White != 0 ? Color::White : Color::Colorless)
		              | (Blue != 0 ? Color::Blue : Color::Colorless)
		              | (Black != 0 ? Color::Black : Color::Colorless)
		              | (Red != 0 ? Color::Red : Color::Colorless)
		              | (Green != 0 ? Color::Green : Color::Colorless);
		for (int i = 0; i < NumHybrid; ++i)
			colors |= Hybrid[i].Color1 | Hybrid[i].Color2;
		return colors;
	}

	static constexpr ManaCost MakeCost(std::string_view src)
	{
		ManaCost cost{};
		if (!src.starts_with('{'))
			for (auto ch : src)
			{
				switch (ch)
				{
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
					cost.Generic *= 10;
					cost.Generic += ch - '0';
					break;
				case 'W':
				case 'w':
					++cost.White;
					break;
				case 'U':
				case 'u':
					++cost.Blue;
					break;
				case 'B':
				case 'b':
					++cost.Black;
					break;
				case 'R':
				case 'r':
					++cost.Red;
					break;
				case 'G':
				case 'g':
					++cost.Green;
					break;
				case 'C':
				case 'c':
					++cost.Colorless;
					break;
				case 'X':
				case 'x':
					++cost.X;
					break;
				default:
					ManaCostError(src);
				}
			}
		else
			for (auto it = src.begin(); it != src.end();)
			{
				// handle double mana costs, i.e. src == " // {cost}"
				if (*it == ' ')
					it += 4;
				ManaCostAssert(*it == '{', src);
				if (*(it + 2) == '}')  // normal, single character symbol
				{
					++it;
					switch (*it)
					{
					case '0':
					case '1':
					case '2':
					case '3':
					case '4':
					case '5':
					case '6':
					case '7':
					case '8':
					case '9':
						cost.Generic = *it - '0';
						break;
					case 'W':
					case 'w':
						++cost.White;
						break;
					case 'U':
					case 'u':
						++cost.Blue;
						break;
					case 'B':
					case 'b':
						++cost.Black;
						break;
					case 'R':
					case 'r':
						++cost.Red;
						break;
					case 'G':
					case 'g':
						++cost.Green;
						break;
					case 'C':
					case 'c':
						++cost.Colorless;
						break;
					case 'X':
					case 'x':
						++cost.X;
						break;
					default:
						ManaCostError(src);
					}
					it += 2;  // advance to next '{' or end
				}
				else if (*(it + 3) == '}')  // 2 digit number
				{
					std::from_chars(&*(it + 1), &*(it + 2), cost.Generic);
					it += 4;
				}
				else  // hybrid in the form W/U or 2/R
				{
					ManaCostAssert(*(it + 2) == '/', src);
					ManaCostAssert(cost.NumHybrid < MaxHybrid, src);
					++it;
					switch (*it)  // first color
					{
					case 'W':
					case 'w':
						cost.Hybrid[cost.NumHybrid].Color1 = Color::White;
						break;
					case 'U':
					case 'u':
						cost.Hybrid[cost.NumHybrid].Color1 = Color::Blue;
						break;
					case 'B':
					case 'b':
						cost.Hybrid[cost.NumHybrid].Color1 = Color::Black;
						break;
					case 'R':
					case 'r':
						cost.Hybrid[cost.NumHybrid].Color1 = Color::Red;
						break;
					case 'G':
					case 'g':
						cost.Hybrid[cost.NumHybrid].Color1 = Color::Green;
						break;
					case '2':
						cost.Hybrid[cost.NumHybrid].Color1 = Color::Colorless;
						break;
					default:
						ManaCostError(src);
					}

					it += 2;  // advance to second color

					switch (*it)  // second color
					{
					case 'W':
					case 'w':
						cost.Hybrid[cost.NumHybrid].Color2 = Color::White;
						break;
					case 'U':
					case 'u':
						cost.Hybrid[cost.NumHybrid].Color2 = Color::Blue;
						break;
					case 'B':
					case 'b':
						cost.Hybrid[cost.NumHybrid].Color2 = Color::Black;
						break;
					case 'R':
					case 'r':
						cost.Hybrid[cost.NumHybrid].Color2 = Color::Red;
						break;
					case 'G':
					case 'g':
						cost.Hybrid[cost.NumHybrid].Color2 = Color::Green;
						break;
					default:
						ManaCostError(src);
					}

					it += 2;  // advance to next '{' or end
					++cost.NumHybrid;
				}
			}
		return cost;
	}

	explicit operator std::string() const
	{
		std::string res;
		for (uint8_t i = 0; i < X; ++i)
			res += "{X}";
		if (Generic != 0)
		{
			char dest[4]{ '{' };
			auto result =
				std::to_chars(std::begin(dest) + 1, std::end(dest) - 1, Generic);
			*result.ptr = '}';  // end with }
			res.append(&*std::begin(dest), result.ptr + 1);
		}
		for (uint8_t i = 0; i < White; ++i)
			res += "{W}";
		for (uint8_t i = 0; i < Blue; ++i)
			res += "{U}";
		for (uint8_t i = 0; i < Black; ++i)
			res += "{B}";
		for (uint8_t i = 0; i < Red; ++i)
			res += "{R}";
		for (uint8_t i = 0; i < Green; ++i)
			res += "{G}";
		for (uint8_t i = 0; i < Colorless; ++i)
			res += "{C}";
		for (uint8_t i = 0; i < NumHybrid; ++i)
		{
			res.reserve(res.size() + 5);
			res += '{';
			res += Hybrid[i].Color1 == Color::Colorless
			           ? '2'
			           : LetterForColor(Hybrid[i].Color1);
			res += '/';
			res += LetterForColor(Hybrid[i].Color2);
			res += '}';
		}
		return res;
	}

	constexpr bool operator==(const ManaCost& rhs) const = default;
};

class CardFace
{
public:
	template <typename T1, typename T2, typename T3>
	CardFace(T1&&       name,
		int             quantity,
		T2&&            collectorNum,
		Type            types,
		Color           colorID,
		const ManaCost& cost,
		T3&&            setCode)
		: mQuantity(quantity)
		, mTypes(types)
		, mColorID(colorID)
		, mCost(cost)
		, mName(std::forward<T1>(name))
		, mSet(std::forward<T2>(setCode))
		, mCollectorNumber(std::forward<T3>(collectorNum))
	{
	}

	explicit CardFace(const nlohmann::json& src)
		: mQuantity(src["quantity"])
		, mTypes(src["types"])
		, mColorID(src["color_id"])
		, mCost(ManaCost::MakeCost(src["cost"].get<std::string_view>()))
		, mName(src["name"])
		, mSet(src["set"])
		, mCollectorNumber(src["collector_number"])
	{
		static_assert(false, "json implicit conv???");
	}

	static CardFace EmptyCardFace()
	{
		return CardFace{
			"", 0, "", Type::NoType, Color::Colorless, ManaCost{}, ""
		};
	}

	explicit operator nlohmann::json() const
	{
		return { { "quantity", mQuantity },
			{ "collector_number", mCollectorNumber },
			{ "types", mTypes },
			{ "color_id", mColorID },
			{ "cost", static_cast<std::string>(mCost) },
			{ "name", mName },
			{ "set", mSet } };
	}

	[[nodiscard]] int GetQuantity() const noexcept
	{
		return mQuantity;
	}
	void SetQuantity(int newQuantity) noexcept
	{
		mQuantity = newQuantity;
	}

	[[nodiscard]] std::string GetCollectorNumber() const
	{
		return mCollectorNumber;
	}
	template <typename T>
	void SetCollectorNumber(T&& newColNum)
	{
		mCollectorNumber = std::forward<T>(newColNum);
	}

	[[nodiscard]] Type GetTypes() const noexcept
	{
		return mTypes;
	}
	void SetTypes(Type newTypes) noexcept
	{
		mTypes = newTypes;
	}

	[[nodiscard]] Color GetColorID() const noexcept
	{
		return mColorID;
	}
	void SetColorID(Color newColorID) noexcept
	{
		mColorID = newColorID;
	}

	[[nodiscard]] ManaCost GetCost() const noexcept
	{
		return mCost;
	}
	void SetCost(const ManaCost& newCost) noexcept
	{
		mCost = newCost;
	}

	[[nodiscard]] std::string GetName() const
	{
		return mName;
	}
	template <typename T>
	void SetName(T&& newName)
	{
		mName = std::forward<T>(newName);
	}

	[[nodiscard]] std::string GetSet() const
	{
		return mSet;
	}
	template <typename T>
	void SetSet(T&& newSet)
	{
		mSet = std::forward<T>(newSet);
	}

private:
	int         mQuantity;
	Type        mTypes;
	Color       mColorID;
	ManaCost    mCost;
	std::string mName, mSet, mCollectorNumber;
};

inline auto CardArrayFromJson(const nlohmann::json& src)
{
	if (src.type() == nlohmann::json::value_t::array)
		return std::array{ CardFace(src[0]), CardFace(src[1]) };
	else
		return std::array{ CardFace(src), CardFace::EmptyCardFace() };
}

class Card /* : public std::array<CardFace, 2>*/
{
	std::array<CardFace, 2>     mFaces;
	decltype(mFaces)::size_type mNumFaces = 1;

public:
	explicit Card(const CardFace& front)
		: mFaces{ front, CardFace::EmptyCardFace() }, mNumFaces(1)
	{
	}

	Card(const CardFace& front, const CardFace& back)
		: mFaces{ front, back }, mNumFaces(2)
	{
	}

	explicit Card(const nlohmann::json& src)
		: mFaces{ CardArrayFromJson(src) }
		, mNumFaces(src.type() == nlohmann::json::value_t::array ? 2 : 1)
	{
	}

	[[nodiscard]] decltype(auto) front() noexcept
	{
		return mFaces.front();
	}
	[[nodiscard]] decltype(auto) front() const noexcept
	{
		return mFaces.front();
	}
	[[nodiscard]] decltype(auto) back() noexcept
	{
		return mFaces.back();
	}
	[[nodiscard]] decltype(auto) back() const noexcept
	{
		return mFaces.back();
	}

	[[nodiscard]] nlohmann::json to_json() const
	{
		return mNumFaces == 1
		           ? static_cast<nlohmann::json>(front())
		           : nlohmann::json{ static_cast<nlohmann::json>(front()),
						 static_cast<nlohmann::json>(back()) };
	}

	[[nodiscard]] int GetQuantity() const noexcept
	{
		return front().GetQuantity();
	}
	void SetQuantity(int newQuantity) noexcept
	{
		front().SetQuantity(newQuantity);
	}

	[[nodiscard]] std::string GetCollectorNumber() const
	{
		return front().GetCollectorNumber();
	}
	template <typename T>
	void SetCollectorNumber(T&& newColNum)
	{
		front().SetCollectorNumber(std::forward<T>(newColNum));
	}

	[[nodiscard]] Type GetTypes() const noexcept
	{
		return front().GetTypes() | back().GetTypes();
	}

	[[nodiscard]] Color GetColorID() const noexcept
	{
		return front().GetColorID() | back().GetColorID();
	}

	[[nodiscard]] std::string GetName() const
	{
		return mNumFaces == 1 ? front().GetName()
		                      : front().GetName() + " // " + back().GetName();
	}
	template <typename T>
	void SetName(T&& newName)
	{
		if (auto pos = newName.find(" // "); pos != std::string::npos)
		{
			front().SetName(newName.substr(0, pos));
			back().SetName(newName.substr(pos + 4));
		}
		else
			front().SetName(std::forward<T>(newName));
	}

	[[nodiscard]] std::string GetSet() const
	{
		return front().GetSet();
	}
	template <typename T>
	void SetSet(T&& newSet)
	{
		front().SetSet(std::forward<T>(newSet));
	}
};
