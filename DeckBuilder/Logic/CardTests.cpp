#include <array>        // for array
#include <string>       // for char_traits, string, basic_string
#include <string_view>  // for string_view, basic_string_view

#include <catch.hpp>  // for AssertionHandler, operator""_catch_sr

#include "BitmaskOperations.h"  // for operator|
#include "Card.h"

constexpr auto Gen = 1, W = 2, U = 3, B = 4, R = 5, G = 6, C = 7, X = 8;

TEST_CASE("Normal ManaCosts Test", "[manacost]")
{
	ManaCost c{ Gen, W, U, B, R, G, C, X };
	CHECK(c.CMC() == (Gen + W + U + B + R + G + C));
	CHECK(c.Colors()
		  == (Color::White | Color::Blue | Color::Black | Color::Red
			  | Color::Green));
	constexpr std::string_view longCost =
								   "{X}{X}{X}{X}{X}{X}{X}{X}{1}{W}{W}{U}{U}{U}"
								   "{B}{B}{B}{B}{R}{R}{R}{R}{R}"
								   "{G}{G}{G}{G}{G}{G}{C}{C}{C}{C}{C}{C}{C}",
							   shortCost =
								   "1WWUUUBBBBRRRRRGGGGGGCCCCCCCXXXXXXXX";
	CHECK(static_cast<std::string>(c) == longCost);
	CHECK(c == ManaCost::MakeCost(longCost));
	CHECK(c == ManaCost::MakeCost(shortCost));
}

TEST_CASE("Hybrid ManaCosts Test", "[manacost]")
{
	SECTION("2 color hybrid")
	{
		ManaCost c{ 1,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			{ { Color::White, Color::Blue, Color::Blue, Color::Red } },
			2 };
		CHECK(c.CMC() == 3);
		CHECK(c.Colors() == (Color::White | Color::Blue | Color::Red));
		constexpr std::string_view cost = "{1}{W/U}{U/R}";
		CHECK(static_cast<std::string>(c) == cost);
		CHECK(c == ManaCost::MakeCost(cost));
	}
	SECTION("Colorless hybrid")
	{
		constexpr std::array hybrid{
			HybridMana{
				Color::Colorless,
				Color::White,
			},
			HybridMana{
				Color::Colorless,
				Color::Blue,
			},
			HybridMana{
				Color::Colorless,
				Color::Black,
			},
			HybridMana{
				Color::Colorless,
				Color::Red,
			},
			HybridMana{
				Color::Colorless,
				Color::Green,
			},
		};
		constexpr ManaCost c{ 0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			{
				hybrid[0],
				hybrid[1],
				hybrid[2],
				hybrid[3],
				hybrid[4],
			},
			hybrid.size() };
		CHECK(c.CMC() == 10);
		CHECK(c.Colors()
			  == (Color::White | Color::Blue | Color::Black | Color::Red
				  | Color::Green));
		constexpr std::string_view cost = "{2/W}{2/U}{2/B}{2/R}{2/G}";
		CHECK(static_cast<std::string>(c) == cost);
		CHECK(c == ManaCost::MakeCost(cost));
	}
}
