#pragma once

#include <string_view>

void download(std::string_view exec,
	std::string_view           download   = {},
	std::string_view           forge_dir  = {},
	bool                       assume_yes = false);
