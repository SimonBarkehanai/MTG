#include "Downloader.h"

#include <array>
#include <charconv>
#include <concepts>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <map>
#include <span>
#include <string>
#include <thread>

#include <boost/json.hpp>
#include <boost/predef/os.h>
#include <getter.h>
#include <searcher.h>

template <typename CharT, typename Traits, typename Allocator>
std::basic_istream<CharT, Traits>& GetLine(std::basic_istream<CharT, Traits>& input,
	std::basic_string<CharT, Traits, Allocator>& str)
{
	std::getline(input, str);
	std::erase(str, '\r');
	return input;
}

#if BOOST_OS_WINDOWS == BOOST_VERSION_NUMBER_NOT_AVAILABLE \
	&& BOOST_OS_LINUX == BOOST_VERSION_NUMBER_NOT_AVAILABLE
#	pragma message( \
		"Warning: This operating system has not been tested.  Assuming a Linux-like OS.")
#endif

//#if BOOST_OS_WINDOWS != BOOST_VERSION_NUMBER_NOT_AVAILABLE
//#	define DEF_FORGE_DIR "D:/Program Files/Forge/"
//#else
//#	ifndef NO_ASSUME_WSL
//#		define NO_ASSUME_WSL 0
//#		pragma message( \
//			"Assuming WSL.  To build for non-WSL Linux, define NO_ASSUME_WSL to
// 1") #	endif #	if NO_ASSUME_WSL #		define DEF_FORGE_DIR
// "/usr/games/Forge/" #	else #		define DEF_FORGE_DIR "/mnt/c/Program
// Files/Forge/" #	endif #endif

constexpr std::string_view DefaultForgeDir = {};  // DEF_FORGE_DIR;

//#undef DEF_FORGE_DIR

using namespace std::literals;
namespace this_thread = std::this_thread;
namespace fs          = std::filesystem;

auto to_upper(std::string str)
{
	std::transform(str.begin(), str.end(), str.begin(), [](char ch) -> char {
		return (static_cast<unsigned>(ch) - 'a') < 26 ? ch - ('a' - 'A') : ch;
	});
	return str;
}

auto findPicsDir(std::string_view ForgeDir)
{
	if (ForgeDir.empty())
		ForgeDir = DefaultForgeDir;
	if (fs::exists(ForgeDir))
	{
		constexpr std::string_view const ExamplePropertiesFile =
											 "forge.profile.properties.example",
										 PropertiesFile =
											 ExamplePropertiesFile.substr(0, 24);
		fs::path propertiesPath = fs::path(ForgeDir) / PropertiesFile;
		if (!fs::exists(propertiesPath))
			propertiesPath = fs::path(ForgeDir) / ExamplePropertiesFile;
		if (!fs::exists(propertiesPath))
			throw std::runtime_error(
				(propertiesPath.parent_path() / PropertiesFile).string()
				+ " doesn't exist");
		std::ifstream istr(propertiesPath);
		std::string   line, cacheDir, cardPicsDir;
		while (std::getline(istr, line))
		{
			if (line.starts_with("cacheDir"))
				cacheDir = line.substr(9);
			if (line.starts_with("cardPicsDir"))
				cardPicsDir = line.substr(12);
		}
		// if empty or whitespace
		if (cardPicsDir.empty()
			|| std::all_of(cardPicsDir.begin(), cardPicsDir.end(), [](char ch) {
				   return ch == ' ' || (ch >= '\x9' && ch <= '\xd');
			   }))
			cardPicsDir = cacheDir + "/pics/cards/";

		std::replace(begin(cardPicsDir), end(cardPicsDir), '\\', '/');

		if (std::string_view{ cardPicsDir }.starts_with("./"))
			cardPicsDir = std::string{ ForgeDir } + cardPicsDir.substr(1);

		auto pos = cardPicsDir.rfind("//");
		while (pos != std::string::npos)
		{
			cardPicsDir.erase(pos);
			pos = cardPicsDir.rfind("//");
		}

		if (!cardPicsDir.ends_with('/'))
			cardPicsDir += '/';

		return cardPicsDir;
	}
	else
		throw std::runtime_error{ "Invalid Forge directory: "
								  + std::string{ ForgeDir } };
}

class Downloader
{
public:
	Downloader(std::string_view exec,
		std::string_view        inForgeDir  = "",
		bool                    inAssumeYes = false);

	void Download(std::string_view toDownload);

private:
	Downloader(std::string_view exec,
		std::string const&      scryfall_cert,
		std::string_view        forge_dir,
		bool                    assume_yes);

	struct download_info
	{
		std::string url, filename, set;
	};

	void                       DownloadCard(std::string_view name,
							  std::string_view               forge_set,
							  std::span<unsigned>            indices = {});
	std::vector<download_info> downloads_for_search(std::string_view search);
	std::vector<download_info> all_downloads_for_search(std::string_view search);
	std::vector<download_info> downloads_for_search(
		std::string_view search, unsigned index, std::string_view index_str = {});
	std::vector<download_info> downloads_for_search(
		std::string_view search, std::span<unsigned> indices = {});
	std::vector<download_info> downloads_for_search_impl(
		unsigned index, std::string_view index_str = {});
	void DownloadDeck(std::string_view deck);
	void DownloadDirectory(std::string_view directory);

	void DownloadImage(std::string_view url, std::string_view dest);
	void DownloadImage(int host, std::string_view target, std::string_view dest);

	std::string set_dir(std::string_view set);

	struct set_codes
	{
		std::string forge_name, forge_img_folder, scryfall_name;
	};

	const std::string                             mPicsDir;
	std::map<std::string, set_codes, std::less<>> m_forge_map, m_scryfall_map;
	const bool                                    mAssumeYes = false;

	getter                          mApiGetter;
	searcher                        mSearcher;
	static constexpr auto           NumImgHosts = 3;
	std::array<getter, NumImgHosts> mImgGetters;
	std::vector<card_t>             mResults;
};

Downloader::Downloader(
	std::string_view exec, std::string_view inForgeDir, bool inAssumeYes)
	: Downloader(exec,
		fs::absolute(exec)                          // make exec absolute
			.replace_filename("scryfall_cert.pem")  // replace executable with
                                                    // cert
			.lexically_normal()                     // remove /foo/../ and /./
			.string(),
		inForgeDir,
		inAssumeYes)
{
}

Downloader::Downloader(std::string_view exec,
	std::string const&                  scryfall_cert,
	std::string_view                    inForgeDir,
	bool                                assume_yes)
	: mAssumeYes(assume_yes)
	, mPicsDir(findPicsDir(inForgeDir))
	, mApiGetter("api.scryfall.com", scryfall_cert)
	, mSearcher(exec)
	, mImgGetters{ getter{ "c1.scryfall.com", scryfall_cert },
		getter{ "c2.scryfall.com", scryfall_cert },
		getter{ "c3.scryfall.com", scryfall_cert } }
{
	std::string_view ForgeDir = inForgeDir.empty() ? DefaultForgeDir : inForgeDir;

	// populate SetMaps
	for (auto& p : fs::directory_iterator(fs::path(ForgeDir) / "res" / "editions"))
	{
		std::ifstream istr(p.path());
		std::string   line;
		set_codes     codes;
		while (GetLine(istr, line) && !line.starts_with("[cards]"))
		{
			if (line.starts_with("Code="))
				codes.forge_name = to_upper(line.substr(5));
			else if (line.starts_with("Code2="))
				codes.forge_img_folder = to_upper(line.substr(6));
			else if (line.starts_with("Alias="))
				codes.scryfall_name = to_upper(line.substr(6));
		}
		if (codes.forge_name == "DDL" || codes.forge_name == "EVE"
			|| codes.forge_name == "V13" || codes.forge_name == "SLD")
			codes.scryfall_name = codes.forge_name;
		if (codes.forge_name == "MPS_GRN" || codes.forge_name == "MPS_RNA"
			|| codes.forge_name == "MPS_WAR")
			codes.scryfall_name = "MED";
		if (codes.forge_name == "PSLD")
			codes.scryfall_name = "SLD";

		if (codes.forge_name.empty())
			throw std::runtime_error("Couldn't find correct set codes.");
		if (codes.scryfall_name.length() <= 2)
			codes.scryfall_name = codes.forge_name;
		if (codes.forge_img_folder.empty())
			codes.forge_img_folder = codes.forge_name;

		// if (codes.forge_name == "EVE")
		//	codes.scryfall_name = "EVE";
		// if (codes.forge_name == "PSLD")
		//	codes.scryfall_name = "SLD";

		m_forge_map.emplace(codes.forge_name, codes);
		m_scryfall_map.emplace(codes.scryfall_name, codes);
	}
}

void Downloader::Download(std::string_view toDownload)
{
	if (fs::is_directory(toDownload))
		DownloadDirectory(toDownload);
	else if (fs::is_regular_file(toDownload))
		DownloadDeck(toDownload);
	else
	{
		for (auto const& dl : all_downloads_for_search(toDownload))
		{
			DownloadImage(dl.url,
				set_dir(m_scryfall_map.at(to_upper(dl.set)).forge_img_folder)
					+ dl.filename);
		}
	}
}

void Downloader::DownloadCard(
	std::string_view name, std::string_view forge_set, std::span<unsigned> indices)
{
	std::string ScryfallSet{ forge_set }, dir{ forge_set };
	if (auto it = m_forge_map.find(forge_set); it != m_forge_map.end())
	{
		ScryfallSet = it->second.scryfall_name;
		dir         = it->second.forge_img_folder;
	}

	const auto query = "!\"" + std::string(name) + "\" unique:prints";
	std::pmr::vector<card_t> cards;
	bool                     err = false;
	for (auto const& dl :
		downloads_for_search(query + " e:" + ScryfallSet, indices))
		DownloadImage(dl.url, set_dir(dir) + dl.filename);
}

template <auto sz>
	requires std::integral<decltype(sz)>
struct char_array
{
	static constexpr auto size = sz;
	using data_type            = char[size];
	data_type data{};

	char_array() = default;
	template <std::integral T>
	char_array(T num)
	{
		assert(num >= 0 && num <= 99);
		std::to_chars(std::begin(data), std::end(data), num);
	}
	template <typename Arg1, typename... ArgN>
		requires(!std::integral<Arg1>)
	constexpr char_array(Arg1&& arg1, ArgN&&... argn)
		: data{ std::forward<Arg1>(arg1), std::forward<ArgN>(argn)... }
	{
	}

	constexpr operator char const *() const
	{
		return begin();
	}
	constexpr operator char*()
	{
		return begin();
	}
	constexpr operator std::string_view() const
	{
		return std::string_view{ begin(), end() };
	}

	constexpr auto begin()
	{
		return data;
	}
	constexpr auto begin() const
	{
		return data;
	}
	constexpr auto end()
	{
		return std::find(data, data + size, '\0');
	}
	constexpr auto end() const
	{
		return std::find(data, data + size, '\0');
	}
};
template <typename CharT, typename Traits, typename Alloc>
auto operator+(std::basic_string<CharT, Traits, Alloc> const& lhs,
	std::basic_string_view<CharT, Traits>                     rhs)
{
	return lhs + std::basic_string<CharT, Traits, Alloc>{ rhs };
}
template <typename CharT, typename Traits, typename Alloc>
auto operator+(std::basic_string<CharT, Traits, Alloc>&& lhs,
	std::basic_string_view<CharT, Traits>                rhs)
{
	return std::move(lhs.append(rhs));
}

auto clean_name(std::string str)
{
	std::erase(str, ':');
	std::erase(str, '"');
	return str;
}

std::vector<Downloader::download_info> Downloader::downloads_for_search(
	std::string_view search)
{
	return downloads_for_search(search, 0);
}

std::vector<Downloader::download_info> Downloader::all_downloads_for_search(
	std::string_view search)
{
	mResults = mSearcher.search(search);

	std::vector<download_info> res;

	for (int i = 0; i < mResults.size(); ++i)
	{
		auto others = downloads_for_search_impl(i);
		res.insert(end(res),
			std::make_move_iterator(begin(others)),
			std::make_move_iterator(end(others)));
	}
	return res;
}

std::vector<Downloader::download_info> Downloader::downloads_for_search(
	std::string_view search, unsigned index, std::string_view index_str)
{
	mResults = mSearcher.search(search);
	return downloads_for_search_impl(index, index_str);
}

std::vector<Downloader::download_info> Downloader::downloads_for_search_impl(
	unsigned index, std::string_view index_str)
{
	auto const& card = mResults[index];

	std::vector<download_info> res;

	using enum layout_e;
	switch (card.layout)
	{
	case transform:
	case double_faced_token:
	case modal_dfc:
	case double_sided:
		// two images
		res.push_back({ .url = std::string{ card.card_faces[0].image_uris.large },
			.filename =
				std::string{ card.card_faces[0].name + index_str + ".full.jpg" },
			.set = std::string{ card.set } });
		res.push_back({ .url = std::string{ card.card_faces[1].image_uris.large },
			.filename =
				std::string{ card.card_faces[1].name + index_str + ".full.jpg" },
			.set = std::string{ card.set } });
		break;
	case flip:
	case adventure:
		// one image, one name
		res.push_back({ .url = std::string{ card.image_uris.large },
			.filename =
				std::string{ card.card_faces[0].name + index_str + ".full.jpg" },
			.set = std::string{ card.set } });
		break;
	case meld:
		// meld, need to also download meld result
		for (auto part : card.all_parts)
		{
			if (part.component == component_e::meld_result)
			{
				res.push_back({ .url = std::string{ part.uri },
					.filename = std::string{ part.name + index_str + ".full.jpg" },
					.set      = std::string{ card.set } });
				break;
			}
		}
		res.push_back({ .url = std::string{ card.image_uris.large },
			.filename = std::string{ card.name + index_str + ".full.jpg" },
			.set      = std::string{ card.set } });
		break;
	case split:
		// one image, two names
		res.push_back({ .url = std::string{ card.image_uris.large },
			.filename =
				std::string{ card.card_faces[0].name + card.card_faces[1].name
							 + index_str + ".full.jpg" },
			.set = std::string{ card.set } });
		break;
	case normal:
	case leveler:
	case saga:
	case planar:
	case scheme:
	case vanguard:
	case token:
	case emblem:
	case augment:
	case host:
	case art_series:
		[[likely]];
		res.push_back({ .url = std::string{ card.image_uris.large },
			.filename = std::string{ card.name + index_str + ".full.jpg" },
			.set      = std::string{ card.set } });
		break;
	}

	return res;
}

std::vector<Downloader::download_info> Downloader::downloads_for_search(
	std::string_view search, std::span<unsigned> indices)
{
	mResults = mSearcher.search(search);

	std::vector<download_info> res;

	if (indices.empty())
	{
		auto others = downloads_for_search_impl(0);
		res.insert(end(res),
			std::make_move_iterator(begin(others)),
			std::make_move_iterator(end(others)));
	}

	for (auto const index : indices)
	{
		auto others = downloads_for_search_impl(index, char_array<3>{ index + 1 });
		res.insert(end(res),
			std::make_move_iterator(begin(others)),
			std::make_move_iterator(end(others)));
	}
	return res;
}

void Downloader::DownloadDeck(std::string_view deck)
{
	std::ifstream deck_file(fs::path{ deck });
	std::string   data;

	while (std::getline(deck_file, data))
	{
		auto const data_view = std::string_view{ data };
		auto const space = data_view.find(' '), firstPipe = data_view.find('|');
		if (firstPipe == data_view.npos)
			continue;
		auto const secondPipe = data_view.find('|', firstPipe + 1);

		std::string_view const name = data_view.substr(
								   space + 1, firstPipe - space - 1),
							   set = data_view.substr(
								   firstPipe + 1, secondPipe - firstPipe - 1);


		if (secondPipe != std::string::npos)
		{
			std::array<unsigned, 1> index;
			auto const index_str = data_view.substr(secondPipe + 1);
			std::from_chars(
				index_str.data(), index_str.data() + index_str.size(), index[0]);
			index[0] -= 1;
			DownloadCard(name, set, index);
		}
		else
			DownloadCard(name, set);
	}
}

void Downloader::DownloadDirectory(std::string_view directory)
{
	for (const auto& p : fs::directory_iterator{ directory })
	{
		auto const str = p.path().string();
		if (!mAssumeYes)
		{
			std::cout << "Download '" << str << "'? [Y/n] ";
			char let;
			std::cin >> let;
			if (let == 'n' || let == 'N')
				continue;
		}
		if (fs::is_directory(p.path()))
			DownloadDirectory(str);
		else
			DownloadDeck(str);
	}
}

void Downloader::DownloadImage(std::string_view url, std::string_view dest)
{
	auto host = [](std::string_view url) {
		auto const host_begin = url.data() + url.find("://") + 3;
		int        res;
		std::from_chars(host_begin + 1, host_begin + 2, res);
		return res - 1;
	};
	auto const target = [](std::string_view url) {
		auto const host_begin = url.find("://") + 3,
				   host_end   = url.find('/', host_begin);
		return url.substr(host_end);
	};

	DownloadImage(host(url), target(url), dest);
}

void Downloader::DownloadImage(
	int host, std::string_view target, std::string_view dest)
{
	mImgGetters[host].download_file(target, dest);
}

std::string Downloader::set_dir(std::string_view set)
{
	std::string res;
	res.reserve(mPicsDir.size() + set.size() + 1);
	res.append(mPicsDir).append(set).append(1, '/');
	if (!fs::exists(res))
		fs::create_directories(res);
	return res;
}

void download(std::string_view exec,
	std::string_view           download,
	std::string_view           forge_dir,
	bool                       assume_yes)
{
	Downloader{ exec, forge_dir, assume_yes }.Download(download);
}
