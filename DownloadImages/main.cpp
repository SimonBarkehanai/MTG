#include <filesystem>
#include <iostream>

#include <boost/program_options.hpp>

#include "Downloader.h"

namespace fs = std::filesystem;
namespace po = boost::program_options;

void Usage(const std::string& progName, const po::options_description& options)
{
	std::cout << "Usage:\n  " << progName << " [options] <Deck File>\n  "
			  << progName << " [options] <Deck Directory>\n\n"
			  << options << "\nExample:\n  " << progName
			  << " \"/mnt/c/Users/simon/AppData/Roaming/Forge/decks/"
				 "constructed/Legacy Ninjas.dck\"\n  "
			  << progName << R"( -y -d "C:\Program Files\Forge")"
			  << R"( "C:\Users\simon\AppData\Roaming\Forge\decks\commander")"
			  << '\n';
}

int main(int argc, const char* argv[])
{
	std::string ForgeDir, toDownload;
	bool        assumeYes = false;

	// Check usage
	{
		po::options_description desc("Options"), hidden(""), all("");
		desc.add_options()("help,h", "Displays usage info")("assume-yes,y",
			"Downloads all decks in the directory")("forge-dir,d",
			po::value<decltype(ForgeDir)>(&ForgeDir)->value_name("ForgeDir"),
			"Directory Forge was installed to");
		hidden.add_options()("file,f",
			po::value<decltype(toDownload)>(&toDownload),
			"File to download");

		po::positional_options_description p;
		p.add("file", -1);

		all.add(hidden).add(desc);

		po::variables_map vm;
		po::store(
			po::command_line_parser(argc, argv).options(all).positional(p).run(),
			vm);
		po::notify(vm);

		if (vm.count("help"))
		{
			Usage(argv[0], desc);
			return 2;
		}

		if (vm.count("file") != 1)
		{
			Usage(argv[0], desc);
			return 1;
		}

		if (vm.count("assume-yes"))
			assumeYes = true;
	}

	download(argv[0], toDownload, ForgeDir, assumeYes);
}
