#include "Timer.h"
#ifdef __cpp_lib_format
#	include <format>
#else
#	include <fmt/format.h>
namespace std
{
using namespace fmt;
}
#endif
#include <string_view>  // for u16string_view

#include <QChar>   // for QStyle
#include <QFont>   // for QFont
#include <QStyle>  // for QStyle

using namespace std::literals;
namespace chrono = std::chrono;

constexpr std::u16string_view rawData{
	u".Timer{border:2px solid #aaaaaa;border-radius:10px 20px;color:white;}"
	uR"(.Timer[color="black"]{background-color:black})"
	uR"(.Timer[color="blue"]{background-color:#00b0e0})"
	uR"(.Timer[color="red"]{background-color:#e00000})"
};
const QString stylesheet = QString::fromRawData(
	reinterpret_cast<const QChar*>(rawData.data()), rawData.size());

Timer::Timer(std::chrono::seconds InitialTime, QWidget* parent, Qt::WindowFlags f)
	: QLabel(parent, f), mTimeLeft(InitialTime)
{
	setFixedSize(mSize);
	setAlignment(Qt::AlignmentFlag::AlignHCenter | Qt::AlignmentFlag::AlignVCenter);
	mTimer->setInterval(std::chrono::seconds(1));

	Draw(InitialTime.count() <= 10 ? 11 : 0);
	setStyleSheet(stylesheet);
	auto fnt = font();
	fnt.setPointSize(11);
	setFont(fnt);

	setText(QString::fromStdString(std::format("{:0>2}:{:0>2}",
		chrono::floor<chrono::minutes>(mTimeLeft).count(),
		mTimeLeft.count() % 60)));
	connect(mTimer, &QTimer::timeout, this, [this]() {
		int oldTimeLeft = mTimeLeft.count();
		--mTimeLeft;
		Draw(oldTimeLeft);
		if (mTimeLeft == std::chrono::seconds(0))
		{
			mTimer->stop();
			emit TimeUp();
		}
	});
}

Timer::Timer(QWidget* parent, Qt::WindowFlags f)
	: Timer(chrono::seconds(0), parent, f)
{
}

Timer::Timer(
	AutoStart_t, std::chrono::seconds InitialTime, QWidget* parent, Qt::WindowFlags f)
	: Timer(InitialTime, parent, f)
{
	Start();
}

void Timer::Start(std::chrono::seconds dur)
{
	SetTime(dur);
	Start();
}

void Timer::SetTime(std::chrono::seconds dur)
{
	auto oldTimeLeft = mTimeLeft.count();
	mTimeLeft        = dur;
	Draw(oldTimeLeft);
}

void Timer::Draw(int oldTimerValue)
{
	setText(QString::fromStdString(std::format("{:0>2}:{:0>2}",
		chrono::floor<chrono::minutes>(mTimeLeft).count(),
		mTimeLeft.count() % 60)));
	if (mTimeLeft.count() > 30)
	{
		if (oldTimerValue <= 30)
		{
			setProperty("color", "black");
			Repolish();
		}
	}
	else if (mTimeLeft.count() > 10)
	{
		if (oldTimerValue <= 10 || oldTimerValue > 30)
		{
			setProperty("color", "blue");
			Repolish();
		}
	}
	else
	{
		if (oldTimerValue > 10)
		{
			setProperty("color", "red");
			Repolish();
		}
	}
}

void Timer::Repolish()
{
	style()->unpolish(this);
	style()->polish(this);
	update();
}
