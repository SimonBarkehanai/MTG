#pragma once
#include <chrono>  // for seconds

#include <QLabel>   // for QLabel
#include <QObject>  // for QObject
#include <QSize>    // for QSize
#include <QString>  // for QString
#include <QTimer>   // for QTimer
#include <QWidget>  // for QWidget
#include <QtCore>   // for WindowFlags, Q_OBJECT, signals, slots

struct AutoStart_t
{
	struct AutoStart_t_TAG
	{
	};
	constexpr AutoStart_t(AutoStart_t_TAG)
	{
	}
};
inline constexpr AutoStart_t AutoStart{ AutoStart_t::AutoStart_t_TAG{} };

class Timer : public QLabel
{
	Q_OBJECT
public:
	// doesn't start the countdown
	Timer(std::chrono::seconds InitialTime,
		QWidget*               parent = nullptr,
		Qt::WindowFlags        f      = {});
	// starts with an initial time of 0
	Timer(QWidget* parent = nullptr, Qt::WindowFlags f = {});
	// same but starts counting down on construction
	Timer(AutoStart_t,
		std::chrono::seconds InitialTime = std::chrono::seconds(0),
		QWidget*             parent      = nullptr,
		Qt::WindowFlags      f           = {});

	auto TimeLeft() const
	{
		return mTimeLeft;
	}

public slots:
	void Start()
	{
		mTimer->start();
	}
	void Start(std::chrono::seconds dur);
	void Stop()
	{
		mTimer->stop();
	}
	// doesn't start
	void SetTime(std::chrono::seconds dur);

signals:
	void TimeUp();

private:
	void Draw(int oldTimerValue);
	void Repolish();

	std::chrono::seconds   mTimeLeft;
	QTimer*                mTimer = new QTimer(this);
	static constexpr QSize mSize  = { 70, 40 };
};
