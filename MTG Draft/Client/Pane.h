#pragma once
#include <cstddef>  // for size_t
#include <vector>   // for vector

#include <QLabel>        // for QLabel
#include <QObject>       // for QObject
#include <QPaintEvent>   // for QPaintEvent
#include <QResizeEvent>  // for QResizeEvent
#include <QString>       // for QString
#include <QWidget>       // for QWidget
#include <QtCore>        // for Q_OBJECT, WindowFlags

enum SortType
{
	Color,
	CMC
};

class Pane : public QWidget
{
	Q_OBJECT
public:
	Pane(const QString&             mainTitle = "",
		const std::vector<QString>& otherText = {},
		QWidget*                    central   = nullptr,
		QWidget*                    parent    = nullptr,
		Qt::WindowFlags             f         = {});

	QWidget* CentralWidget()
	{
		return mCentral;
	}
	void setCentralWidget(QWidget* newCentral);

	// @brief sets the title to newTitle
	void setTitle(const QString& newTitle);
	// @brief adds the text at the end of the current text, where it might not
	// be visible.
	void addNewOtherText(const QString& newText);
	// @brief sets the text at index to newText.
	// If index >= number of text items, behavior is undefined
	void setOtherText(std::size_t index, const QString& newText);

protected:
	void resizeEvent(QResizeEvent* e) override;
	void paintEvent(QPaintEvent* e) override;

private:
	// @return the total width of the labels
	int DrawLabels();

	static constexpr auto TopHeight = 24, Border = 1,
						  TopContentHeight = TopHeight - 2 * Border,
						  TextPadding = 5, Padding = 10, SortWidth = 60,
						  ZoomWidth = 160, CentralMinHeight = 100,
						  TitlePointSize = 10;

	QWidget*             mTopBar = new QWidget(this);
	QWidget*             mCentral;
	QLabel*              mTitle = new QLabel(mTopBar);
	std::vector<QLabel*> mOtherLabels;
};
