#define CATCH_CONFIG_ENABLE_CHRONO_STRINGMAKER
#include <chrono>  // for duration, operator""s, operator==
#include <random>  // for mt19937, random_device, uniform_int
#include <ratio>   // for ratio

#include <catch.hpp>  // for operator""_catch_sr, AssertionHandler

#include <QImage>   // for QImage
#include <QPixmap>  // for QPixmap
#include <QRect>    // for QRect
#include <QSize>    // for QSize, operator==
#include <QString>  // for QString
#include <QTest>    // for qWait
#include <QWidget>  // for QWidget
#include <QtGui>    // for qRgb

#include "Timer.h"  // for Timer, AutoStart

using namespace std::literals;

TEST_CASE("Timer Construction", "[timer]")
{
	Timer t1;
	CHECK(t1.TimeLeft() == 0s);

	Timer t2(5s);
	CHECK(t2.TimeLeft() == 5s);

	Timer t3(5min);
	CHECK(t3.TimeLeft() == 5min);

	QWidget w;
	Timer*  t4 = new Timer(&w);
	CHECK(t4->TimeLeft() == 0s);

	Timer* t5 = new Timer(1'000'000s, &w);
	CHECK(t5->TimeLeft() == 1'000'000s);

	Timer t6(AutoStart, std::chrono::minutes(5));
	QTest::qWait(std::chrono::milliseconds(1s + 500ms).count());
	CHECK(t6.TimeLeft() == 5min - 1s);
}

TEST_CASE("Timer Resize", "[timer]")
{
	Timer                         t;
	const auto                    sz = t.size();
	std::mt19937                  eng(std::random_device{}());
	std::uniform_int_distribution dist(10, 1'000);
	const auto                    x = dist(eng), y = dist(eng);

	INFO("Trying to resize to { " << x << ", " << y << " }");
	t.resize(x, y);
	CHECK(t.size() == sz);
}

TEST_CASE("Timer Text", "[timer]")
{
	Timer t(std::chrono::minutes(2));
	CHECK(t.text() == "02:00");

	t.Start();
	QTest::qWait(std::chrono::milliseconds(1s + 500ms).count());
	CHECK(t.text() == "01:59");
}

TEST_CASE("Timer Color", "[timer]")
{
	Timer t(std::chrono::minutes(2));
	CHECK(t.grab(QRect(1, 1, 1, 1)).toImage().pixel(0, 0)
		  == qRgb(0x00, 0x00, 0x00));

	t.SetTime(std::chrono::seconds(20));
	CHECK(t.grab(QRect(1, 1, 1, 1)).toImage().pixel(0, 0)
		  == qRgb(0x00, 0xb0, 0xe0));

	t.SetTime(std::chrono::seconds(3));
	CHECK(t.grab(QRect(1, 1, 1, 1)).toImage().pixel(0, 0)
		  == qRgb(0xe0, 0x00, 0x00));
}
