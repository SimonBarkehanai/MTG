#define CATCH_CONFIG_RUNNER
#include <catch.hpp>  // for Session

#include <QApplication>  // for QApplication

int main(int argc, char* argv[])
{
	QApplication app(argc, argv);

	return Catch::Session().run(argc, argv);
}
