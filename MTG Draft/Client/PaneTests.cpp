#include <random>  // for mt19937, random_device, uniform_int, uni...

#include <catch.hpp>  // for operator""_catch_sr, AssertionHandler

#include <QCoreApplication>  // for QCoreApplication
#include <QImage>            // for QImage
#include <QLabel>            // for QLabel
#include <QPixmap>           // for QPixmap
#include <QPoint>            // for operator+, QPoint
#include <QRect>             // for QRect, operator==
#include <QResizeEvent>      // for QResizeEvent
#include <QSize>             // for QSize, operator==
#include <QString>           // for QString
#include <QWidget>           // for QWidget
#include <QtCore>            // for Window, WindowType
#include <QtGui>             // for qRgb

#include "Pane.h"  // for Pane

TEST_CASE("Pane Construction", "[pane]")
{
	CHECK_NOTHROW([]() {
		Pane p1("");
	}());

	CHECK_NOTHROW([]() {
		Pane p2(
			"Main Deck: 40 cards", { "Lands: 17", "Creature: 18", "Other: 5" });
	}());

	CHECK_NOTHROW([]() {
		auto c = new QWidget;
		Pane p3(
			"Main Deck: 40 cards", { "Lands: 17", "Creature: 18", "Other: 5" }, c);
	}());

	CHECK_NOTHROW([]() {
		auto c = new QWidget, w = new QWidget;
		Pane p4("Main Deck: 40 cards",
			{ "Lands: 17", "Creature: 18", "Other: 5" },
			c,
			w);
	}());

	CHECK_NOTHROW([]() {
		Pane p5("Main Deck: 40 cards",
			{ "Lands: 17", "Creature: 18", "Other: 5" },
			nullptr,
			nullptr,
			Qt::WindowType::Window);
	}());
}

TEST_CASE("Pane Resize", "[pane]")
{
	Pane p("Main Deck: 40 cards",
		{ "Lands: 17", "Creature: 18", "Other: 5" },
		new QWidget);

	std::mt19937                  eng(std::random_device{}());
	std::uniform_int_distribution width(600, 1000);
	std::uniform_int_distribution height(200, 1000);

	auto w = width(eng), h = height(eng);

	INFO("Resizing to { " << w << ", " << h << " }");

	auto oldSize = p.size();
	p.resize(w, h);
	QResizeEvent e(QSize(w, h), oldSize);
	QCoreApplication::sendEvent(&p, &e);

	CHECK(p.CentralWidget()->size() == QSize(w - 2, h - 26));
	CHECK(p.childAt(2, 2)->geometry() == QRect(1, 1, w - 2, 24));

	CHECK(p.grab(QRect(2, 2, 1, 1)).toImage().pixel(0, 0)
		  == qRgb(0x00, 0x00, 0x00));
	auto pixmap =
		p.grab(QRect(0, 0, -1, -1)).toImage().save(R"(C:\Users\simon\Desktop\img.jpg)");
	CHECK(p.grab(QRect(0, 30, 3, 3)).toImage().pixel(0, 0)
		  == qRgb(0x70, 0x70, 0x70));
	CHECK(p.grab(QRect(w - 1, 5, 1, 1)).toImage().pixel(0, 0)
		  == qRgb(0x70, 0x70, 0x70));
	auto borderColor = p.grab(QRect(25, h - 1, 1, 1)).toImage().pixel(0, 0);
	CHECK((borderColor == qRgb(0x70, 0x70, 0x70)
		   || borderColor == qRgb(0x71, 0x71, 0x71)));
	CHECK(p.grab(QRect(5, 40, 1, 1)).toImage().pixel(0, 0)
		  == qRgb(0xff, 0xff, 0xff));
}

TEST_CASE("Pane Text", "[pane]")
{
	Pane  p("Main Deck: 40 cards", { "Lands: 17", "Creature: 18", "Other: 5" });
	auto  oldSize = p.size();
	QSize newSize(2'000, 2'000);
	p.resize(newSize);
	QResizeEvent e(newSize, oldSize);
	QCoreApplication::sendEvent(&p, &e);

	auto Title = static_cast<QLabel*>(p.childAt(20, 10));
	p.setTitle("Title");
	CHECK(Title->text() == "Title");

	p.addNewOtherText("Other Text");
	auto newLabel = static_cast<QLabel*>(p.childAt(215, 10));
	CHECK(newLabel->text() == "Other Text");

	auto oldX = newLabel->x();

	p.setOtherText(1, "long string to push all others right");
	CHECK(oldX + 75 < newLabel->x());
}

TEST_CASE("Pane Central Widget", "[pane]")
{
	Pane p("Main Deck: 40 cards",
		{ "Lands: 17", "Creature: 18", "Other: 5" },
		new QWidget);
	p.resize(300, 300);
	auto central = p.CentralWidget();
	REQUIRE(central != nullptr);

	auto box1 = new QWidget(central);
	box1->setGeometry(10, 10, 50, 50);
	box1->setStyleSheet("background-color:#ff0000;");

	auto box2 = new QWidget(central);
	box2->setGeometry(50, 50, 50, 50);
	box2->setStyleSheet("background-color:#00ff00;");

	auto box3 = new QWidget(central);
	box3->setGeometry(90, 90, 50, 50);
	box3->setStyleSheet("background-color:#0000ff;");

	CHECK(p.childAt(box1->pos() + central->pos()) == box1);
	CHECK(p.childAt(box2->pos() + central->pos()) == box2);
	CHECK(p.childAt(box3->pos() + central->pos()) == box3);

	CHECK(p.grab(QRect(box1->pos() + central->pos() + QPoint(10, 10), QSize(1, 1)))
			  .toImage()
			  .pixel(0, 0)
		  == qRgb(0xff, 0, 0));
	CHECK(p.grab(QRect(box2->pos() + central->pos() + QPoint(10, 10), QSize(1, 1)))
			  .toImage()
			  .pixel(0, 0)
		  == qRgb(0, 0xff, 0));
	CHECK(p.grab(QRect(box3->pos() + central->pos() + QPoint(10, 10), QSize(1, 1)))
			  .toImage()
			  .pixel(0, 0)
		  == qRgb(0, 0, 0xff));
}
