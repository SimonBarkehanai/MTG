#include "Pane.h"

#include <QFont>         // for QFont
#include <QFontMetrics>  // for QFontMetrics
#include <QPainter>      // for QPainter
#include <QSize>         // for QSize
#include <QStyle>        // for QStyle, QStyle::PE_Widget
#include <QStyleOption>  // for QStyleOption
#include <QtCore>        // for WindowFlags

Pane::Pane(const QString&       mainTitle,
	const std::vector<QString>& otherText,
	QWidget*                    central,
	QWidget*                    parent,
	Qt::WindowFlags             f)
	: QWidget(parent, f), mCentral(central)
{
	for (const auto& str : otherText)
	{
		auto toAdd = new QLabel(str, mTopBar);
		toAdd->setObjectName("__PANE_Other");
		mOtherLabels.push_back(toAdd);
	}

	setStyleSheet(
		{ R"(.Pane{background-color:white;border:1px solid #707070;}.QWidget#__PANE_Top{background-color:black;}QLabel#__PANE_Title{color:#2596c0;}QLabel#__PANE_Other{color:white;})" });

	mTopBar->move(Border, Border);
	mTopBar->setObjectName("__PANE_Top");

	auto TitleFont = mTitle->font();
	TitleFont.setBold(true);
	TitleFont.setPointSize(10);
	mTitle->setFont(TitleFont);
	mTitle->setText(mainTitle);
	mTitle->setObjectName("__PANE_Title");
	mTitle->move(Padding, 0);
	auto LabelWidth = DrawLabels();

	if (mCentral != nullptr)
	{
		mCentral->setParent(this);
		mCentral->move(Border, TopHeight + Border);
	}

	const QSize minSz{ 2 * Border + 2 * Padding + LabelWidth,
		TopHeight + 2 * Border + CentralMinHeight };
	setMinimumSize(minSz);
	resize(minSz);
}

void Pane::setCentralWidget(QWidget* newCentral)
{
	mCentral->deleteLater();
	mCentral = newCentral;
	mCentral->setParent(this);
	if (mCentral != nullptr)
		mCentral->setGeometry(Border,
			TopHeight + Border,
			width() - 2 * Border,
			height() - TopHeight - 2 * Border);
}

void Pane::setTitle(const QString& newTitle)
{
	mTitle->setText(newTitle);
	DrawLabels();
}

void Pane::addNewOtherText(const QString& newText)
{
	mOtherLabels.push_back(new QLabel(newText, mTopBar));
	DrawLabels();
}

void Pane::setOtherText(std::size_t index, const QString& newText)
{
	mOtherLabels[index]->setText(newText);
	DrawLabels();
}

void Pane::resizeEvent(QResizeEvent* e)
{
	const auto w = width(), h = height();
	mTopBar->resize(w - 2 * Border, TopHeight);
	if (mCentral != nullptr)
		mCentral->resize(w - 2 * Border, h - TopHeight - 2 * Border);
}

void Pane::paintEvent(QPaintEvent* e)
{
	QWidget::paintEvent(e);

	QStyleOption o;
	o.initFrom(this);
	QPainter p(this);
	style()->drawPrimitive(QStyle::PE_Widget, &o, &p, this);
}

int Pane::DrawLabels()
{
	const static QFontMetrics TitleMetrics(mTitle->font()), otherMetrics(QFont{});

	// title
	auto advance = TitleMetrics.horizontalAdvance(mTitle->text());
	mTitle->setGeometry(Padding, 0, advance, TopHeight);
	int LastRight = Padding + advance + TextPadding;  // add extra padding to
	                                                  // the right of the title

	for (auto label : mOtherLabels)
	{
		const auto width = otherMetrics.horizontalAdvance(label->text());
		label->setGeometry(LastRight + TextPadding, 0, width, TopHeight);
		LastRight += TextPadding + width;
	}

	return LastRight - Padding;
}
