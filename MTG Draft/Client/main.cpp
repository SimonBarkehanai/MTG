#include <chrono>  // for minutes

#include <QApplication>  // for QApplication
#include <QSize>         // for operator-, QSize
#include <QString>       // for QString
#include <QWidget>       // for QWidget

#include "Pane.h"   // for Pane
#include "Timer.h"  // for Timer

int main(int argc, char* argv[])
{
	QApplication app(argc, argv);

	QWidget w;
	Pane*   p = new Pane("Main Deck", { "other text", "vector" }, nullptr, &w);
	w.resize(1000, 600);
	p->setGeometry(10, 10, 900, 500);
	p->setCentralWidget(new QWidget);
	auto timer = new Timer(p->CentralWidget());
	auto sz    = p->CentralWidget()->size() - QSize{ 10, 10 } - timer->size();
	timer->move(sz.width(), sz.height());
	timer->Start(std::chrono::minutes(2));
	w.show();

	return app.exec();
}
