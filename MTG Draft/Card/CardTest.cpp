#include "Card.h"

#include <functional>  // for hash
#include <string>      // for basic_string, operator==, char_traits

#include <catch.hpp>          // for AssertionHandler, operator""_catch_sr
#include <nlohmann/json.hpp>  // for basic_json, operator==, json_ref, opera...
#include <stdint.h>           // for uint16_t

#include "ManaCost.h"  // for ManaCost
// IWYU thinks I compare map iterators, but we don't need that header
// IWYU pragma: no_include <map>


constexpr uint16_t   colNum = 1;
const std::string    id     = "2398892d-28e9-4009-81ec-0d544af79d2b";
constexpr ManaCost   c      = { 0, 0, 1, 0, 0, 0, 0 };
const nlohmann::json json{
	{ "num", colNum },
	{ "id", id },
	{ "cost", c },
};

TEST_CASE("Card Construction", "[card]")
{
	Card c1(colNum, id, c);
	REQUIRE(c1.CollectorNumber == colNum);
	REQUIRE(c1.ScryfallID == id);
	REQUIRE(c1.Cost == c);

	Card c2(json);
	REQUIRE(c1 == c2);

	Card c3(c1);
	REQUIRE(c1 == c3);
}

TEST_CASE("Card Json conversion", "[card]")
{
	Card c1(colNum, id, c);
	REQUIRE(static_cast<nlohmann::json>(c1) == json);

	Card c2(json);
	REQUIRE(static_cast<nlohmann::json>(c2) == json);
}

TEST_CASE("Card hash", "[card]")
{
	Card c1(colNum + 1, id, c);

	Card c2(json);

	std::hash<Card> hash;

	REQUIRE(hash(c1) != hash(c2));
}
