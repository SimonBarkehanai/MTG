#pragma once
#include <cstddef>      // for size_t
#include <cstdint>      // for uint16_t
#include <functional>   // for hash
#include <string>       // for string
#include <string_view>  // for string_view
#include <type_traits>  // for hash, _Conditionally_enabled...

#include <nlohmann/json.hpp>  // for basic_json<>::object_t, basi...

#include "ManaCost.h"  // for ManaCost, hash

struct Card
{
	uint16_t    CollectorNumber;
	std::string ScryfallID;
	ManaCost    Cost;

	Card(uint16_t ColNum, std::string id, ManaCost c)
		: CollectorNumber(ColNum), ScryfallID(std::move(id)), Cost(c)
	{
	}
	explicit Card(const nlohmann::json& src)
		: CollectorNumber(src["num"].get<uint16_t>())
		, ScryfallID(src["id"].get<std::string>())
		, Cost(src["cost"].get<std::string_view>())
	{
	}

	operator nlohmann::json() const
	{
		return {
			{ "num", CollectorNumber },
			{ "id", ScryfallID },
			{ "cost", static_cast<std::string>(Cost) },
		};
	}

	bool operator==(const Card& rhs) const = default;
};

template <>
struct std::hash<Card>
{
public:
	size_t operator()(const Card& c) const
	{
		return 43 * std::hash<uint16_t>{}(c.CollectorNumber)
		       + 37 * std::hash<std::string>{}(c.ScryfallID)
		       + 31 * std::hash<ManaCost>{}(c.Cost);
	}
};
