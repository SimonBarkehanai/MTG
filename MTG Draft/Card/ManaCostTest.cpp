#include "ManaCost.h"  // for operator|, ManaCost, Color, Color::Blue, Colo...

#include <cstdint>      // for int8_t
#include <exception>    // for exception
#include <functional>   // for hash
#include <string>       // for string, basic_string, char_traits, operator==
#include <string_view>  // for string_view, basic_string_view

#include <catch.hpp>  // for operator""_catch_sr, AssertionHandler, Source...

TEST_CASE("Color Bitwise OR Works", "[color]")
{
	CHECK((Color::Colorless | Color::Red) == Underlying(Color::Red));
	CHECK((Color::Blue | Color::Red) == 0xa);
	CHECK((Color::White | Color::Blue | Color::Black | Color::Red | Color::Green)
		  == 0x1f);
}

constexpr int8_t           gen = 1, w = 2, u = 3, b = 4, r = 5, g = 6, c = 7;
constexpr char             s[8]{ gen, w, u, b, r, g, c, gen };
constexpr std::string_view str(s, 7);

class CatchExceptionExtractor
	: public Catch::Matchers::Exception::ExceptionMessageMatcher
{
public:
	CatchExceptionExtractor(const std::string& s = "")
		: ExceptionMessageMatcher(s)
	{
	}
	bool match(std::exception const& ex) const override
	{
		mWhat = ex.what();
		return true;
	}
	std::string what() const
	{
		return mWhat;
	}

private:
	mutable std::string mWhat;
};

TEST_CASE("Mana Cost Construction", "[manacost]")
{
	ManaCost c1;

	ManaCost c2(gen, w, u, b, r, g, c);
	CHECK(c2.Generic == gen);
	CHECK(c2.White == w);
	CHECK(c2.Blue == u);
	CHECK(c2.Black == b);
	CHECK(c2.Red == r);
	CHECK(c2.Green == g);
	CHECK(c2.Colorless == c);

	ManaCost c3(str);
	CHECK(c3.Generic == gen);
	CHECK(c3.White == w);
	CHECK(c3.Blue == u);
	CHECK(c3.Black == b);
	CHECK(c3.Red == r);
	CHECK(c3.Green == g);
	CHECK(c3.Colorless == c);

#ifndef NDEBUG  // exceptions from
	ManaCost                c4{ std::string(str) };
	CatchExceptionExtractor e;
	CHECK_THROWS_MATCHES(
		[]() {
			ManaCost c5(str.data());
		}(),
		assertion_violation,
		e);
	CHECK_THAT(e.what(),
		Catch::Matches(R"(^assertion violation ".+" at .+ManaCost\.h:\d+$)"));
#else
	CHECK_NOTHROW([]() {
		ManaCost c5(str.data());
	}());
#endif
}

TEST_CASE("Mana Cost to string", "[manacost]")
{
	ManaCost c1(gen, w, u, b, r, g, c);
	CHECK(static_cast<std::string>(c1) == str);
	ManaCost c2(str);
	CHECK(c1 == c2);
}

TEST_CASE("Mana Cost member functions", "[manacost]")
{
	ManaCost c1(gen, w, u, b, 0, 0, 0);
	CHECK(c1.CMC() == gen + w + u + b);
	CHECK(c1.Colors() == (Color::White | Color::Blue | Color::Black));
}

TEST_CASE("Mana Cost constexpr stuff", "[manacost]")
{
	constexpr ManaCost c1;

	constexpr ManaCost c2(gen, w, u, b, 0, 0, 0);
	constexpr auto     cmc    = c2.CMC();
	constexpr auto     colors = c2.Colors();
	CHECK(cmc == gen + w + u + b);
	CHECK(colors == (Color::White | Color::Blue | Color::Black));

	constexpr ManaCost c3(c2);
	STATIC_REQUIRE(c2 == c3);

	constexpr ManaCost c4(str);
}

TEST_CASE("Mana Cost hash", "[manacost]")
{
	constexpr ManaCost c1(str);

	constexpr ManaCost c2(gen, w, u, b, 0, 0, 0);

	std::hash<ManaCost> hash;

	CHECK(hash(c1) != hash(c2));
}
