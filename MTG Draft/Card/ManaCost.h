#pragma once
#include <cstddef>      // for size_t
#include <cstdint>      // for uint64_t, uint8_t
#include <functional>   // for hash
#include <stdexcept>    // for runtime_error
#include <string>       // for string
#include <string_view>  // for basic_string_view, string_view
#include <type_traits>  // for underlying_type_t

#ifdef NDEBUG
#	define throwing_assert(expression) ((void)0)
#else
class assertion_violation : public std::runtime_error
{
public:
	explicit assertion_violation(std::string_view msg)
		: std::runtime_error(msg.data())
	{
	}
	explicit assertion_violation(const char* msg) : std::runtime_error(msg)
	{
	}
};
[[noreturn]] inline bool throw_assertion_violation(const char* msg)
{
	throw assertion_violation(msg);
}
#	define STRINGIZE_DETAIL(x) #    x
#	define STRINGIZE(x)        STRINGIZE_DETAIL(x)
#	define throwing_assert(expression)                                      \
		(void)((!!(expression))                                              \
			   || (throw_assertion_violation("assertion violation "          \
		                                     "\"" #expression "\" "          \
		                                                      "at " __FILE__ \
											 ":" STRINGIZE(__LINE__))))
#endif

template <typename T>
concept Enumeration = std::is_enum_v<T>;

template <Enumeration E>
using underlying_t = std::underlying_type_t<E>;
template <Enumeration E>
constexpr auto Underlying(E val)
{
	return static_cast<underlying_t<E>>(val);
}

enum class Color : unsigned
{
	Colorless = 0x00,
	White     = 0x01,
	Blue      = 0x02,
	Black     = 0x04,
	Red       = 0x08,
	Green     = 0x10,
	C         = Colorless,
	W         = White,
	U         = Blue,
	B         = Black,
	R         = Red,
	G         = Green,
};

constexpr auto operator|(Color lhs, Color rhs)
{
	using underlying = underlying_t<Color>;
	return static_cast<underlying>(lhs) | static_cast<underlying>(rhs);
}

template <typename T>
constexpr auto operator|(T lhs, Color rhs)
{
	using underlying = underlying_t<Color>;
	return lhs | static_cast<underlying>(rhs);
}

template <typename T>
constexpr auto operator|(Color lhs, T rhs)
{
	using underlying = underlying_t<Color>;
	return static_cast<underlying>(lhs) | rhs;
}

struct ManaCost
{
	uint8_t Generic, White, Blue, Black, Red, Green, Colorless;

	constexpr ManaCost()
		: Generic(0), White(0), Blue(0), Black(0), Red(0), Green(0), Colorless(0)
	{
	}

	constexpr ManaCost(
		uint8_t gen, uint8_t w, uint8_t u, uint8_t b, uint8_t r, uint8_t g, uint8_t c)
		: Generic(gen), White(w), Blue(u), Black(b), Red(r), Green(g), Colorless(c)
	{
	}

	explicit constexpr ManaCost(std::string_view src)
		: Generic(0), White(0), Blue(0), Black(0), Red(0), Green(0), Colorless(0)
	{
		throwing_assert(src.length() == 7);
		Generic   = src[0];
		White     = src[1];
		Blue      = src[2];
		Black     = src[3];
		Red       = src[4];
		Green     = src[5];
		Colorless = src[6];
	}
	explicit operator std::string() const
	{
		return std::string(reinterpret_cast<const char* const>(this), 7);
	}
	constexpr bool operator==(ManaCost const& rhs) const = default;

	constexpr uint8_t CMC() const
	{
		return Generic + White + Blue + Black + Red + Green + Colorless;
	}
	constexpr auto Colors() const
	{
		return (White != 0 ? Color::White : Color::Colorless)
		       | (Blue != 0 ? Color::Blue : Color::Colorless)
		       | (Black != 0 ? Color::Black : Color::Colorless)
		       | (Red != 0 ? Color::Red : Color::Colorless)
		       | (Green != 0 ? Color::Green : Color::Colorless);
	}
};

template <>
class std::hash<ManaCost>
{
public:
	constexpr size_t operator()(ManaCost c) const
	{
		if constexpr (sizeof(size_t) == sizeof(uint64_t))
		{
			return (static_cast<uint64_t>(c.Generic))
			       + (static_cast<uint64_t>(c.White) << 8)
			       + (static_cast<uint64_t>(c.Blue) << 16)
			       + (static_cast<uint64_t>(c.Black) << 24)
			       + (static_cast<uint64_t>(c.Red) << 32)
			       + (static_cast<uint64_t>(c.Green) << 40)
			       + (static_cast<uint64_t>(c.Colorless) << 48);
		}
		else
		{
			return (static_cast<uint64_t>(c.Generic))
			       + (static_cast<uint64_t>(c.White) << 4)
			       + (static_cast<uint64_t>(c.Blue) << 8)
			       + (static_cast<uint64_t>(c.Black) << 12)
			       + (static_cast<uint64_t>(c.Red) << 16)
			       + (static_cast<uint64_t>(c.Green) << 20)
			       + (static_cast<uint64_t>(c.Colorless) << 24);
		}
	}
};
