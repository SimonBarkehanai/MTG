#include <array>

#include "launcher.h"

#ifndef NDEBUG
int debug_main(int argc, char* argv[])
{
	std::vector<char const*> args{ argv, argv + argc };
	args[0] = R"(D:\Program Files\MTG\Forge\ForgeLauncher.exe)";
	launcher l(args.size(), args.data());
	return l.run();
}
#endif

int main(int argc, char* argv[])
{
#ifdef NDEBUG
	launcher l(argc, argv);
	return l.run();
#else
	return debug_main(argc, argv);
#endif
}
