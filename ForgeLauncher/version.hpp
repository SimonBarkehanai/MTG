#pragma once
#include <charconv>
#include <ostream>
#include <string_view>

constexpr std::uint8_t digit_from_byte[] = { 255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	0,
	1,
	2,
	3,
	4,
	5,
	6,
	7,
	8,
	9,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255,
	255 };

[[nodiscard]] inline constexpr std::uint8_t digit_from_char(const char ch) noexcept
{
	static_assert(
		std::size(digit_from_byte)
		== static_cast<std::size_t>(std::numeric_limits<std::uint8_t>::max()) + 1);

	return digit_from_byte[static_cast<unsigned char>(ch)];
}

template <std::unsigned_integral T>
constexpr void from_chars(
	const char* const first, const char* const last, T& val) noexcept
{
	constexpr T uint_max  = static_cast<T>(-1);
	constexpr T risky_val = static_cast<T>(uint_max / 10);
	constexpr T max_digit = static_cast<T>(uint_max % 10);
	T           value{};

	for (const char* next = first; next != last; ++next)
	{
		const unsigned char digit = digit_from_char(*next);

		if (digit < 10
			&& (value < risky_val  // never overflows
				|| (value == risky_val && digit <= max_digit)))  // overflows
		                                                         // for certain
		                                                         // digits
			value = static_cast<T>(value * 10 + digit);
		else
			return;
	}
	val = value;
}

template <std::unsigned_integral T>
constexpr T from_chars(const char* const first, const char* const last) noexcept
{
	T value;
	from_chars(first, last, value);
	return value;
}

template <std::unsigned_integral T>
constexpr void from_chars(std::string_view const str, T& value) noexcept
{
	from_chars(str.data(), str.data() + str.size(), value);
}

template <std::unsigned_integral T>
constexpr T from_chars(std::string_view const str) noexcept
{
	return from_chars<T>(str.data(), str.data() + str.size());
}

struct version
{
	constexpr version(uint16_t maj = 0, uint16_t min = 0, uint16_t patch = 0)
		: major(maj), minor(min), patch(patch)
	{
	}

	version(std::string_view src)
	{
		auto const pos1 = src.find('.'), pos2 = src.find('.', pos1 + 1);

		from_chars(src.substr(0, pos1), major);
		from_chars(src.substr(pos1 + 1, pos2 - pos1 - 1), minor);
		from_chars(src.substr(pos2 + 1), patch);
	}

	explicit operator std::string() const
	{
		constexpr auto digitsPerNum =
			std::numeric_limits<decltype(major)>::digits10 + 1;
		char buf[digitsPerNum * 3 + 2]{};
		auto next = std::to_chars(buf, buf + sizeof(buf), major).ptr;
		*next     = '.';
		++next;
		next  = std::to_chars(next, buf + sizeof(buf), minor).ptr;
		*next = '.';
		++next;
		next = std::to_chars(next, buf + sizeof(buf), patch).ptr;
		return { buf, next };
	}

	constexpr bool operator==(const version& rhs) const = default;
	constexpr std::strong_ordering operator<=>(const version& rhs) const = default;

	template <typename CharT>
	friend std::basic_ostream<CharT>& operator<<(
		std::basic_ostream<CharT>& os, version ver);

private:
	std::uint16_t major, minor, patch;
};

template <typename CharT>
std::basic_ostream<CharT>& operator<<(std::basic_ostream<CharT>& os, version ver)
{
	return os << ver.major << '.' << ver.minor << '.' << ver.patch;
}
