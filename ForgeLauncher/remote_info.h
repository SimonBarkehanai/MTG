#pragma once
#include <chrono>
#include <filesystem>

#include "version.hpp"

struct last_changed_t
{
	uint16_t       year;
	uint8_t        month, day;
	uint8_t        hour, minute;
	constexpr bool operator==(last_changed_t const& other) const = default;
	static last_changed_t from_bytes(std::string_view bytes);
	std::string           to_bytes() const;
};

class remote_info
{
public:
	remote_info(bool is_snapshot, std::string_view exec);
	~remote_info();
	version        newest_version() const;
	last_changed_t last_changed() const;

	void download_latest(
		std::string_view dest =
			(std::filesystem::temp_directory_path() / "forge.tar.bz2").string());

private:
	class impl;
	std::unique_ptr<impl> m_impl;
};
