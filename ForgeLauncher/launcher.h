#pragma once
#include <optional>
#include <string_view>

#include "local_info.h"

struct options;

enum class arg_type : uint8_t
{
	none       = 0x00,
	help       = 0x01,
	version    = 0x02,
	update     = 0x04,
	launch     = 0x08,
	snapshot   = 0x10,
	assume_yes = 0x20,
	download   = 0x40,
};

class launcher
{
public:
	launcher(int argc, char const* const* argv);
	~launcher();

	int run();

private:
	std::unique_ptr<options> m_options;

	std::string_view const m_exec;
	local_info const       m_local;
	std::string_view const m_forge_dir;

	int  launch();
	void update();
	void help();
	void ver();
	void dl();

	std::optional<int> handle_arg(arg_type type);
};

extern template std::unique_ptr<options>;
