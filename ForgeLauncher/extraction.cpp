#include "extraction.h"

#include <array>
#include <filesystem>
#include <fstream>
#include <iostream>

#include <boost/iostreams/filter/bzip2.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/predef.h>

namespace fs = std::filesystem;

constexpr std::string_view tmagic = { "ustar" };
constexpr std::string_view tver   = { "00" };

enum class type_flag_e : char
{
	regtype  = '0',
	aregtype = '\0',
	lnktype  = '1',
	symtype  = '2',
	chrtype  = '3',
	blktype  = '4',
	dirtype  = '5',
	fifotype = '6',
	conttype = '7',
	xhdtype  = 'x',
	xgltype  = 'g',
	longname = 'L'
};

enum class mode_e : std::uint_fast16_t
{
	empty = 0,
	tsuid = 04000,    // set UID on execution
	tsgid = 02000,    // set GID on execution
	tsvtx = 01000,    // reserved
	                  // file permissions
	turead  = 00400,  // read by owner
	tuwrite = 00200,  // write by owner
	tuexec  = 00100,  // execute/search by owner
	tgread  = 00040,  // read by group
	tgwrite = 00020,  // write by group
	tgexec  = 00010,  // execute/search by group
	toread  = 00004,  // read by other
	towrite = 00002,  // write by other
	toexec  = 00001,  // execute/search by other
};

constexpr auto& operator|=(mode_e& lhs, mode_e rhs)
{
	return lhs = static_cast<mode_e>(
			   std::to_underlying(lhs) | std::to_underlying(rhs));
}

constexpr auto operator&(mode_e lhs, mode_e rhs)
{
	return static_cast<mode_e>(std::to_underlying(lhs) & std::to_underlying(rhs));
}

constexpr bool operator!(mode_e m)
{
	return m == static_cast<mode_e>(0);
}

struct tar_header
{                                    // byte offset
	std::array<char, 100> name;      //   0
	std::array<char, 8>   mode;      // 100
	std::array<char, 8>   uid;       // 108
	std::array<char, 8>   gid;       // 116
	std::array<char, 12>  size;      // 124
	std::array<char, 12>  mtime;     // 136
	std::array<char, 8>   chksum;    // 148
	type_flag_e           typeflag;  // 156
	std::array<char, 100> linkname;  // 157
	std::array<char, 6>   magic;     // 257
	std::array<char, 2>   version;   // 263
	std::array<char, 32>  uname;     // 265
	std::array<char, 32>  gname;     // 297
	std::array<char, 8>   devmajor;  // 329
	std::array<char, 8>   devminor;  // 337
	std::array<char, 155> prefix;    // 345
	std::array<char, 12>  padding;   // 500
};

template <std::integral T>
constexpr T octal_ascii_to(std::string_view ascii)
{
	T res = 0;
	for (auto const ch : ascii)
	{
		if (ch == ' ' || ch == '\0')
			continue;
		res *= 8;
		res += ch - '0';
	}
	return res;
}

fs::perms mode_to_perms(decltype(tar_header::mode) const& mode)
{
	constexpr std::array all_modes = {
		mode_e::tsuid,
		mode_e::tsgid,
		mode_e::tsvtx,
		mode_e::turead,
		mode_e::tuwrite,
		mode_e::tuexec,
		mode_e::tgread,
		mode_e::tgwrite,
		mode_e::tgexec,
		mode_e::toread,
		mode_e::towrite,
		mode_e::toexec,
	};
	constexpr std::array equiv_perms{
		fs::perms::set_uid,
		fs::perms::set_gid,
		fs::perms::none,
		fs::perms::owner_read,
		fs::perms::owner_write,
		fs::perms::owner_exec,
		fs::perms::group_read,
		fs::perms::group_write,
		fs::perms::group_exec,
		fs::perms::others_read,
		fs::perms::others_write,
		fs::perms::others_exec,
	};
	static_assert(all_modes.size() == equiv_perms.size());

	mode_e m = mode_e::empty;
	if (mode[0] != '0')
		m |= mode_e::tsuid;
	if (mode[1] != '0')
		m |= mode_e::tsgid;
	if (mode[2] != '0')
		m |= mode_e::tsvtx;
	m |= static_cast<mode_e>(
		static_cast<std::underlying_type_t<mode_e>>(mode[3] - '0') << 6);
	m |= static_cast<mode_e>(
		static_cast<std::underlying_type_t<mode_e>>(mode[4] - '0') << 3);
	m |= static_cast<mode_e>(
		static_cast<std::underlying_type_t<mode_e>>(mode[5] - '0') << 0);

	fs::perms res{};
	for (std::size_t i = 0; i < all_modes.size(); ++i)
	{
		if ((m & all_modes[i]) != mode_e::empty)
			res |= equiv_perms[i];
	}
	return res;
}

void extract_tar(fs::path const& where, std::istream& istr)
{
	constexpr std::size_t            tar_block_size = 512;
	std::array<char, tar_block_size> buf;
	std::string                      longname;
	while (istr.peek() != EOF)
	{
		istr.read(buf.data(), tar_block_size);
		auto header = std::bit_cast<tar_header>(buf);
		assert(std::memcmp(header.magic.data(), tmagic.data(), tmagic.size()) == 0);
		std::cout << std::format("\r\033[K Extracting {} ",
			std::basic_string_view{ header.name.data() });
		std::uint_fast16_t size = octal_ascii_to<std::uint64_t>(header.size);
		fs::path           file = where / header.name.data();
		if (!longname.empty())
		{
			file = where / longname;
			longname.clear();
		}
		if (header.typeflag == type_flag_e::dirtype)
		{
			fs::create_directories(file);
		}
		else if (header.typeflag == type_flag_e::longname)
		{
			while (size > tar_block_size)
			{
				istr.read(buf.data(), tar_block_size);
				longname.append(buf.data(), tar_block_size);
				size -= tar_block_size;
			}
			if (size != 0)
			{
				istr.read(buf.data(), tar_block_size);
				longname.append(buf.data(), size);
			}
			continue;
		}
		else
		{
			fs::create_directories(file.parent_path());
			std::ofstream ostr{ file, std::ios::out | std::ios::binary };
			while (size > tar_block_size)
			{
				istr.read(buf.data(), tar_block_size);
				ostr.write(buf.data(), tar_block_size);
				size -= tar_block_size;
			}
			if (size != 0)
			{
				istr.read(buf.data(), tar_block_size);
				ostr.write(buf.data(), size);
			}
			ostr.flush();
		}
		fs::permissions(file, mode_to_perms(header.mode));
		auto utc_time = std::chrono::utc_clock::time_point(
			std::chrono::seconds(octal_ascii_to<std::uint64_t>(header.mtime)));
		fs::last_write_time(file, fs::file_time_type::clock::from_utc(utc_time));
		while (istr.peek() == '\0')
			istr.ignore(tar_block_size);
	}
}

void extract_tar_bz2(
	std::filesystem::path const& from, std::filesystem::path const& to)
{
	assert(fs::is_regular_file(from));
	assert(!fs::exists(to) || fs::is_directory(to));
	assert(from.extension() == ".bz2");
	assert(from.stem().extension() == ".tar");

	fs::create_directories(to);

	std::ifstream file(from, std::ios::in | std::ios::binary);
	boost::iostreams::filtering_stream<boost::iostreams::input> in;
	in.push(boost::iostreams::bzip2_decompressor());
	in.push(file);
	extract_tar(to, in);
	std::cout << "\r\033[KFinished extracting\n";
}
