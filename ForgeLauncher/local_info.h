#pragma once
#include <charconv>
#include <compare>
#include <filesystem>
#include <string>
#include <string_view>

#include "Version.hpp"

namespace fs = std::filesystem;

class local_info
{
public:
	local_info() = default;
	local_info(std::string_view forge_dir);

	std::string_view file() const;
	version          ver() const;

private:
	local_info(std::pair<std::string_view, version> args);

	std::string const m_java_file;
	version const     m_ver;
};
