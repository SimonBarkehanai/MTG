#include "remote_info.h"

#include <fstream>
#include <iostream>

#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/beast.hpp>
#include <boost/beast/ssl.hpp>
#include <getter.h>

#include "version.hpp"

namespace net   = boost::asio;
namespace ip    = net::ip;
using tcp       = ip::tcp;
namespace beast = boost::beast;
namespace http  = beast::http;
namespace fs    = std::filesystem;

constexpr last_changed_t last_changed_from_str(std::string_view const str)
{
	return last_changed_t{
		.year   = from_chars<uint16_t>(str.substr(0, 4)),
		.month  = from_chars<uint8_t>(str.substr(4, 2)),
		.day    = from_chars<uint8_t>(str.substr(6, 2)),
		.hour   = from_chars<uint8_t>(str.substr(8, 2)),
		.minute = from_chars<uint8_t>(str.substr(10, 2)),
	};
}

class remote_info::impl
{
public:
	impl(bool is_snapshot, std::string_view exec)
		: m_is_snapshot{ is_snapshot }
		, getter_(host,
			  fs::absolute(exec)
				  .replace_filename("forge_cert.pem")
				  .lexically_normal()
				  .string())
	{
		populate_info();
	}

	version newest_version()
	{
		return m_newest_ver;
	}

	last_changed_t last_changed()
	{
		return m_last_changed;
	}

	void download_latest(std::string_view dest)
	{
		getter_.download_file(m_latest_target, dest, true);
	}

private:
	void populate_info()
	{
		if (m_is_snapshot)
		{
			auto const             html_str = getter_.get("/dailysnapshots/");
			std::string_view const html{ html_str };

			auto const file_start = html.find("forge-gui-desktop-"),
					   file_end   = html.find('"', file_start);
			auto time_start       = html.find("</a>", file_end) + 5;
			while (html[time_start] == ' ')
				++time_start;

			m_latest_target =
				"/dailysnapshots/"
				+ std::string{ html.substr(file_start, file_end - file_start) };

			auto const time_str  = html.substr(time_start, 17);
			auto const month_str = html.substr(time_start + 3, 3);
			uint8_t    month{};
			if (month_str == "Jan")
				month = 1;
			else if (month_str == "Feb")
				month = 2;
			else if (month_str == "Mar")
				month = 3;
			else if (month_str == "Apr")
				month = 4;
			else if (month_str == "May")
				month = 5;
			else if (month_str == "Jun")
				month = 6;
			else if (month_str == "Jul")
				month = 7;
			else if (month_str == "Aug")
				month = 8;
			else if (month_str == "Sep")
				month = 9;
			else if (month_str == "Oct")
				month = 10;
			else if (month_str == "Nov")
				month = 11;
			else if (month_str == "Dec")
				month = 12;

			m_last_changed = last_changed_t{
				.year   = from_chars<uint16_t>(time_str.substr(7, 4)),
				.month  = month,
				.day    = from_chars<uint8_t>(time_str.substr(0, 2)),
				.hour   = from_chars<uint8_t>(time_str.substr(12, 2)),
				.minute = from_chars<uint8_t>(time_str.substr(15, 2)),
			};
		}
		else
		{
			auto const releases_str =
				getter_.get("/forge/forge-gui-desktop/maven-metadata.xml");
			std::string_view releases{ releases_str };

			constexpr std::string_view rel_beg    = "<release>",
									   rel_end    = "</release>",
									   update_beg = "<lastUpdated>",
									   update_end = "</lastUpdated>";

			unsigned   end_pos{};
			auto const rel_pos    = releases.find(rel_beg) + rel_beg.size(),
					   update_pos = releases.find(update_beg) + update_beg.size();

			m_newest_ver = releases.substr(
				rel_pos, releases.find(rel_end, rel_pos) - rel_pos);
			m_last_changed = last_changed_from_str(releases.substr(update_pos,
				releases.find(update_end, update_pos) - update_pos));

			auto const ver_str = static_cast<std::string>(m_newest_ver);
			m_latest_target    = "/forge/forge-gui-desktop/" + ver_str
			                  + "/forge-gui-desktop-" + ver_str + ".tar.bz2";
		}
	}

	bool const             m_is_snapshot;
	std::string_view const host =
		m_is_snapshot ? "downloads.cardforge.org" : "releases.cardforge.org";
	getter         getter_;
	version        m_newest_ver{};
	last_changed_t m_last_changed;
	std::string    m_latest_target;
};

remote_info::remote_info(bool is_snapshot, std::string_view exec)
	: m_impl{ new impl{ is_snapshot, exec } }
{
}

remote_info::~remote_info() = default;

version remote_info::newest_version() const
{
	return m_impl->newest_version();
}

last_changed_t remote_info::last_changed() const
{
	return m_impl->last_changed();
}

void remote_info::download_latest(std::string_view dest)
{
	return m_impl->download_latest(dest);
}

last_changed_t last_changed_t::from_bytes(std::string_view bytes)
{
	last_changed_t res;
	std::memcpy(&res, bytes.data(), std::min(bytes.size(), sizeof(last_changed_t)));
	return res;
}

std::string last_changed_t::to_bytes() const
{
	char res[sizeof(last_changed_t)];
	std::memcpy(res, this, sizeof(last_changed_t));
	return { res, sizeof(last_changed_t) };
}
