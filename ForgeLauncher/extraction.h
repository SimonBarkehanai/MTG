#pragma once
#include <filesystem>

void extract_tar_bz2(
	std::filesystem::path const& from, std::filesystem::path const& to);
