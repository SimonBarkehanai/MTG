#include "launcher.h"

#include <format>
#include <fstream>
#include <iostream>
#include <numeric>
#include <vector>

#include <boost/predef/os.h>
#include <boost/program_options.hpp>

#include <Downloader.h>

#include "extraction.h"
#include "remote_info.h"

#ifdef BOOST_OS_WINDOWS_AVAILABLE
#	include <Windows.h>
#elif defined(BOOST_OS_UNIX)
#	include <sys/ioctl.h>
#	include <unistd.h>
#endif

unsigned get_console_width()
{
#ifdef BOOST_OS_WINDOWS_AVAILABLE
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
	return csbi.srWindow.Right - csbi.srWindow.Left + 1;
#elif defined(BOOST_OS_UNIX)
	winsize size;
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &size);
	return size.ws_col;
#else
#	pragma message( \
		"Can't determine window size for this OS. Defaulting to 120 columns.")
	return 120;
#endif
}

namespace po = boost::program_options;

constexpr arg_type operator|(arg_type lhs, arg_type rhs)
{
	using underlying = std::underlying_type_t<arg_type>;
	return static_cast<arg_type>(
		static_cast<underlying>(lhs) | static_cast<underlying>(rhs));
}

constexpr arg_type& operator|=(arg_type& lhs, arg_type rhs)
{
	lhs = lhs | rhs;
	return lhs;
}

constexpr arg_type operator&(arg_type lhs, arg_type rhs)
{
	using underlying = std::underlying_type_t<arg_type>;
	return static_cast<arg_type>(
		static_cast<underlying>(lhs) & static_cast<underlying>(rhs));
}

struct options
{
	arg_type                types;
	std::string             to_download;
	po::options_description desc;
};

auto parse_options(int argc, char const* const* argv)
{
	std::unique_ptr<options> opt{
		new options{.types = {},
					.desc = po::options_description{ "Options", get_console_width(), 85 }}
	};

	std::vector<std::string> download;

	// clang-format off
	opt->desc.add_options()
		("launch,l",        "Launches the latest version of Forge (default)")
		("help,h",          "Displays usage info")
		("version,v",       "Outputs the current Forge version")
		("update,u",        "Updates Forge if there is a new version available")
		("snapshot,s",      "Target the snapshot version (./snapshot/) "
			                "instead of the normal version (./normal/)")
		("assume-yes,y",    "Downloads all decks in the directory")
		("to-download,d", 
			po::value(&download)->multitoken()->value_name("file/search"), 
			"Deck, directory or Scryfall search to download pictures from");
	// clang-format on

	po::variables_map vm;
	po::store(po::command_line_parser{ argc, argv }.options(opt->desc).run(), vm);
	po::notify(vm);

	if (vm.count("help"))
		opt->types |= arg_type::help;
	if (vm.count("launch"))
		opt->types |= arg_type::launch;
	if (vm.count("version"))
		opt->types |= arg_type::version;
	if (vm.count("update"))
		opt->types |= arg_type::update;
	if (vm.count("snapshot"))
		opt->types |= arg_type::snapshot;
	if (vm.count("assume-yes"))
		opt->types |= arg_type::assume_yes;
	if (vm.count("to-download"))
		opt->types |= arg_type::download;
	if (opt->types == arg_type::snapshot || opt->types == arg_type::none)
		opt->types |= arg_type::launch;

	if (!download.empty())
		opt->to_download = std::accumulate(download.begin() + 1, download.end(),
			download.front(), [](std::string&& left, std::string const& right) {
				return std::move(left) + ' ' + right;
			});

	return opt;
}

launcher::launcher(int argc, char const* const* argv)
try : m_options{ parse_options(argc, argv) }, m_exec{ argv[0] },
	m_local{ (fs::absolute(m_exec).parent_path()
			  / ((m_options->types & arg_type::snapshot) == arg_type::snapshot
					  ? "snapshot"
					  : "normal"))
				 .string() },
	m_forge_dir{ m_local.file().substr(0, m_local.file().rfind('/')) }
{
}
catch (po::unknown_option const&)
{
	std::cerr << "Unknown option\n";
}

launcher::~launcher() = default;

int launcher::run()
{
	for (auto const arg : {
			 arg_type::help,
			 arg_type::version,
			 arg_type::update,
			 arg_type::launch,
			 // arg_type::snapshot,
	         // arg_type::assume_yes,
			 arg_type::download,

		 })
		if (auto ret = handle_arg(m_options->types & arg); ret.has_value())
			return *ret;
	return 0;
}

int launcher::launch()
{
	fs::current_path(fs::path(m_local.file()).parent_path());
	auto const cmd = std::format(
		"java -Xmx4096m -Dfile.encoding=UTF-8 "
		"--add-opens java.base/java.lang=ALL-UNNAMED "
		"--add-opens java.base/java.util=ALL-UNNAMED "
		"--add-opens java.base/java.lang.reflect=ALL-UNNAMED "
		"--add-opens java.base/java.text=ALL-UNNAMED "
		"--add-opens java.desktop/java.awt.font=ALL-UNNAMED "
		"-classpath \"{};anything\" forge.view.Main",
		m_local.file());
	return std::system(cmd.c_str());
}

void launcher::update()
{
	auto const download = (fs::temp_directory_path() / "forge.tar.bz2").string();
	fs::path const dest = m_forge_dir;

	if ((m_options->types & arg_type::snapshot) == arg_type::snapshot)
	{
		fs::path const changed = dest / "last_changed.txt";
		remote_info    remote{ true, m_exec };

		if (fs::exists(changed))
		{
			last_changed_t remote_changed = remote.last_changed();
			std::ifstream  istr(changed);
			char           bytes[sizeof(last_changed_t)];
			istr.read(bytes, sizeof(last_changed_t));
			if (remote_changed
				== last_changed_t::from_bytes({ bytes, sizeof(last_changed_t) }))
			{
				std::cout << std::format(
					"Not updating (last updated {}/{}/{} {}:{:0>2})\n",
					remote_changed.month, remote_changed.day, remote_changed.year,
					remote_changed.hour, remote_changed.minute);
				return;
			}
		}

		remote.download_latest(download);

		std::ofstream(changed) << remote.last_changed().to_bytes();
	}
	else
	{
		remote_info remote{ false, m_exec };
		if (remote.newest_version() > m_local.ver())
		{
			std::cout << "Updating from v" << m_local.ver() << " to v"
					  << remote.newest_version() << '\n';

			remote.download_latest(download);
		}
		else
		{
			std::cout << "Not updating v" << m_local.ver() << '\n';
			return;
		}
	}

	std::cout << "\nExtracting to " << dest.string() << ":\n";
	extract_tar_bz2(download, dest);

	fs::remove(download);
}

void launcher::help()
{
	std::cerr << "\nUsage:\n    " << m_exec << " [options]\n\n"
			  << m_options->desc << '\n';
}

void launcher::ver()
{
	std::cout << "Forge version " << m_local.ver()
			  << ((m_options->types & arg_type::snapshot) == arg_type::snapshot
						 ? "-SNAPSHOT\n"
						 : "\n");
}

void launcher::dl()
{
	download(m_exec, m_options->to_download,
		std::string(m_forge_dir.substr(0, m_forge_dir.rfind('/'))) + "/snapshot",
		// always use snapshot for new edition files
		(m_options->types & arg_type::assume_yes) == arg_type::assume_yes);
}

std::optional<int> launcher::handle_arg(arg_type type)
{
	switch (type)
	{
	case arg_type::none:
		return std::nullopt;
		break;

	case arg_type::version:
		ver();
		return std::nullopt;
		break;

	case arg_type::update:
		update();
		return std::nullopt;
		break;

	case arg_type::launch:
		return launch();
		break;

	case arg_type::help:
		help();
		return 2;
		break;

	case arg_type::download:
		dl();
		return 0;
		break;
	}

	return std::nullopt;
}
