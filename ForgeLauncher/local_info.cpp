#include "local_info.h"

constexpr std::string_view file_start = "forge-gui-desktop-";

local_info::local_info(std::string_view forge_dir)
	: local_info{ std::invoke([forge_dir]() {
		std::string path{ forge_dir };
		path += "/forge.java";
		version max_ver{};
		if (fs::exists(forge_dir))
			for (const auto& i : fs::directory_iterator(forge_dir))
			{
				std::string const filename_str{ i.path().filename().string() };
				std::string_view const filename{ filename_str };

				if (!filename.starts_with(file_start))
					continue;
				version this_ver{ filename.substr(file_start.size(),
					filename.find('-', file_start.size()) - file_start.size()) };

				if (this_ver > max_ver)
				{
					max_ver = this_ver;
					path    = i.path().generic_string();
				}
			}
		return std::pair{ path, max_ver };
	}) }
{
}

std::string_view local_info::file() const
{
	return m_java_file;
}

version local_info::ver() const
{
	return m_ver;
}

local_info::local_info(std::pair<std::string_view, version> args)
	: m_java_file{ args.first }, m_ver{ args.second }
{
}
